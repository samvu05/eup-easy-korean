@file:Suppress("DEPRECATION")

package mobi.eup.easykorean.database.repo

import android.content.Context
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import mobi.eup.easykorean.data.model.ItemAudio
import mobi.eup.easykorean.data.model.ItemFeed
import mobi.eup.easykorean.data.model.ItemWord
import mobi.eup.easykorean.data.model.ItemWordHistory

@Suppress("DEPRECATION")
open class MyDatabaseRepo(context: Context) {
    private val mDatabase: MyDatabase = MyDatabase.getInstance(context)

    //  MARK : NEWS FEED
    suspend fun taskFeedInsert(item: ItemFeed) =
        mDatabase.itemFeedDao().insert(item)

    suspend fun taskFeedGetItem(id: String): ItemFeed =
        mDatabase.itemFeedDao().get(id)

    suspend fun taskFeedCheckExist(id: String): Boolean =
        mDatabase.itemFeedDao().checkExist(id)

    suspend fun taskFeedUpdateFavorite(id: String, isFavorite: Int) =
        mDatabase.itemFeedDao().updateFavorite(id, isFavorite)

    fun taskNewsGetListFavorite(type: String) =

        mDatabase.itemFeedDao().getListFavorite(type)

    suspend fun taskFeedUpdateIsSeen(id: String, isSeen: Int) =
        mDatabase.itemFeedDao().updateSeen(id, isSeen)

    fun taskFeedGetListSeen(): LiveData<MutableList<ItemFeed>> =
        mDatabase.itemFeedDao().getListSeen()

    suspend fun taskFeedDeleteAllSeen() =
        mDatabase.itemFeedDao().clearAllSeen()

    fun asyncTaskGetFaforiteStatus(id: String): Int {
        class TaskGetFaforiteStatus : AsyncTask<Void, Void, Int>() {
            override fun doInBackground(vararg params: Void?): Int {
                return mDatabase.itemFeedDao().isFavorite(id)
            }
        }
        return TaskGetFaforiteStatus().execute().get()
    }

    fun asyncTaskGetSeenStatus(id: String): Int {
        class TaskGetSeenStatus : AsyncTask<Void, Void, Int>() {
            override fun doInBackground(vararg params: Void?): Int {
                return mDatabase.itemFeedDao().isSeen(id)
            }
        }
        return TaskGetSeenStatus().execute().get()
    }


    //   MARK : WORD
    suspend fun taskWordInsert(item: ItemWord) =
        mDatabase.itemWordDao().insert(item)

    suspend fun taskWordCheckExits(wordID: String): Boolean =
        mDatabase.itemWordDao().checkExist(wordID)

    suspend fun taskWordGetNote(wordID: String) =
        mDatabase.itemWordDao().getNote(wordID)

    suspend fun taskWordUpdateNote(word: String, note: String) =
        mDatabase.itemWordDao().updateNote(word, note)

    suspend fun taskWordGetFavorite(wordID: String) =
        mDatabase.itemWordDao().getFavorite(wordID)

    suspend fun taskWordUpdateFavorite(wordID: String, isFavorite: Int) =
        mDatabase.itemWordDao().updateIsFavorite(wordID, isFavorite)

    fun taskWordGetListFavorite() =
        mDatabase.itemWordDao().getListFavorite()


    //    MARK: WORD HISTORY
    suspend fun taskWordHistoryInsert(item: ItemWordHistory) =
        mDatabase.itemWordHistoryDao().insert(item)

    fun taskWordHistoryGetAll() =
        mDatabase.itemWordHistoryDao().getAll()

    suspend fun taskWordHistoryDeleteAll() =
        mDatabase.itemWordHistoryDao().clearAll()


    //    MARK: AUDIO MANAGER
    suspend fun taskAudioInsert(itemAudio: ItemAudio) =
        mDatabase.itemAudioDao().insert(itemAudio)

    fun taskAudioGetAll() =
        mDatabase.itemAudioDao().getAll()

    suspend fun taskAudioDeleteAll() =
        mDatabase.itemAudioDao().deleteALl()

    suspend fun taskAudioDelete(itemAudio: ItemAudio) =
        mDatabase.itemAudioDao().delete(itemAudio)

    fun asyncTaskAudioCheckExist(id: String): Boolean {
        class TaskAudioCheckExist : AsyncTask<Void, Void, Boolean>() {
            override fun doInBackground(vararg params: Void): Boolean {
                return mDatabase.itemAudioDao().checkExist(id)
            }
        }
        return TaskAudioCheckExist().execute().get()
    }
}

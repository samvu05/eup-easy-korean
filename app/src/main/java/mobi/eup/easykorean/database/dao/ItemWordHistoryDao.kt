package mobi.eup.easykorean.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import mobi.eup.easykorean.data.model.ItemWordHistory

/**
 * Created by Dinh Sam Vu on 1/18/2021.
 */

@Dao
interface ItemWordHistoryDao {

    @PrimaryKey
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(itemWordHistory: ItemWordHistory)

    @Query("DELETE FROM table_history_word")
    suspend fun clearAll()

    @Query("SELECT * FROM table_history_word")
    fun getAll(): LiveData<List<ItemWordHistory>>
}
package mobi.eup.easykorean.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import mobi.eup.easykorean.data.model.ItemAudio

/**
 * Created by Dinh Sam Vu on 1/21/2021.
 */

@Dao
interface ItemAudioDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(itemAudio: ItemAudio)

    @Delete
    suspend fun delete(itemAudio: ItemAudio)

    @Query("DELETE FROM table_audio")
    suspend fun deleteALl()

    @Query("SELECT * FROM table_audio")
    fun getAll(): LiveData<List<ItemAudio>>

    @Query("SELECT EXISTS (SELECT 1 FROM table_audio WHERE id = :id)")
    fun checkExist(id: String): Boolean

}
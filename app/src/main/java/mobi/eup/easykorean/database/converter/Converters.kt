package mobi.eup.easykorean.database.converter

import androidx.room.TypeConverter
import com.google.gson.Gson

class Converters {

//    Convert the list to json to save it to Room Databse
    @TypeConverter
    fun listToJson(value: List<String>): String = Gson().toJson(value)

    @TypeConverter
    fun jsonToList(value: String) = Gson().fromJson(value, Array<String>::class.java).toList()

}
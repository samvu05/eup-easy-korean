package mobi.eup.easykorean.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import mobi.eup.easykorean.data.model.ItemFeed

@Dao
interface ItemFeedDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(item: ItemFeed)

    @Query("SELECT * FROM table_feed WHERE id = :id")
    suspend fun get(id: String): ItemFeed

    @Delete
    suspend fun clear(item: ItemFeed)

    @Query("SELECT EXISTS (SELECT 1 FROM table_feed WHERE id = :id)")
    suspend fun checkExist(id: String): Boolean

    @Query("SELECT is_seen FROM table_feed WHERE id = :id")
    fun isSeen(id: String): Int

    @Query("UPDATE table_feed SET is_seen = :isSeen WHERE id = :id")
    suspend fun updateSeen(id: String, isSeen: Int)

    @Query("SELECT * FROM table_feed WHERE is_seen = 1")
    fun getListSeen(): LiveData<MutableList<ItemFeed>>

    @Query("UPDATE table_feed SET is_seen = 0")
    suspend fun clearAllSeen()

    @Query("SELECT is_favorite FROM table_feed WHERE id = :id")
    fun isFavorite(id: String): Int

    @Query("UPDATE table_feed SET is_favorite = :isFavorite WHERE id = :id")
    suspend fun updateFavorite(id: String, isFavorite: Int)

    @Query("SELECT * FROM table_feed WHERE is_favorite = 1 AND type = :type")
    fun getListFavorite(type: String): LiveData<MutableList<ItemFeed>>
}
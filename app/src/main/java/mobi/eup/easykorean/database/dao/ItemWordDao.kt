package mobi.eup.easykorean.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import mobi.eup.easykorean.data.model.ItemWord

@Dao
interface ItemWordDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(itemWord: ItemWord)

    @Query("SELECT * FROM table_word WHERE word =:wordID")
    suspend fun getItemWord(wordID: String): ItemWord

    @Query("SELECT EXISTS (SELECT 1 FROM table_word WHERE word = :word)")
    suspend fun checkExist(word: String): Boolean

    @Query("SELECT note FROM table_word WHERE word =:wordID")
    suspend fun getNote(wordID: String): String

    @Query("UPDATE table_word SET note = :note WHERE word = :wordID")
    suspend fun updateNote(wordID: String, note: String)

    @Query("SELECT is_favorite FROM table_word WHERE word = :wordID")
    suspend fun getFavorite(wordID: String) : Int

    @Query("UPDATE table_word SET is_favorite = :isFavorite WHERE word = :word")
    suspend fun updateIsFavorite(word: String, isFavorite: Int)

    @Query("SELECT * FROM table_word WHERE is_favorite = 1")
    fun getListFavorite(): LiveData<MutableList<ItemWord>>

}
package mobi.eup.easykorean.database.repo

import android.content.Context
import androidx.room.*
import mobi.eup.easykorean.database.converter.Converters
import mobi.eup.easykorean.database.dao.ItemAudioDao
import mobi.eup.easykorean.database.dao.ItemFeedDao
import mobi.eup.easykorean.database.dao.ItemWordDao
import mobi.eup.easykorean.database.dao.ItemWordHistoryDao
import mobi.eup.easykorean.data.model.ItemAudio
import mobi.eup.easykorean.data.model.ItemFeed
import mobi.eup.easykorean.data.model.ItemWord
import mobi.eup.easykorean.data.model.ItemWordHistory


@Database(
    entities = [ItemFeed::class, ItemWord::class, ItemWordHistory::class, ItemAudio::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(Converters::class)
abstract class MyDatabase : RoomDatabase() {
    abstract fun itemFeedDao(): ItemFeedDao
    abstract fun itemWordDao(): ItemWordDao
    abstract fun itemWordHistoryDao(): ItemWordHistoryDao
    abstract fun itemAudioDao(): ItemAudioDao

    companion object {
        @Volatile
        private var instance: MyDatabase? = null

        @JvmStatic
        fun getInstance(context: Context): MyDatabase {
            if (instance == null) {
                val builder = Room.databaseBuilder(
                    context.applicationContext,
                    MyDatabase::class.java,
                    "my_database"
                )
                instance = builder
                    .fallbackToDestructiveMigration()
                    .build()
            }
            return instance!!

        }
    }

}
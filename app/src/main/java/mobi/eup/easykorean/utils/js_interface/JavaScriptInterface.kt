package mobi.eup.easykorean.utils.js_interface

import android.webkit.JavascriptInterface
import mobi.eup.easykorean.event.StringCallback
import mobi.eup.easykorean.event.TextSelectCallback

/**
 * Created by Dinh Sam Vu on 1/18/2021.
 */
open class JavaScriptInterface {

    private var stringCallback: StringCallback
    private lateinit var onTextSelectChangeCallback: TextSelectCallback

    constructor(stringCallback: StringCallback) {
        this.stringCallback = stringCallback
    }

    constructor(stringCallback: StringCallback, onTextSelectChangeCallback: TextSelectCallback) {
        this.stringCallback = stringCallback
        this.onTextSelectChangeCallback = onTextSelectChangeCallback
    }

    @JavascriptInterface
    fun onClickText(text: String) {
        stringCallback.execute(text)
    }

    @JavascriptInterface
    fun onTextChange(content: String, left: String, top: String, right: String, bottom: String) {
        onTextSelectChangeCallback.onSelect(content, left, top, right, bottom)
    }

}
package mobi.eup.easykorean.utils.translate;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import mobi.eup.easykorean.data.model.GoogleTranslateJSONObject;
import mobi.eup.easykorean.data.model.WordJSONObject;
import mobi.eup.easykorean.utils.app.GlobalHelper;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Dinh Sam Vu on 1/24/2021.
 */
public class GetGoogleTranslateHelper {
    static String GOOGLE_URL_TRANSLATE = "https://translate.googleapis.com/translate_a/single?client=gtx&dt=t&dt=bd&dj=1&dt=ex&dt=ld&dt=md&dt=qca&dt=rw&dt=rm&dt=ss&sl=%s&tl=%s&q=%s"; // from, to, query
    static String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36";
    private static final OkHttpClient client = new OkHttpClient();

    @SuppressWarnings("deprecation")
    public static GoogleTranslateJSONObject getGoogleTranslate(String from, String to, String
            query) throws JsonSyntaxException {
        String google_translate_url =
                String.format(GOOGLE_URL_TRANSLATE, from, to, URLEncoder.encode(query));

        String json;
        Request request = new Request.Builder()
                .url(google_translate_url)
                .header("charset", "UTF-8")
                .addHeader("User-Agent", USER_AGENT)
                .build();


        Response response;
        try {
            response = client.newCall(request).execute();
            if (response.body() != null) {
                json = response.body().string();
                return new Gson().fromJson(json, GoogleTranslateJSONObject.class);
            }
        } catch (IOException | JsonSyntaxException e) {
            return newGoogleTranslateApi(query, from, to);
        }

        return null;
    }


    // MARK: new google translate api
    public static GoogleTranslateJSONObject newGoogleTranslateApi(String query, String
            from, String to) {
        String json;
        GoogleTranslateJSONObject jsonObject = null;

        String google_translate_url = "https://translate.google.com/_/TranslateWebserverUi/data/batchexecute?rpcids=MkEWBc&f.sid=9003407303509348738&bl=boq_translate-webserver_20201215.15_p0&hl=vi&soc-app=1&soc-platform=1&soc-device=1&_reqid=828345&rt=c";
        String freq = String.format("[[[\"MkEWBc\",\"[[\\\"%s\\\",\\\"%s\\\",\\\"%s\\\",true],[null]]\",null,\"generic\"]]]", query, from, to);
        RequestBody requestBody = new FormBody.Builder()
                .add("f.req", freq)
                .add("at", "AD08yZnPhFKfec27OLdMK7AvFyJw:1608511943250")
                .build();

        Request request = new Request.Builder()
                .url(google_translate_url)
                .post(requestBody)
                .addHeader("User-Agent", USER_AGENT)
                .addHeader("content-type", "application/x-www-form-urlencoded;charset=utf-8")
                .addHeader("Cookie", "NID=205=aol2HVd-_cynCwllqjD_FWXCnm-0m1lkKpdi6l5wGV6vNW1KcOyk7JmProtwBxEp1quflqaa3NgbdCSGVRRonEbehVnZiYo-iX_8OOZj-EcZMCg14CgL2u4gZY9ljpUCAM-92yP390AFgu2nKCODpgYLLbFd9yVJhvNeO8JuMh0; 1P_JAR=2020-12-19-09")
                .build();

        try {
            Response response = client.newCall(request).execute();
            if (response.body() != null) {
                json = response.body().string();

                String mean = "", phonetic = "";
                Gson gson = new Gson();

                Pattern pattern = Pattern.compile("\\[\"wrb.fr\",\"MkEWBc\"(.|\\n)*?\"generic\"]");
                Matcher matcher = pattern.matcher(json);
                while (matcher.find()) {
                    json = matcher.group(0);
                }

                Type listType = new TypeToken<List<Object>>() {
                }.getType();
                List<Object> arr = gson.fromJson(json, listType);
                if (arr != null && arr.size() > 2) {
                    String arr1String = (String) arr.get(2);
                    List<Object> arr1 = gson.fromJson(arr1String, listType);
                    if (arr1 != null && arr1.size() > 0 && arr1.get(0) != null) {
                        List<Object> arr2 = (List<Object>) arr1.get(0);
                        if (arr2 != null && arr2.size() > 0 && arr2.get(0) != null) {
                            phonetic = ((String) arr2.get(0)).trim();
                        }
                    }

                    if (arr1 != null && arr1.size() > 1 && arr1.get(1) != null) {
                        List<Object> arr3 = (List<Object>) arr1.get(1);
                        if (arr3 != null && arr3.size() > 0 && arr3.get(0) != null) {
                            List<Object> arr4 = (List<Object>) arr3.get(0);
                            if (arr4 != null && arr4.size() > 0 && arr4.get(0) != null) {
                                arr4 = (List<Object>) arr4.get(0);

                                if (arr4 != null && arr4.size() > 4 && phonetic.isEmpty()) {
                                    for (int i = 0; i < 5; i++) {
                                        if (arr4.get(i) != null) {
                                            try {
                                                String p = ((String) arr4.get(i)).trim();
                                                if (!p.isEmpty()) {
                                                    phonetic = p;
                                                    break;
                                                }
                                            } catch (ClassCastException ignored) {
                                            }
                                        }
                                    }
                                }

                                if (arr4 != null && arr4.size() > 5 && arr4.get(5) != null) {
                                    List<Object> arr5 = (List<Object>) arr4.get(5);
                                    if (arr5 != null && arr5.size() > 0) {
                                        for (Object item5 : arr5) {
                                            if (item5 != null) {
                                                List<Object> arr6 = (List<Object>) item5;
                                                if (arr6.size() > 0) {
                                                    String temp = (String) arr6.get(0);
                                                    if (temp != null) mean += temp;
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }
                }

                if (!mean.isEmpty() && !phonetic.isEmpty()) {
                    jsonObject = createFakeFromString(query, mean, phonetic);
                }
            }
        } catch (IOException | JsonSyntaxException e) {
            e.printStackTrace();
        }

        if (jsonObject == null) {
            google_translate_url = "https://www.google.com.vn/async/translate?vet=12ahUKEwjSgcPShM7eAhUH_GEKHaG2D5kQqDgwAHoECAYQFg..i&ei=EQTpW5K1EYf4hwOh7b7ICQ&yv=3";
            requestBody = new FormBody.Builder()
                    .add("async", "translate,sl:" + from + ",tl:" + to + ",st:" + URLEncoder.encode(query) + ",id:1541997726654,qc:true,ac:true,_id:tw-async-translate,_pms:s,_fmt:pc")
                    .build();

            request = new Request.Builder()
                    .url(google_translate_url)
                    .post(requestBody)
                    .addHeader("User-Agent",USER_AGENT)
                    .addHeader("content-type", "application/x-www-form-urlencoded;charset=utf-8")
                    .build();

            try {
                Response response = client.newCall(request).execute();
                if (response.body() != null) {
                    json = response.body().string();
                    String mean = "", phonetic = "";

                    Pattern pattern = Pattern.compile("<span id=\"tw-answ-target-text\">((.|\\n)*?)</span>");
                    Matcher matcher = pattern.matcher(json);
                    while (matcher.find()) {
                        mean = matcher.group(1);
                    }

                    pattern = Pattern.compile("<span id=\"tw-answ-romanization\">((.|\\n)*?)</span>");
                    matcher = pattern.matcher(json);
                    while (matcher.find()) {
                        phonetic = matcher.group(1).trim();
                    }

                    if (phonetic == null || phonetic.isEmpty()) {
                        pattern = Pattern.compile("<span id=\"tw-answ-source-romanization\">((.|\\n)*?)</span>");
                        matcher = pattern.matcher(json);
                        while (matcher.find()) {
                            phonetic = matcher.group(1).trim();
                        }
                    }

                    if (mean != null && !mean.isEmpty() && phonetic != null && !phonetic.isEmpty()) {
                        jsonObject = createFakeFromString(query, mean, phonetic);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }

        return jsonObject;
    }

    private static GoogleTranslateJSONObject createFakeFromString(String query, String
            mean, String phonetic) {
        GoogleTranslateJSONObject object = new GoogleTranslateJSONObject();
        List<GoogleTranslateJSONObject.Sentence> sentences = new ArrayList<>();
        String[] queries = query.split("\n");
        String[] means = mean.split("\n");
        String[] phonetics = phonetic.split("\n");

        if (queries.length == means.length && queries.length > 1) {
            for (int i = 0; i < means.length; i++) {
                GoogleTranslateJSONObject.Sentence sen = new GoogleTranslateJSONObject.Sentence();
                sen.setOrig(queries[i] + "\n");
                if (means[i] != null) {
                    sen.setTrans(means[i] + "\n");
                }
                if (phonetics.length > i && phonetics[i] != null &&
                        !phonetics[i].isEmpty()) {
                    sen.setSrcTranslit(phonetics[i] + " ");
                }
                sentences.add(sen);
            }
        } else {
            GoogleTranslateJSONObject.Sentence sen = new GoogleTranslateJSONObject.Sentence();
            sen.setOrig(query);
            if (mean != null) {
                sen.setTrans(mean);
            }
            if (phonetic != null && !phonetic.isEmpty()) {
                sen.setSrcTranslit(phonetic);
            }
            sentences.add(sen);
        }

        object.setSentences(sentences);
        return object;
    }

    public static WordJSONObject convertGoogleTranslate2Word(GoogleTranslateJSONObject
                                                                     googleTranslateJSONObject, String word) {
        WordJSONObject wordJSONObject = new WordJSONObject();
        ArrayList<WordJSONObject.Datum> datumArrayList = new ArrayList<>();
        WordJSONObject.Datum datum = convertGoogleTranslate2Datum(googleTranslateJSONObject, word);
        datumArrayList.add(datum);

        if (googleTranslateJSONObject.getRelatedWords() != null && googleTranslateJSONObject.getRelatedWords().getWord() != null) {
            for (String wordRelative : googleTranslateJSONObject.getRelatedWords().getWord()) {
                WordJSONObject.Datum datumOther = new WordJSONObject.Datum(false, "", wordRelative, "", "0", null);
                datumArrayList.add(datumOther);
            }
        }

        wordJSONObject.setData(datumArrayList);
        return wordJSONObject;
    }

    public static WordJSONObject.Datum convertGoogleTranslate2Datum(GoogleTranslateJSONObject
                                                                            googleTranslateJSONObject, String word) {
        String origin = "", phonetic = ""; // tempMeans = "";
        ArrayList<WordJSONObject.Mean> means = new ArrayList<>();

        if (googleTranslateJSONObject.getSentences() != null && googleTranslateJSONObject.getSentences().size() > 0) {
            for (int i = 0; i < googleTranslateJSONObject.getSentences().size(); i++) {
                String p = googleTranslateJSONObject.getSentences().get(i).getSrcTranslit();
                if (p != null && !p.isEmpty()) {
                    phonetic += p;
                }
            }
        }

        if (googleTranslateJSONObject.getSentences() != null && !googleTranslateJSONObject.getSentences().isEmpty()) {
            String kind = "";
            StringBuilder mean = new StringBuilder();
            for (GoogleTranslateJSONObject.Sentence sentence : googleTranslateJSONObject.getSentences()) {
                if (sentence.getOrig() != null) {
                    origin = sentence.getOrig();
                }
                if (sentence.getTrans() != null)
                    mean.append((mean.length() == 0) ? sentence.getTrans() : ", " + sentence.getTrans());
            }
//            tempMeans = mean.toString();
            means.add(new WordJSONObject.Mean(mean.toString(), kind));
        }
        if (googleTranslateJSONObject.getDict() != null && !googleTranslateJSONObject.getDict().isEmpty()) {
            for (GoogleTranslateJSONObject.Dict dict : googleTranslateJSONObject.getDict()) {
                if (!origin.equals(dict.getBaseForm())) continue;

                String kind = "";
                StringBuilder mean = new StringBuilder();
                if (dict.getPos() != null) kind = dict.getPos();
                if (dict.getTerms() != null) {
                    for (String term : dict.getTerms()) {
                        mean.append((mean.length() == 0) ? term : "; " + term);
                    }
                }
                means.add(new WordJSONObject.Mean(mean.toString(), kind));
            }
        }

        WordJSONObject.Datum datum = new WordJSONObject.Datum(true, "", word, phonetic, "0", means);

        // tu dong nghia
        datum.setSynsetArrayList(googleTranslateJSONObject.getSynsets());

        return datum;
    }

    // MARK : WORD
    public static String getMeanAndPhoneticByWord(String from, String to, String query) {
        String google_translate_url = String.format(GlobalHelper.GOOGLE_URL_TRANSLATE, from, to, URLEncoder.encode(query));
        String json = "";

        Request request = new Request.Builder()
                .url(google_translate_url)
                .header("charset", "UTF-8")
                .addHeader("User-Agent", GlobalHelper.USER_AGENT)
                .build();

        Response response = null;
        try {
            response = client.newCall(request).execute();
            if (response.body() != null) {
                json = response.body().string();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        GoogleTranslateJSONObject jsonObject = null;
        try {
            jsonObject = new Gson().fromJson(json, GoogleTranslateJSONObject.class);
        } catch (JsonSyntaxException e) {
            jsonObject = newGoogleTranslateApi(query, from, to);
        }

        if (jsonObject == null || jsonObject.getSentences() == null)
            return "";

        StringBuilder trans = new StringBuilder();
        String tran = "";
        if (jsonObject.getSentences().get(0).getTrans() != null) {
            tran = jsonObject.getSentences().get(0).getTrans();
            trans = new StringBuilder(tran);
        }

        if (jsonObject.getDict() != null && !jsonObject.getDict().isEmpty()) {
            for (GoogleTranslateJSONObject.Dict dict : jsonObject.getDict()) {
                if (dict.getTerms() != null && !dict.getTerms().isEmpty()) {
                    for (String m : dict.getTerms()) {
                        if (!tran.toLowerCase().equals(m.trim().toLowerCase())) {
                            trans.append(", ").append(m);
                        }
                    }
                }
            }
        }

        StringBuilder srcTranslate = new StringBuilder();
        if (jsonObject.getSentences().size() > 0) {
            for (int i = 0; i < jsonObject.getSentences().size(); i++) {
                String p = jsonObject.getSentences().get(i).getSrcTranslit();
                if (p != null && !p.isEmpty()) {
                    srcTranslate.append(p);
                }
            }
        }

        return query + "_" + trans.toString() + "_" + srcTranslate;
    }
}

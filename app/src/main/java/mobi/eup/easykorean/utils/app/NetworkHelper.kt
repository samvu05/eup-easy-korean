package mobi.eup.easykorean.utils.app

import android.content.Context
import android.net.ConnectivityManager

/**
 * Created by Dinh Sam Vu on 1/21/2021.
 */
@Suppress("DEPRECATION")
class NetworkHelper {
    companion object {
        @JvmStatic
        fun isNetWork(context: Context?): Boolean {
            if (context == null) return false
            val connect =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val netInfo = connect.activeNetworkInfo
            return netInfo != null && netInfo.isConnected
        }

        @JvmStatic
        fun isNetWorkMobile(context: Context): Boolean {
            val connect =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val netInfo = connect.activeNetworkInfo
            return netInfo != null && netInfo.isConnected && netInfo.type == ConnectivityManager.TYPE_MOBILE
        }

    }
}
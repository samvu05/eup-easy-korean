package mobi.eup.easykorean.utils.iap;

import android.os.AsyncTask;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.SkuDetails;

import java.util.ArrayList;

import mobi.eup.easykorean.utils.app.GlobalHelper;

@SuppressWarnings("deprecation")
public class GetPriceHelper extends AsyncTask<BillingProcessor, Void, ArrayList<SkuDetails>> {
    private final GetPriceCallback getPriceCallback;
    private final String percent;

    public GetPriceHelper(GetPriceCallback getPriceCallback, String percent) {
        this.getPriceCallback = getPriceCallback;
        this.percent = !percent.equals("0") ? "_".concat(percent) : "";
    }

    @Override
    protected ArrayList<SkuDetails> doInBackground(BillingProcessor... billingProcessors) {
        ArrayList<SkuDetails> result = new ArrayList<>();
        BillingProcessor bp = billingProcessors[0];

        SkuDetails skuPremium = bp.getPurchaseListingDetails(GlobalHelper.SKU_PREMIUM.concat(percent));
        if (skuPremium != null)
            result.add(skuPremium);

        SkuDetails sku12 = bp.getSubscriptionListingDetails(GlobalHelper.SKU_SUB12.concat(percent));
        if (sku12 != null)
            result.add(sku12);

        SkuDetails sku3 = bp.getSubscriptionListingDetails(GlobalHelper.SKU_SUB3.concat(percent));
        if (sku3 != null) {
            result.add(sku3);
        }

        return result;
    }

    @Override
    protected void onPostExecute(ArrayList<SkuDetails> skuDetails) {
        super.onPostExecute(skuDetails);
        if (skuDetails == null || skuDetails.size() < 3) return;

        if (getPriceCallback != null) getPriceCallback.onPost(skuDetails);
    }

    public interface GetPriceCallback {
        void onPost(ArrayList<SkuDetails> skuDetails);
    }
}

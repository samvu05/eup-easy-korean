package mobi.eup.easykorean.utils.eventbus

class EventSettingsHelper(mState: EventBusState) {
    private var state: EventBusState = mState

    fun getStateChange(): EventBusState {
        return state
    }
}
package mobi.eup.easykorean.utils.translate;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import mobi.eup.easykorean.data.model.GoogleTranslateJSONObject;
import mobi.eup.easykorean.data.model.WordJSONObject;
import mobi.eup.easykorean.event.VoidCallback;
import mobi.eup.easykorean.event.WordCallback;
import mobi.eup.easykorean.utils.translate.GetGoogleTranslateHelper;

import okhttp3.OkHttpClient;

/**
 * Created by Dinh Sam Vu on 1/24/2021.
 */
public class SearchHelper<T> extends AsyncTask<String, Void, T> {
    private VoidCallback onPreExecute;
    private WordCallback wordCallback;
    private OkHttpClient client;
    private Class<T> clazz;
    private Context context;


    public SearchHelper(VoidCallback onPreExecute, Context context, Class<T> clazz) {
        this.onPreExecute = onPreExecute;
        this.client = new OkHttpClient();
        this.clazz = clazz;
        this.context = context;
    }

    @Override
    protected T doInBackground(String... strings) {

        T obj = null;

        String type = strings[0], query = strings[1];
//        String currentLangCode = (strings.length > 3 && strings[3] != null) ? strings[3] : preferenceHelper.getCurrentLanguageCode();
        String currentLangCode = (strings.length > 3 && strings[3] != null) ? strings[3] : "vi";

        switch (type) {
            case "word":
                GoogleTranslateJSONObject googleTranslateJSONObject = GetGoogleTranslateHelper.getGoogleTranslate("ko", currentLangCode, query);
                if (googleTranslateJSONObject != null) {
                    return (T) GetGoogleTranslateHelper.convertGoogleTranslate2Word(googleTranslateJSONObject, query);
                }
                break;
        }

        switch (type) {
            case "word":
                GoogleTranslateJSONObject googleTranslateJSONObject = GetGoogleTranslateHelper.getGoogleTranslate("ko", currentLangCode, query);
                if (googleTranslateJSONObject != null) {
                    // 4/24/18 create word json object from google translate json object
                    return (T) GetGoogleTranslateHelper.convertGoogleTranslate2Word(googleTranslateJSONObject, query);
                }
                break;
        }

        if (obj instanceof WordJSONObject) {
            if (((WordJSONObject) obj).getData() != null && !((WordJSONObject) obj).getData().isEmpty()) {

                GoogleTranslateJSONObject googleTranslateJSONObject = GetGoogleTranslateHelper.getGoogleTranslate("ko", currentLangCode, query);
                if (googleTranslateJSONObject != null) {
                    ((WordJSONObject) obj).getData().add(0, GetGoogleTranslateHelper.convertGoogleTranslate2Datum(googleTranslateJSONObject, query));
                }
            } else {
                GoogleTranslateJSONObject googleTranslateJSONObject = GetGoogleTranslateHelper.getGoogleTranslate("ko", currentLangCode, query);
                if (googleTranslateJSONObject != null)// 4/24/18 create word json object from google translate json object
                    obj = (T) GetGoogleTranslateHelper.convertGoogleTranslate2Word(googleTranslateJSONObject, query);
            }

        }

        GoogleTranslateJSONObject googleTranslateJSONObject = GetGoogleTranslateHelper.getGoogleTranslate("ko", currentLangCode, query);
        if (googleTranslateJSONObject != null)
            obj = (T) GetGoogleTranslateHelper.convertGoogleTranslate2Word(googleTranslateJSONObject, query);

        return obj;
    }

    public void setWordCallback(WordCallback wordCallback) {
        this.wordCallback = wordCallback;
    }

    @Override
    protected void onPostExecute(T t) {
        super.onPostExecute(t);
        if (t instanceof WordJSONObject && wordCallback != null) {
            wordCallback.execute((WordJSONObject) t);
        }
        context = null;
    }
}

package mobi.eup.easykorean.utils.js_interface

import android.webkit.JavascriptInterface
import mobi.eup.easykorean.event.StringCallback
import mobi.eup.easykorean.event.TextareaCallback

/**
 * Created by Dinh Sam Vu on 1/18/2021.
 */
class JavaScriptInterfaceTranslate(
    stringCallback: StringCallback,
    mTextareaCallback: TextareaCallback
) : JavaScriptInterface(
    stringCallback
) {
    private var textareaCallback: TextareaCallback = mTextareaCallback

    @JavascriptInterface
    fun checkInput(json: String) {
        textareaCallback.checkInput(json)
    }

    @JavascriptInterface
    fun onInputChange(text: String) {
        textareaCallback.onInputChange(text)
    }

    @JavascriptInterface
    fun onFocusChange(text: String) {
        textareaCallback.onFocusChange(text)
    }
}
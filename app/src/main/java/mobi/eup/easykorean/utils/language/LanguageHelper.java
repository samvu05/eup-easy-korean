package mobi.eup.easykorean.utils.language;

import android.content.Context;

import java.util.Locale;

/**
 * Created by Dinh Sam Vu on 1/24/2021.
 */
public class LanguageHelper {

    public static String[][] LIST_LANGUAGE = {
            {"af", "Afrikaans", "", "south_africa", "\uD83C\uDDFF\uD83C\uDDE6"},
            {"sq", "Albanian", "", "albania", "\uD83C\uDDE6\uD83C\uDDF1"},
            {"ar", "Arabic", "ar-SA", "saudi_arabia", "\uD83C\uDDE6\uD83C\uDDEA"},
            {"hy", "Armenian", "", "armenia", "\uD83C\uDDE6\uD83C\uDDF2"},
            {"az", "Azerbaijani", "", "azerbaijan", "\uD83C\uDDE6\uD83C\uDDFF"},
            {"eu", "Basque", "", "spain", "\uD83C\uDFF3️\u200D\uD83C\uDF08"},
            {"be", "Belarusian", "", "belarus", "\uD83C\uDDE7\uD83C\uDDFE"},
            {"bn", "Bengali", "", "benin", "\uD83C\uDDE7\uD83C\uDDE9"},
            {"bs", "Bosnian", "", "bosnia_and_herzegovina", "\uD83C\uDDE7\uD83C\uDDE6"},
            {"bg", "Bulgarian", "", "bulgaria", "\uD83C\uDDE7\uD83C\uDDEC"},
            {"ca", "Catalan", "", "spain", "\uD83C\uDFF3️\u200D\uD83C\uDF08"},
            {"ceb", "Cebuano", "", "philippines", "\uD83C\uDFF3️\u200D\uD83C\uDF08"},
            {"ny", "Chichewa", "", "malawi", "\uD83C\uDDF2\uD83C\uDDFC"},
            {"zh-CN", "Chinese Simplified", "zh-CN", "china", "\uD83C\uDDE8\uD83C\uDDF3"},
            {"zh-TW", "Chinese Traditional", "zh-CN", "china", "\uD83C\uDDF9\uD83C\uDDFC"},
            {"hr", "Croatian", "", "croatia", "\uD83C\uDDED\uD83C\uDDF7"},
            {"cs", "Czech", "cs-CZ", "czech_republic", "\uD83C\uDDE8\uD83C\uDDFF"},
            {"da", "Danish", "da-DK", "denmark", "\uD83C\uDDE9\uD83C\uDDF0"},
            {"nl", "Dutch", "", "netherlands", "\uD83C\uDDF3\uD83C\uDDF1"},
            {"en", "English", "en-US", "united_kingdom", "\uD83C\uDFF4\uDB40\uDC67\uDB40\uDC62\uDB40\uDC65\uDB40\uDC6E\uDB40\uDC67\uDB40\uDC7F"},
            {"eo", "Esperanto", "", "esperanto", "\uD83C\uDFF3️\u200D\uD83C\uDF08"},
            {"et", "Estonian", "", "estonia", "\uD83C\uDDEA\uD83C\uDDEA"},
            {"tl", "Filipino", "", "philippines", "\uD83C\uDDF5\uD83C\uDDED"},
            {"fi", "Finnish", "fi-FI", "finland", "\uD83C\uDDEB\uD83C\uDDEE"},
            {"fr", "French", "fr-CA", "france", "\uD83C\uDDEB\uD83C\uDDF7"},
            {"gl", "Galician", "", "spain", "\uD83C\uDFF3️\u200D\uD83C\uDF08"},
            {"ka", "Georgian", "", "georgia", "\uD83C\uDDEC\uD83C\uDDEA"},
            {"de", "German", "de-DE", "germany", "\uD83C\uDDE9\uD83C\uDDEA"},
            {"gu", "Gujarati", "", "india", "\uD83C\uDFF3️\u200D\uD83C\uDF08"},
            {"el", "Greek", "el-GR", "greece", "\uD83C\uDDEC\uD83C\uDDF7"},
            {"ht", "Haitian Creole", "", "haiti", "\uD83C\uDDED\uD83C\uDDF9"},
            {"ha", "Hausa", "", "niger", "\uD83C\uDFF3️\u200D\uD83C\uDF08"},
            {"iw", "Hebrew", "he-IL", "israel", "\uD83C\uDDEE\uD83C\uDDF1"},
            {"hi", "Hindi", "hi-IN", "india", "\uD83C\uDDEE\uD83C\uDDF3"},
            {"hmn", "Hmong", "", "china", "\uD83C\uDFF3️\u200D\uD83C\uDF08"},
            {"hu", "Hungarian", "hu-HU", "hungary", "\uD83C\uDDED\uD83C\uDDFA"},
            {"is", "Icelandic", "", "iceland", "\uD83C\uDDEE\uD83C\uDDF8"},
            {"id", "Indonesian", "in_ID", "indonesia", "\uD83C\uDDEE\uD83C\uDDE9"},
            {"ig", "Igbo", "", "nigeria", "\uD83C\uDDFF\uD83C\uDDE6"},
            {"ga", "Irish", "", "ireland", "\uD83C\uDDEE\uD83C\uDDEA"},
            {"it", "Italian", "it-IT", "italy", "\uD83C\uDDEE\uD83C\uDDF9"},
            {"ja", "Japanese", "ja-JP", "japan", "\uD83C\uDDEF\uD83C\uDDF5"},
            {"jw", "Javanese", "", "indonesia", "\uD83C\uDDEE\uD83C\uDDE9"},
            {"kk", "Kazakh", "", "kazakhstan", "\uD83C\uDDF0\uD83C\uDDFF"},
            {"km", "Khmer", "", "cambodia", "\uD83C\uDDF0\uD83C\uDDED"},
            {"kn", "Kannada", "", "india", "\uD83C\uDFF3️\u200D\uD83C\uDF08"},
            {"ko", "Korean", "ko-KR", "south_korea", "\uD83C\uDDF0\uD83C\uDDF7"},
            {"lo", "Lao", "", "laos", "\uD83C\uDDF1\uD83C\uDDE6"},
            {"lv", "Latvian", "", "latvia", "\uD83C\uDDF1\uD83C\uDDFB"},
            {"lt", "Lithuanian", "", "lithuania", "\uD83C\uDDF1\uD83C\uDDF9"},
            {"mk", "Macedonian", "", "macedonia", "\uD83C\uDDF2\uD83C\uDDF0"},
            {"mg", "Malagasy", "", "madagascar", "\uD83C\uDDF2\uD83C\uDDEC"},
            {"ms", "Malay", "", "malaysia", "\uD83C\uDDF2\uD83C\uDDFE"},
            {"ml", "Malayalam", "", "india", "\uD83C\uDFF3️\u200D\uD83C\uDF08"},
            {"mi", "Maori", "", "new_zealand", "\uD83C\uDFF3️\u200D\uD83C\uDF08"},
            {"mr", "Marathi", "", "india", "\uD83C\uDFF3️\u200D\uD83C\uDF08"},
            {"my", "Myanmar (Burmese)", "", "myanmar", "\uD83C\uDDF2\uD83C\uDDF2"},
            {"mn", "Mongolian", "", "mongolia", "\uD83C\uDDF2\uD83C\uDDF3"},
            {"ne", "Nepali", "", "nepal", "\uD83C\uDDF3\uD83C\uDDF5"},
            {"no", "Norwegian", "no-NO", "norway", "\uD83C\uDDF3\uD83C\uDDF4"},
            {"fa", "Persian", "", "iran", "\uD83C\uDDEE\uD83C\uDDF7"},
            {"pl", "Polish", "pl-PL", "poland", "\uD83C\uDDF5\uD83C\uDDF1"},
            {"pt", "Portuguese", "pt-BR", "portugal", "\uD83C\uDDF5\uD83C\uDDF9"},
            {"pa", "Punjabi", "", "pakistan", "\uD83C\uDFF3️\u200D\uD83C\uDF08"},
            {"ro", "Romanian", "ro-RO", "romania", "\uD83C\uDDF7\uD83C\uDDF4"},
            {"ru", "Russian", "ru-RU", "russia", "\uD83C\uDDF7\uD83C\uDDFA"},
            {"sr", "Serbian", "", "serbia", "\uD83C\uDDF7\uD83C\uDDF8"},
            {"st", "Sesotho", "", "lesotho", "\uD83C\uDDF1\uD83C\uDDF8"},
            {"si", "Sinhala", "", "sri_lanka", "\uD83C\uDDF1\uD83C\uDDF0"},
            {"sk", "Slovak", "sk-SK", "slovakia", "\uD83C\uDDF8\uD83C\uDDF0"},
            {"sl", "Slovenian", "", "slovenia", "\uD83C\uDDF8\uD83C\uDDEE"},
            {"so", "Somali", "", "somalia", "\uD83C\uDDF8\uD83C\uDDF4"},
            {"es", "Spanish", "es-ES", "spain", "\uD83C\uDDEA\uD83C\uDDF8"},
            {"su", "Sudanese", "", "sudan", "\uD83C\uDDF8\uD83C\uDDE9"},
            {"sv", "Swedish", "sv-SE", "sweden", "\uD83C\uDDF8\uD83C\uDDEA"},
            {"sw", "Swahili", "", "tanzania", "\uD83C\uDDF0\uD83C\uDDEA"},
            {"ta", "Tamil", "", "singapore", "\uD83C\uDFF3️\u200D\uD83C\uDF08"},
            {"te", "Telugu", "", "india", "\uD83C\uDDEE\uD83C\uDDEA"},
            {"tg", "Tajik", "", "tajikistan", "\uD83C\uDDF9\uD83C\uDDEF"},
            {"th", "Thai", "th-TH", "thailand", "\uD83C\uDDF9\uD83C\uDDED"},
            {"tr", "Turkish", "tr-TR", "turkey", "\uD83C\uDDF9\uD83C\uDDF7"},
            {"uk", "Ukrainian", "", "ukraine", "\uD83C\uDDFA\uD83C\uDDE6"},
            {"ur", "Urdu", "", "pakistan", "\uD83C\uDDF5\uD83C\uDDF0"},
            {"uz", "Uzbek", "", "uzbekistan", "\uD83C\uDDFA\uD83C\uDDFF"},
            {"vi", "Vietnamese", "", "vietnam", "\uD83C\uDDFB\uD83C\uDDF3"},
            {"cy", "Welsh", "", "wales", "\uD83C\uDFF4\uDB40\uDC67\uDB40\uDC62\uDB40\uDC77\uDB40\uDC6C\uDB40\uDC73\uDB40\uDC7F"},
            {"yi", "Yiddish", "", "sweden", "\uD83C\uDDEE\uD83C\uDDF1"},
            {"yo", "Yoruba", "", "nigeria", "\uD83C\uDDF3\uD83C\uDDEC"},
            {"zu", "Zulu", "", "south_africa", "\uD83C\uDDFF\uD83C\uDDE6"},
    };

    private static final int DEFAULT_POSITION = 19;

    public static boolean isVietnamese(String word) {
        char[] vietnameseChars = new char[]{'à', 'á', 'ạ', 'ả', 'ã', 'â', 'ầ', 'ấ', 'ậ', 'ẩ', 'ẫ', 'ă', 'ằ', 'ắ', 'ặ', 'ẳ', 'ẵ', 'è', 'é', 'ẹ', 'ẻ', 'ẽ', 'ê', 'ề', 'ế', 'ệ', 'ể', 'ễ', 'ì', 'í', 'ị', 'ỉ', 'ĩ', 'ò', 'ó', 'ọ', 'ỏ', 'õ', 'ô', 'ồ', 'ố', 'ộ', 'ổ', 'ỗ', 'ơ', 'ờ', 'ớ', 'ợ', 'ở', 'ỡ', 'ù', 'ú', 'ụ', 'ủ', 'ũ', 'ư', 'ừ', 'ứ', 'ự', 'ử', 'ữ', 'ỳ', 'ý', 'ỵ', 'ỷ', 'ỹ', 'đ', 'À', 'Á', 'Ạ', 'Ả', 'Ã', 'Â', 'Ầ', 'Ấ', 'Ậ', 'Ẩ', 'Ẫ', 'Ă', 'Ằ', 'Ắ', 'Ặ', 'Ẳ', 'Ẵ', 'È', 'É', 'Ẹ', 'Ẻ', 'Ẽ', 'Ê', 'Ề', 'Ế', 'Ệ', 'Ể', 'Ễ', 'Ì', 'Í', 'Ị', 'Ỉ', 'Ĩ', 'Ò', 'Ó', 'Ọ', 'Ỏ', 'Õ', 'Ô', 'Ồ', 'Ố', 'Ộ', 'Ổ', 'Ỗ', 'Ơ', 'Ờ', 'Ớ', 'Ợ', 'Ở', 'Ỡ', 'Ù', 'Ú', 'Ụ', 'Ủ', 'Ũ', 'Ư', 'Ừ', 'Ứ', 'Ự', 'Ử', 'Ữ', 'Ỳ', 'Ý', 'Ỵ', 'Ỷ', 'Ỹ', 'Đ'};
        int len = word.length();
        for (int i = 0; i < len; i++) {
            for (char vietnameseChar : vietnameseChars)
                if (vietnameseChar == (word.charAt(i))) {
                    return true;
                }
        }
        return false;
    }

    public static boolean isEnglish(String keyword) {
        if (keyword != null) {
            int len = keyword.length();
            for (int i = 0; i < len; i++) {
                if ((keyword.charAt(i) >= 0x0061 && keyword.charAt(i) <= 0x007A) ||
                        (keyword.charAt(i) >= 0x0041 && keyword.charAt(i) <= 0x005A)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static int getDefaultPositionLanguage(Context context) {
        Locale currentLocale = context.getResources().getConfiguration().locale;
        String languageCode = currentLocale.getLanguage();

        int defaultPositionLanguage = DEFAULT_POSITION;

        if (languageCode.equals("zh")) { // 13: simple, 14: traditional
            String country = currentLocale.toString();
            if (country.contains("CN") || country.contains("Hans")) {
                return 13;
            } else {
                return 14;
            }
        }

        if (languageCode.equals("in")) { // indo
            return 37;
        }

        for (int i = 0; i < LIST_LANGUAGE.length; i++) {
            if (languageCode.equals(LIST_LANGUAGE[i][0])) {
                defaultPositionLanguage = i;
                break;
            }
        }
        return defaultPositionLanguage;
    }

    public static String getDefaultLanguageName(Context context) {
        if (context == null) return "English";
        return LIST_LANGUAGE[getDefaultPositionLanguage(context)][1];
    }

    public static String getDefaultLanguageCode(Context context) {
        if (context == null) return "en";
        return LIST_LANGUAGE[getDefaultPositionLanguage(context)][0];
    }

}

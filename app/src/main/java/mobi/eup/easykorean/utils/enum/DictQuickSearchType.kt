package mobi.eup.easykorean.utils.enum

/**
 * Created by Dinh Sam Vu on 1/22/2021.
 */
enum class DictQuickSearchType {
    CURRENT, KOKO, KOEN
}
package mobi.eup.easykorean.utils.app

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import mobi.eup.easykorean.R
import mobi.eup.easykorean.data.model.ItemFeed
import mobi.eup.easykorean.view.adapter.RccHistoryNewsAdapter
import mobi.eup.easykorean.view.adapter.RccNewsAdapter

class Utils {

    companion object {
        @JvmStatic
        @BindingAdapter("loadImage")
        fun loadImage(iv: ImageView, link: String?) {
            if (link == null || link == "") {
                Glide.with(iv)
                    .load(R.drawable.bg_easy_korean)
                    .into(iv)
            } else {
                Glide.with(iv)
                    .load(link)
                    .centerCrop()
                    .placeholder(R.drawable.bg_easy_korean)
                    .dontAnimate()
                    .into(iv)
            }
        }

        @JvmStatic
        @BindingAdapter("notifyDataChange")
        fun notifyDataChange(rc: RecyclerView, data: MutableList<ItemFeed>?) =
            (rc.adapter as RccNewsAdapter).setAdapterData(data ?: mutableListOf())


        @JvmStatic
        @BindingAdapter("notifyDataChangeHistoryNews")
        fun notifyDataChangeHistoryNews(rc: RecyclerView, data: MutableList<ItemFeed>?) {
            (rc.adapter as RccHistoryNewsAdapter).setAdapterData(data ?: mutableListOf())
        }

        @JvmStatic
        @BindingAdapter("getCorrectForTextView")
        fun getCorrectForTextView(textView: TextView, origin: String) {
            var detailContent: String = origin.trim()
            detailContent = detailContent.replace("▶", ".")
            detailContent = detailContent.replace("※", ".")
            detailContent = detailContent.replace("..", ".")
            detailContent = detailContent.replace('ⓒ', '.')
            detailContent = detailContent.replace(">>", "")
            textView.text = detailContent
        }

        fun getCorrectForHtml(origin: String): String {
            var detailContent: String = origin.trim()
            detailContent = detailContent.replace("▶", ".")
                .replace("※", ".")
                .replace("..", ".")
                .replace('ⓒ', '.')
                .replace(">>", "")
                .replace(" . ", ".")
                .replace("(\\w{3})\\.\\s*(\\d+),\\s*(\\d+)".toRegex(), "$1/$2/$3")
                .replace(". ", ".<br>")
            return detailContent
        }

        fun getCorrectForSplit(origin: String): String {
            var detailContent: String = origin.trim()
            detailContent = detailContent.replace("▶", ".")
                .replace("※", ".")
                .replace("..", ".")
                .replace('ⓒ', '.')
                .replace(">>", "")
                .replace(" . ", ".")
                .replace("(\\w{3})\\.\\s*(\\d+),\\s*(\\d+)".toRegex(), "$1/$2/$3")
            return detailContent
        }
    }
}
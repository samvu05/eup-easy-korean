package mobi.eup.easykorean.utils.translate

import android.annotation.SuppressLint
import android.content.Context
import android.provider.Settings
import mobi.eup.easykorean.utils.language.StringHelper
import mobi.eup.easykorean.data.model.Datum
import java.io.IOException
import java.util.*

/**
 * Created by Dinh Sam Vu on 1/18/2021.
 */
class HtmlHelper(context: Context) {
    // webview text color
    companion object {
        const val rubyColor = "#6D6D6D"
        const val backgroundColorTextarea = "rgba(255,255,255,1)"
        private const val linkColor = "#387ef5"
        private const val bodyColor = "rgba(0,0,0,.81)"
        private const val dateColor = "rgba(0,0,0,.71)"
        private const val titleColor = "rgba(0,0,0,.91)"

        private const val tagLiJa = "<li class = \"ja\">%s</li><br>"
        private const val tagLiOther = "<li class = \"other\">%s</li><br>"
        private const val tagTexarea =
            "<textarea rows=\"3\" cols=\"50\" name=\"%d\" autocorrect=\"off\" autocapitalize=\"on\" onchange=\"onInputChange(%d)\" onfocus=\"onFocusChange(%d)\" oninput='this.style.height = \"\";this.style.height = this.scrollHeight + \"px\"'>%s</textarea><br>"

    }

    private var newsTranslateHtml: String = ""

    init {
        getNewsTranslateHtmlString(context)
    }

    private fun getNewsTranslateHtmlString(context: Context) {
        try {
            newsTranslateHtml = StringHelper.getStringFromAsset(context, "html/newTranslate.html")
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }


    @SuppressLint("DefaultLocale", "HardwareIds")
    fun convertDict2Html(
        context: Context,
        datas: Datum?,
        contentNews: List<String>,
        isUpdate: Boolean,
    ): String {
//        val fontSize = preferenceHelper.getFontSize();
        val fontSize = 18
        val stringBuffer = StringBuilder()
        if (datas == null) { // tạo
            for (i in contentNews.indices) {
                stringBuffer.append(String.format(tagLiJa, contentNews[i] + ". "))
                stringBuffer.append(String.format(tagTexarea, i, i, i, ""))
            }
        } else {
            val android_id =
                Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
            val hashMap: HashMap<Int, String> = StringHelper.getContentTranslate(datas.content)
            if (isUpdate && datas.uuid.equals(android_id)) {
                for (i in contentNews.indices) {
                    stringBuffer.append(
                        String.format(
                            tagLiJa,
                            contentNews[i] + "."
                        )
                    )
                    stringBuffer.append(
                        String.format(
                            tagTexarea, i, i, i,
                            if (hashMap.containsKey(i)) hashMap[i]!!
                                .trim { it <= ' ' }.replace("^\"".toRegex(), "")
                                .replace("\"$".toRegex(), "") else ""
                        )
                    )
                }
            } else {
//                val emojiFlag =
//                    "\uD83C\uDFF4\uDB40\uDC67\uDB40\uDC62\uDB40\uDC65\uDB40\uDC6E\uDB40\uDC67\uDB40\uDC7F"
                val emojiFlag =
                    "\uD83C\uDDFB\uD83C\uDDF3"
                for (i in contentNews.indices) {
                    stringBuffer.append(
                        String.format(
                            tagLiJa,
                            contentNews[i] + "."
                        )
                    )
                    stringBuffer.append(
                        String.format(
                            tagLiOther,
                            "$emojiFlag " + if (hashMap.containsKey(i)) hashMap[i]!!
                                .trim { it <= ' ' }.replace("^\"".toRegex(), "")
                                .replace("\"$".toRegex(), "") else ""
                        )
                    )
                }
            }
        }
        return newsTranslateHtml.replace("<nguyenthelinh>", stringBuffer.toString())
            .replace("<myFontSize>".toRegex(), fontSize.toString())
            .replace("<bodyColor>".toRegex(), bodyColor)
            .replace("<linkColor>".toRegex(), linkColor)
            .replace("<rubyColor>".toRegex(), rubyColor)
            .replace("<backgroundColorTextarea>".toRegex(), backgroundColorTextarea)
    }

}
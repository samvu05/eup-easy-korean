package mobi.eup.easykorean.utils.app

/**
 * Created by Dinh Sam Vu on 1/20/2021.
 */
class GlobalHelper {

    companion object {
        const val DEFAULT_TEXT_BODY_EMPTY = "내용이 없습니다. 다시 시도하십시오."
        const val PREFERENCE_NAME_APP = "eup.mobi.easykorean"
        const val ITEM_ID_EXTRA = "item.id"
        const val ITEM_TYPE_EXTRA = "item.type"
        const val DEFAULT_APP_IMG_LINK = "https://i.imgur.com/T7mihUN.jpg"
        const val LOGCAT_TAG = "logDB"

        const val DEFAULT_PAGE_NEWS_COUNT = 10
        const val DEFAULT_FONT_SIZE = 18
        const val DEFAULT_ADPRESS = 3600000
        const val DEFAULT_INTERVALADSINTER = 300000

        const val banner = "banner"
        const val interstitial = "interstitial"
        const val deviceId = "deviceId"
        const val adpress = "adpress"
        const val intervalAdsInter = "intervalAdsInter"

        const val idBanner = "idBanner"
        const val DEFAULT_ID_BANNER = "ca-app-pub-8268370626959195/2315571469"
//        const val DEFAULT_ID_BANNER = "ca-app-pub-3940256099942544/6300978111"

        const val idInterstitial = "idInterstitial"
        const val DEFAULT_ID_INTER = "ca-app-pub-8268370626959195/6745771062"
//        const val DEFAULT_ID_INTER = "ca-app-pub-3940256099942544/1033173712"

        const val idNativeAds = "idNativeAds"
        const val DEFAULT_ID_NATIVE = "ca-app-pub-8268370626959195/7461892163"
//        const val DEFAULT_ID_NATIVE = "ca-app-pub-3940256099942544/2247696110"

        const val lastTimeClickAds = "lastTimeClickAds"
        const val lastTimeShowAdsInter = "lastTimeShowAdsInter"

        const val GOOGLE_URL_TRANSLATE =
            "https://translate.googleapis.com/translate_a/single?client=gtx&dt=t&dt=bd&dj=1&dt=ex&dt=ld&dt=md&dt=qca&dt=rw&dt=rm&dt=ss&sl=%s&tl=%s&q=%s" // from, to, query
        var GOOGLE_URL_IMAGE = "https://www.google.com/search?source=lnms&tbm=isch&q=%s" //query

        var isPremium = "isPremium"
        var isPremiumDay = "isPremiumDay"
        var showUnderlineHighlightLevel = "showUnderlineHighlightLevel"
        var isNightMode = "cb_black"
        var isAutoNightMode = "autoNightMode"
        var enableClickHighlight = "enableClickHighlight"
        var fontSize = "fontSize"

        var positionLanguage = "positionLanguage"
        var isMachineVoice = "isMachineVoice"
        var talkId = "talkId"
        var audio_speed = "spinner"
        var replay_audio = "replay"
        var typePlayAudioManager = "typePlayAudioManager"
        var SortLevelWordReview = "SortLevelWordReview"
        var ShowLevelWordReview = "ShowLevelWordReview"

        var existYoutube = "existYoutube"

        var card_tutorial = "card_tutorial"
        var tip_favorite = "TIP_FAVORITE"
        var tip_example = "TIP_EXAMPLE"
        var tip_machine_void = "tip_machine_void"

        var tip_main = "tip_main"
        var tip_news = "tip_news"
        var tip_listen_repeat = "tip_listen_repeat"
        var tip_word_review = "tip_word_review"
        var tip_translate = "tip_translate"

        var USER_NAME = "USER_NAME"
        var USER_NAME_DEFAULT = "EASY JAPANESE"

        var PREFERENCE_NAME_RATE_EJ = "RATE_EJ"
        var NUM_OPEN_APP = "NUM_OPEN_APP"
        var typeNotification = "typeNotification"
        var CLICK_ADS_MAZII = "CLICK_ADS_MAZII"

        const val SKU_PREMIUM = "com.mobi.eup.konews.removeads"
        const val SKU_SUB3 = "com.mobi.eup.konews.sub3"
        const val SKU_SUB12 = "com.mobi.eup.konews.sub12"

        const val USER_AGENT =
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36"

        var isExistDBGrammar = "isExistDBGrammar"
        var RandomWord = "RandomWord"

        var NHK_DICT_URL = " https://www3.nhk.or.jp/news/easy/%s/%s.out.dic?date=%d"
        var PLAY_STORE_URL = "https://play.google.com/store/apps/details?id="
        var VOIKY_PLAYSTORE_URL = PLAY_STORE_URL + "com.eup.japanvoice"
        var MYTEST_PLAYSTORE_URL = PLAY_STORE_URL + "com.eup.mytest"
        var JEDICT_PLAYSTORE_URL = PLAY_STORE_URL + "com.mazii.dictionary"
        var EASY_PLAYSTORE_URL = PLAY_STORE_URL + "mobi.eup.jpnews&hl=en"

        var FILTER_SOURCE_NEWS = "FilterSourceNews"
        var FILTER_TOPIC_NEWS = "FilterTopicNews"
        var OFFLINE_DICT = "OfflineDictionary"
        var BADGE_JLPT = "BadgeJlpt"

        var TOTAL_NEWS = "TOTAL_NEWS"
        var GG_IMG_PATTERN = "GG_IMG_PATTERN"

        // api youtube
        var API_KEY = "AIzaSyBh-bFCHXt1QDv6Ap0kUgrTBhUFaAPTMuA"

        // url video for video fragment
        var URL_GET_SONGS_SONG = "http://pikasmart.com/api/Songs/song?song_id=%d"

        // url list singer
        var URL_GET_LIST_SINGER = "http://pikasmart.com/api/Singers/listsinger?limit=%d"
        var URL_GET_INFO_SINGER = "http://pikasmart.com/api/Singers/infor?id_singer=%d"
        var URL_GET_TOP_VIDEO =
            "http://pikasmart.com/api/Singers/topvideo?limit=%d&skip=%d&id_singer=%s"

        // url list recommended
        var URL_GET_LIST_RECOMMEND =
            "http://pikasmart.com/api/Songs/recommend?limit=%d&video_type=%s"

        //album
        var URL_GET_LIST_ALBUM =
            "http://pikasmart.com/api/albums/listAlbum?limit=%d&skip=%d&type_album=%d"
        var URL_GET_LIST_SONG_ALBUM =
            "http://pikasmart.com/api/albumSongs/listSongInAlbum?album_id=%d&limit=%d&skip=%d"

        // new
        var URL_GET_LIST_NEW_SONG =
            "http://pikasmart.com/api/Songs/listnew?limit=%d&skip=%d&video_type=%s"

        // pi chart
        var URL_GET_VIDEO_CHART =
            "http://pikasmart.com/api/Songs/topViewVideo?video_type=%s&limit=%d&type=%d&skip=%d"

        // video hot
        var URL_GET_TOP_VIDEO_VIEW =
            "http://pikasmart.com/api/Songs/getTopViewVideoPerNew?video_type=%s&limit=%d&type=%d&skip=%d"

        // lyric
        var URL_GET_LYRIC_SENTENCE =
            "http://pikasmart.com/api/SongSentences/getListLyricsBylanguageCode?song_id=%d&language_code=%s"

        // list word
        var URL_GET_LIST_WORD =
            "http://pikasmart.com/api/words/listWord?song_id=%d&language_code=%s"

        var PIKA_SMART_API = "http://pikasmart.com/api/"

        // Date
        var ONE_DAY_MILIS: Long = 86400000
        var NUM_LIMIT_NEWS = 10

    }
}
package mobi.eup.easykorean.utils.app

import android.content.Context
import android.content.SharedPreferences
import mobi.eup.easykorean.utils.language.LanguageHelper

open class PreferenceHelper(private val context: Context, name: String) {
    private val sharedPreferences: SharedPreferences

    init {
        sharedPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE)
    }

    // MARK : Font size
    open fun getFontSize(): Int = sharedPreferences.getInt("fontSize", 19)
    open fun setFontSize(newValue: Int) {
        sharedPreferences.edit().putInt("fontSize", newValue).apply()
    }

    // MARK : Premium State
    open fun isPremium(): Boolean = sharedPreferences.getBoolean(GlobalHelper.isPremium, false)
    open fun setPremium(newValue: Boolean) {
        sharedPreferences.edit().putBoolean(GlobalHelper.isPremium, newValue).apply()
    }

    // MARK: premium day
    open fun isPremiumDay(): Boolean {
        return getPremiumDay() > System.currentTimeMillis()
    }

    open fun getPremiumDay(): Long {
        return sharedPreferences.getLong(GlobalHelper.isPremiumDay, 0)
    }

    open fun setPremiumDay(time: Long?) {
        sharedPreferences.edit().putLong(GlobalHelper.isPremiumDay, time!!).apply()
    }

    open fun isPremiumUser(): Boolean {
        return isPremium() || isPremiumDay()
    }


    // MARK: alert tip trick
    open fun showedTipMain(): Boolean {
        return sharedPreferences.getBoolean(GlobalHelper.tip_main, false) ?: false
    }

    open fun setShowedTipMain(newValue: Boolean) {
        sharedPreferences.edit().putBoolean(GlobalHelper.tip_main, newValue).apply()
    }

    // MARK : Is audio replay
    fun isReplayAudio(): Boolean = sharedPreferences.getBoolean("audioreplay", false)
    open fun setReplayAudio(newValue: Boolean) {
        sharedPreferences.edit().putBoolean("audioreplay", newValue).apply()
    }

    // MARK : Speak Voice
    open fun getSpeakerID(): Int = sharedPreferences.getInt("speaker", 14)
    open fun setSpeakerID(newValue: Int) {
        sharedPreferences.edit().putInt("speaker", newValue).apply()
    }

    // Mark : Local name
    open fun getLocalName(): String? = sharedPreferences.getString("localName", "Anonymous")
    open fun setLocalName(newValue: String) =
        sharedPreferences.edit().putString("localName", newValue).apply()


    // MARK : Notification
    open fun isNotification(): Boolean = sharedPreferences.getBoolean("notification", false)
    open fun setNotification(newValue: Boolean) {
        sharedPreferences.edit().putBoolean("notification", newValue).apply()
    }

    // MARK: night mode
    open fun isNightMode(): Boolean {
        return sharedPreferences.getBoolean(
            GlobalHelper.isNightMode,
            false
        )
    }

    open fun setNightMode(newValue: Boolean?) {
        sharedPreferences.edit().putBoolean(GlobalHelper.isNightMode, newValue!!).apply()
    }

    open fun isEnglishOrKorean(): Boolean {
        return getCurrentLanguageCode() == "en" || getCurrentLanguageCode() == "ko"
    }

    open fun isEnglishOrViet(): Boolean {
        return getCurrentLanguageCode() == "en" || getCurrentLanguageCode() == "vi"
    }

    open fun getCurrentLanguageCode(): String {
        return LanguageHelper.LIST_LANGUAGE.get(
            getCurrentLanguagePosition()
        ).get(0)
    }

    open fun getCurrentLanguagePosition(): Int {
        return sharedPreferences?.getInt(
            GlobalHelper.positionLanguage,
            LanguageHelper.getDefaultPositionLanguage(context)
        )
            ?: 19 // english
    }

    open fun getCurrentFlag(): String {
        return LanguageHelper.LIST_LANGUAGE[getCurrentLanguagePosition()][4] //english
    }

    /**
     * kiem tra xem co phai dang cai dat tieng viet khong
     */
    open fun isVietNamese(): Boolean {
        return getCurrentLanguageCode() == "vi"
    }

    /**
     * MARK: update config ads
     *
     * @return
     */
    open fun getBanner(): Float {
        return try {
            sharedPreferences.getFloat(GlobalHelper.banner, 1f)
        } catch (e: ClassCastException) {
            1f
        }
    }

    open fun setBanner(newValue: Float) {
        sharedPreferences.edit().putFloat(GlobalHelper.banner, newValue).apply()
    }

    open fun getInterstitial(): Float {
        return sharedPreferences.getFloat(
            GlobalHelper.interstitial,
            1f
        )
    }

    open fun setInterstitial(newValue: Float) {
        sharedPreferences.edit().putFloat(GlobalHelper.interstitial, newValue).apply()
    }

    open fun getAdpress(): Int {
        return sharedPreferences.getInt(
            GlobalHelper.adpress,
            GlobalHelper.DEFAULT_ADPRESS
        )
    }

    open fun setAdPress(newValue: Int) {
        sharedPreferences.edit().putInt(GlobalHelper.adpress, newValue).apply()
    }

    open fun getIntervalAdsInter(): Int {
        return sharedPreferences.getInt(
            GlobalHelper.intervalAdsInter,
            GlobalHelper.DEFAULT_INTERVALADSINTER
        )
    }

    open fun setIntervalAdsInter(newValue: Int) {
        sharedPreferences.edit().putInt(GlobalHelper.intervalAdsInter, newValue).apply()
    }

    open fun getIdBanner(): String? {
        return sharedPreferences.getString(
            GlobalHelper.idBanner,
            GlobalHelper.DEFAULT_ID_BANNER
        )
    }

    open fun setIdBanner(newValue: String?) {
        sharedPreferences.edit().putString(GlobalHelper.idBanner, newValue).apply()
    }

    open fun getIdInter(): String? {
        return sharedPreferences.getString(
            GlobalHelper.idInterstitial,
            GlobalHelper.DEFAULT_ID_INTER
        )
    }

    open fun setIdInter(newValue: String?) {
        sharedPreferences.edit().putString(GlobalHelper.idInterstitial, newValue).apply()
    }

    open fun getIdNative(): String? {
        return sharedPreferences.getString(
            GlobalHelper.idNativeAds,
            GlobalHelper.DEFAULT_ID_NATIVE
        )
    }

    open fun setIdNative(newValue: String?) {
        sharedPreferences.edit().putString(GlobalHelper.idNativeAds, newValue).apply()
    }


    open fun getLastTimeClickAds(): Long {
        return sharedPreferences.getLong(
            GlobalHelper.lastTimeClickAds,
            0
        )
    }

    open fun setLastTimeClickAds(time: Long) {
        sharedPreferences.edit().putLong(GlobalHelper.lastTimeClickAds, time).apply()
    }

    open fun getLastTimeShowAdsInter(): Long {
        return sharedPreferences.getLong(
            GlobalHelper.lastTimeShowAdsInter,
            0
        )
    }

    open fun setLastTimeShowAdsInter(time: Long) {
        sharedPreferences.edit().putLong(GlobalHelper.lastTimeShowAdsInter, time).apply()
    }

}
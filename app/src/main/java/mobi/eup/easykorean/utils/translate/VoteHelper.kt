@file:Suppress("DEPRECATION")

package mobi.eup.easykorean.utils.translate

import android.annotation.SuppressLint
import android.content.Context
import android.os.AsyncTask
import android.provider.Settings
import mobi.eup.easykorean.event.BooleanCallback
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import okhttp3.Response
import java.io.IOException

/**
 * Created by Dinh Sam Vu on 1/19/2021.
 */
@Suppress("DEPRECATION")
open class VoteHelper(mContext: Context, mOnPostExecute: BooleanCallback) : AsyncTask<String, Void, Boolean>() {
    private val onPostExecute: BooleanCallback = mOnPostExecute
    private val context: Context = mContext
    private val client = OkHttpClient()


    @SuppressLint("HardwareIds")
    override fun doInBackground(vararg strings: String?): Boolean {
        val androiId =
            Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
        val body = RequestBody.create(null, ByteArray(0))
        val value = strings[0]!!.toInt()
        val url = String.format("http://api.mazii.net/ko/api/ratting?uuid=%s&&rate=%s&&news_id=%s&createid=%s",
            androiId,
            value,
            strings[1],
            strings[2])
        val request = Request.Builder()
            .post(body)
            .url(url)
            .build()
        val response: Response?
        try {
            response = client.newCall(request).execute()
            return response.isSuccessful
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return false
    }

    override fun onPostExecute(aBoolean: Boolean) {
        super.onPostExecute(aBoolean)
        onPostExecute.execute(aBoolean)
    }
}
package mobi.eup.easykorean.utils.view

import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView

/**
 * Created by Dinh Sam Vu on 1/25/2021.
 */
class UIHelper {
    companion object {
        fun imageViewSetColorFilter(
            images: Array<ImageView>,
            isNight: Boolean,
            colorNight: Int,
            colorDefault: Int
        ) {
            for (image in images) {
                image.setColorFilter(if (isNight) colorNight else colorDefault)
            }
        }

        fun textViewSetColor(
            textViews: Array<TextView>,
            isNight: Boolean,
            colorNight: Int,
            colorDefault: Int
        ) {
            for (textView in textViews) {
                textView.setTextColor(if (isNight) colorNight else colorDefault)
            }
        }

        fun viewSetBackgroundColor(
            views: Array<View>,
            isNight: Boolean,
            colorNight: Int,
            colorDefault: Int
        ) {
            for (view in views) {
                view.setBackgroundColor(if (isNight) colorNight else colorDefault)
            }
        }

        fun buttonSetTextColor(
            buttons: Array<Button>,
            isNight: Boolean,
            colorNight: Int,
            colorDefault: Int
        ) {
            for (button in buttons) {
                button.setTextColor(if (isNight) colorNight else colorDefault)
            }
        }
    }
}
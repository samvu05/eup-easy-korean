package mobi.eup.easykorean.utils.translate;

import android.content.Context;

import mobi.eup.easykorean.R;

import java.util.HashMap;

public class KindMapHelper {

    private static HashMap<String, String> hashMap;

    private static void init(Context context) {
        hashMap = new HashMap<>();
        hashMap.put("abbr", context.getString(R.string.abbr));
        hashMap.put("adj", context.getString(R.string.adj));
        hashMap.put("adj-i", context.getString(R.string.adj_i));
        hashMap.put("adj-na", context.getString(R.string.adj_na));
        hashMap.put("adj-no", context.getString(R.string.adj_no));
        hashMap.put("adj-pn", context.getString(R.string.adj_pn));
        hashMap.put("adj-s", context.getString(R.string.adj_s));
        hashMap.put("adj-t", context.getString(R.string.adj_t));
        hashMap.put("adv", context.getString(R.string.adv));
        hashMap.put("adv-n", context.getString(R.string.adv_n));
        hashMap.put("adv-to", context.getString(R.string.adv_to));
        hashMap.put("arch", context.getString(R.string.arch));
        hashMap.put("ateji", context.getString(R.string.ateji));
        hashMap.put("aux", context.getString(R.string.aux));
        hashMap.put("aux-v", context.getString(R.string.aux_v));
        hashMap.put("aux-adj", context.getString(R.string.aux_adj));
        hashMap.put("Buddh", context.getString(R.string.Buddh));
        hashMap.put("chn", context.getString(R.string.chn));
        hashMap.put("col", context.getString(R.string.col));
        hashMap.put("comp", context.getString(R.string.comp));
        hashMap.put("conj", context.getString(R.string.conj));
        hashMap.put("derog", context.getString(R.string.derog));
        hashMap.put("ek", context.getString(R.string.ek));
        hashMap.put("exp", context.getString(R.string.exp));
        hashMap.put("fam", context.getString(R.string.fam));
        hashMap.put("fem", context.getString(R.string.fem));
        hashMap.put("food", context.getString(R.string.food));
        hashMap.put("geom", context.getString(R.string.geom));
        hashMap.put("gikun", context.getString(R.string.gikun));
        hashMap.put("gram", context.getString(R.string.gram));
        hashMap.put("hon", context.getString(R.string.hon));
        hashMap.put("hum", context.getString(R.string.hum));
        hashMap.put("id", context.getString(R.string.id));
        hashMap.put("int", context.getString(R.string.int_));
        hashMap.put("iK", context.getString(R.string.iK));
        hashMap.put("ik", context.getString(R.string.ik));
        hashMap.put("io", context.getString(R.string.io));
        hashMap.put("iv", context.getString(R.string.iv));
        hashMap.put("kyb", context.getString(R.string.kyb));
        hashMap.put("ksb", context.getString(R.string.ksb));
        hashMap.put("ktb", context.getString(R.string.ktb));
        hashMap.put("ling", context.getString(R.string.ling));
        hashMap.put("MA", context.getString(R.string.MA));
        hashMap.put("male", context.getString(R.string.male));
        hashMap.put("math", context.getString(R.string.math));
        hashMap.put("mil", context.getString(R.string.mil));
        hashMap.put("m-sl", context.getString(R.string.m_sl));
        hashMap.put("n", context.getString(R.string.n));
        hashMap.put("n-adv", context.getString(R.string.n_adv));
        hashMap.put("n-pref", context.getString(R.string.n_pref));
        hashMap.put("n-suf", context.getString(R.string.n_suf));
        hashMap.put("n-t", context.getString(R.string.n_t));
        hashMap.put("neg", context.getString(R.string.neg));
        hashMap.put("neg-v", context.getString(R.string.neg_v));
        hashMap.put("obs", context.getString(R.string.obs));
        hashMap.put("obsc", context.getString(R.string.obsc));
        hashMap.put("oK", context.getString(R.string.oK));
        hashMap.put("ok", context.getString(R.string.ok_));
        hashMap.put("osk", context.getString(R.string.osk));
        hashMap.put("physics", context.getString(R.string.physics));
        hashMap.put("pol", context.getString(R.string.pol));
        hashMap.put("pref", context.getString(R.string.pref));
        hashMap.put("prt", context.getString(R.string.prt));
        hashMap.put("qv", context.getString(R.string.qv));
        hashMap.put("rare", context.getString(R.string.rare));
        hashMap.put("sl", context.getString(R.string.sl));
        hashMap.put("suf", context.getString(R.string.suf));
        hashMap.put("tsb", context.getString(R.string.tsb));
        hashMap.put("uK", context.getString(R.string.uK));
        hashMap.put("uk", context.getString(R.string.uk));
        hashMap.put("v", context.getString(R.string.v));
        hashMap.put("v1", context.getString(R.string.v1));
        hashMap.put("v5aru", context.getString(R.string.v5aru));
        hashMap.put("v5b", context.getString(R.string.v5b));
        hashMap.put("v5g", context.getString(R.string.v5g));
        hashMap.put("v5k", context.getString(R.string.v5k));
        hashMap.put("v5k-s", context.getString(R.string.v5k_s));
        hashMap.put("v5m", context.getString(R.string.v5m));
        hashMap.put("v5n", context.getString(R.string.v5n));
        hashMap.put("v5r", context.getString(R.string.v5r));
        hashMap.put("v5r-i", context.getString(R.string.v5r_i));
        hashMap.put("v5s", context.getString(R.string.v5s));
        hashMap.put("v5t", context.getString(R.string.v5t));
        hashMap.put("v5u", context.getString(R.string.v5u));
        hashMap.put("v5u-s", context.getString(R.string.v5u_s));
        hashMap.put("v5uru", context.getString(R.string.v5uru));
        hashMap.put("vi", context.getString(R.string.vi));
        hashMap.put("vk", context.getString(R.string.vk));
        hashMap.put("vs", context.getString(R.string.vs));
        hashMap.put("vs-i", context.getString(R.string.vs_i));
        hashMap.put("vt", context.getString(R.string.vt));
        hashMap.put("vulg", context.getString(R.string.vulg));
        hashMap.put("vz", context.getString(R.string.vz));
        hashMap.put("X", context.getString(R.string.X));
        hashMap.put("v5", context.getString(R.string.v5));
        hashMap.put("ng", context.getString(R.string.ng));
    }

    public static String getKind(String key, Context context) {
        if (hashMap == null) {
            init(context);
        }

        String result = key;

        if (hashMap.get(key) != null) {
            result = hashMap.get(key);
        }

        return result;
    }
}
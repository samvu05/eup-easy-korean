package mobi.eup.easykorean.utils.language

import android.content.Context
import android.text.Html
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.util.*
import java.util.regex.Pattern

/**
 * Created by Dinh Sam Vu on 1/18/2021.
 */
@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS", "DEPRECATION",
    "NAME_SHADOWING"
)
open class StringHelper {
    companion object {

        // lay cach doc bai bao de tao audio doc luu offline
        fun html2Phonetic(html: String): String? {
            var html = html
            html = html.replace("<ruby>(\\d+?)<rt></rt></ruby>".toRegex(), "$1")
                .replace("<ruby.*?<rt>(.+?)</rt></ruby>".toRegex(), "$1")
            return Html.fromHtml(html).toString()
        }

        /*
            get string from asset file
        */
        @Throws(IOException::class)
        @JvmStatic
        fun getStringFromAsset(context: Context, path: String?): String {
            val buf = StringBuilder()
            val json = context.assets.open(path!!)
            val inPut = BufferedReader(InputStreamReader(json, "UTF-8"))
            var str: String?
            try {
                while (inPut.readLine().also { str = it } != null) {
                    buf.append(str)
                }
            } catch (e: OutOfMemoryError) {
                e.printStackTrace()
                return ""
            }
            inPut.close()
            return buf.toString()
        }


        fun getContentTranslate(content: String): HashMap<Int, String> {
            val hashMap = HashMap<Int, String>()
            if (content.length <= 2) return hashMap
            val str =
                content.substring(0, content.length - 2).replaceFirst("\\{\"\\d+\":".toRegex(), "")
            val listContent: List<String> =
                ArrayList(Arrays.asList(*str.split(",\"\\d+\":".toRegex()).toTypedArray()))
            val p = Pattern.compile("\"\\d+\":")
            val m = p.matcher(content)
            var i = 0
            while (m.find() && i < listContent.size) {
                val s = listContent[i]
                hashMap[m.group(0).replace(":", "").replace("\"".toRegex(), "").toInt()] = s
                i++
            }
            return hashMap
        }


        fun stringToHex(str: String): String? {
            var unicode = 0
            for (i in 0 until str.length) {
                unicode = unicode * 10 + str[i].toInt()
            }
            return Integer.toHexString(unicode)
        }


        fun convertStringToMilisecond(time: String?): Int {
            var milis = 0
            if (time == null) milis = -1 else {
                try {
                    val times = time.trim { it <= ' ' }.split(":".toRegex()).toTypedArray()
                    if (times.size == 3) { // string 00:00:00
                        val second = times[2].trim { it <= ' ' }.split(",".toRegex()).toTypedArray()
                        milis = if (second.size == 2) {
                            times[0].toInt() * 60 * 60 * 1000 + times[1].toInt() * 60 * 1000 + second[0].toInt() * 1000 + second[1].toInt()
                        } else {
                            times[0].toInt() * 60 * 60 * 1000 + times[1].toInt() * 60 * 1000 + second[0].toInt() * 1000
                        }
                    } else if (times.size == 2) { // string 00:00
                        val second = times[1].trim { it <= ' ' }.split(",".toRegex()).toTypedArray()
                        milis = if (second.size == 2) {
                            times[0].toInt() * 60 * 1000 + second[0].toInt() * 1000 + second[1].toInt()
                        } else {
                            times[0].toInt() * 60 * 1000 + second[0].toInt() * 1000
                        }
                    }
                } catch (ignored: NumberFormatException) {
                }
            }
            return milis
        }
    }

}
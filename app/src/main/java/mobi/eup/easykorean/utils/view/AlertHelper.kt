package mobi.eup.easykorean.utils.view

import android.content.Context
import android.content.DialogInterface
import android.os.Build
import androidx.appcompat.app.AlertDialog
import mobi.eup.easykorean.R
import mobi.eup.easykorean.event.VoidCallback

/**
 * Created by Dinh Sam Vu on 1/21/2021.
 */
class AlertHelper {
    companion object {
        @JvmStatic
        fun showYesNoAlert(
            context: Context?,
            icon: Int,
            title: String?,
            desc: String?,
            okCallback: VoidCallback?,
            cancelCallback: VoidCallback?
        ) {
            if (context == null) return
//            val preferenceHelper = PreferenceHelper(context, GlobalHelper.PREFERENCE_NAME_APP)
//            val isNight: Boolean = preferenceHelper.isNightMode()
            val isNight = true

            val builder: AlertDialog.Builder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                AlertDialog.Builder(
                    context,
                    if (isNight) R.style.AppThemeNight_AlertThemeNight else R.style.AppTheme_AlertTheme
                )
            } else {
                AlertDialog.Builder(context)
            }
            builder.setTitle(title)
                .setMessage(desc)
                .setPositiveButton(R.string.ok) { _, _ -> okCallback?.execute() }
                .setNegativeButton(R.string.cancel) { _, _ -> cancelCallback?.execute() }
                .setIcon(icon)
                .setCancelable(false)
                .show()
        }

        @JvmStatic
        fun showTipAlert(
            context: Context?,
            icon: Int,
            title: String?,
            desc: String?,
            voidCallback: VoidCallback?
        ) {
            if (context == null) return
//            val preferenceHelper = PreferenceHelper(context, GlobalHelper.PREFERENCE_NAME_APP)
//            val isNight: Boolean = preferenceHelper.isNightMode()
            val isNight = true

            val builder: AlertDialog.Builder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                AlertDialog.Builder(
                    context,
                    if (isNight) R.style.AppThemeNight_AlertThemeNight else R.style.AppTheme_AlertTheme
                )
            } else {
                AlertDialog.Builder(context)
            }
            builder.setTitle(title)
                .setMessage(desc)
                .setPositiveButton(R.string.ok) { _, _ -> voidCallback?.execute() }
                .setIcon(icon)
                .setCancelable(false)
                .show()
        }

        @JvmStatic
        fun showPremiumOnlyDialog(
            context: Context?,
            message: String?,
            voidCallback: VoidCallback?
        ) {
            if (context == null) return
//            val preferenceHelper = PreferenceHelper(context, GlobalHelper.PREFERENCE_NAME_APP)
//            val isNight: Boolean = preferenceHelper.isNightMode()
            val isNight = true

            val builder: AlertDialog.Builder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                AlertDialog.Builder(
                    context,
                    if (isNight) R.style.AppThemeNight_AlertThemeNight else R.style.AppTheme_AlertTheme
                )
            } else {
                AlertDialog.Builder(context)
            }
            val s = message ?: context.resources.getString(R.string.audio_manager_premium_only)
            builder.setTitle(context.resources.getString(R.string.oops))
                .setMessage(s)
                .setNeutralButton(
                    android.R.string.ok
                ) { _: DialogInterface?, _: Int -> }
            if (voidCallback != null) {
                builder.setPositiveButton(R.string.upgrade_now) { _, _ -> voidCallback.execute() }
            }
            builder.setIcon(R.drawable.img_premium)
                .setCancelable(false)
                .show()
        }

    }
}
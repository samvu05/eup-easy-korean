@file:Suppress("DEPRECATION")

package mobi.eup.easykorean.admob

import android.app.Activity
import android.graphics.Color
import android.util.Log
import android.view.View
import android.view.ViewTreeObserver
import android.widget.*
import com.google.android.gms.ads.*
import com.google.android.gms.ads.formats.MediaView
import com.google.android.gms.ads.formats.UnifiedNativeAd
import com.google.android.gms.ads.formats.UnifiedNativeAdView
import mobi.eup.easykorean.R
import mobi.eup.easykorean.utils.app.GlobalHelper
import mobi.eup.easykorean.utils.app.PreferenceHelper

/**
 * Created by Dinh Sam Vu on 2/25/2021.
 */
@Suppress("DEPRECATION")
class AdsNative(activity: Activity) {
    private val mActivity = activity
    private val bannerEvent: BannerEvent = mActivity as BannerEvent
    private val isPremium: Boolean
    var currentNativeAd: UnifiedNativeAd? = null

    private val preferenceHelper: PreferenceHelper =
        PreferenceHelper(activity, GlobalHelper.PREFERENCE_NAME_APP)

    init {
        isPremium = preferenceHelper.isPremiumUser()
    }

    fun showNativeAdsView(containerView: LinearLayout, isMedium: Boolean) {
        if (!isShowAble()) {
            return
        }

        val builder = AdLoader.Builder(mActivity, GlobalHelper.DEFAULT_ID_NATIVE)
        builder.forUnifiedNativeAd { unifiedNativeAd ->
            currentNativeAd?.destroy()
            currentNativeAd = unifiedNativeAd
            val adView = mActivity.layoutInflater
                .inflate(
                    if (isMedium) R.layout.layout_native_medium else R.layout.layout_native_banner,
                    null
                ) as UnifiedNativeAdView

            populateUnifiedNativeAdView(unifiedNativeAd, adView, isMedium)

            containerView.removeAllViews()
            containerView.addView(adView)
        }

        val videoOptions = VideoOptions.Builder()
            .setStartMuted(true)
            .build()

        val adOptions = com.google.android.gms.ads.formats.NativeAdOptions.Builder()
            .setVideoOptions(videoOptions)
            .build()

        builder.withNativeAdOptions(adOptions)

        val adLoader = builder.withAdListener(object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                containerView.visibility = View.VISIBLE
                containerView.setBackgroundColor(Color.WHITE)

                val observer = containerView.viewTreeObserver
                containerView.requestLayout()
                observer.addOnGlobalLayoutListener(object :
                    ViewTreeObserver.OnGlobalLayoutListener {
                    override fun onGlobalLayout() {
                        containerView.requestLayout()
                        if (!isMedium) {
                            bannerEvent.updateContentViewWithBanner(containerView.height)
                        }
                        containerView.viewTreeObserver.removeOnGlobalLayoutListener(this)
                        Log.d(GlobalHelper.LOGCAT_TAG, "Native Ads loaded")
                    }
                })
            }

            override fun onAdFailedToLoad(loadAdError: LoadAdError) {
                val error =
                    "domain: ${loadAdError.domain}, code: ${loadAdError.code}, message: ${loadAdError.message}"
                Log.d(GlobalHelper.LOGCAT_TAG, "Native Ads load error : $error")
            }

        }).build()

        adLoader.loadAd(AdRequest.Builder().build())
    }


    private fun populateUnifiedNativeAdView(
        nativeAd: UnifiedNativeAd,
        adView: UnifiedNativeAdView,
        isMedium: Boolean
    ) {
        adView.mediaView = adView.findViewById<MediaView>(R.id.ad_media)
        adView.headlineView = adView.findViewById(R.id.ad_headline)
        adView.bodyView = adView.findViewById(R.id.ad_body)
        adView.callToActionView = adView.findViewById(R.id.ad_call_to_action)
        adView.iconView = adView.findViewById(R.id.ad_app_icon)

        if (isMedium) {
            adView.priceView = adView.findViewById(R.id.ad_price)
            adView.starRatingView = adView.findViewById(R.id.ad_stars)
            adView.storeView = adView.findViewById(R.id.ad_store)
            adView.advertiserView = adView.findViewById(R.id.ad_advertiser)
        }
        (adView.headlineView as TextView).text = nativeAd.headline

        if (nativeAd.body == null) {
            adView.bodyView.visibility = View.INVISIBLE
        } else {
            adView.bodyView.visibility = View.VISIBLE
            (adView.bodyView as TextView).text = nativeAd.body
        }

        if (nativeAd.callToAction == null) {
            adView.callToActionView.visibility = View.INVISIBLE
        } else {
            adView.callToActionView.visibility = View.VISIBLE
            (adView.callToActionView as Button).text = nativeAd.callToAction
        }

        if (nativeAd.icon == null) {
            adView.iconView.visibility = View.GONE
        } else {
            (adView.iconView as ImageView).setImageDrawable(
                nativeAd.icon.drawable
            )
            adView.iconView.visibility = View.VISIBLE
        }

        if (isMedium) {
            if (nativeAd.price == null) {
                adView.priceView.visibility = View.INVISIBLE
            } else {
                adView.priceView.visibility = View.VISIBLE
                (adView.priceView as TextView).text = nativeAd.price
            }

            if (nativeAd.store == null) {
                adView.storeView.visibility = View.INVISIBLE
            } else {
                adView.storeView.visibility = View.VISIBLE
                (adView.storeView as TextView).text = nativeAd.store
            }

            if (nativeAd.starRating == null) {
                adView.starRatingView.visibility = View.INVISIBLE
            } else {
                (adView.starRatingView as RatingBar).rating = nativeAd.starRating!!.toFloat()
                adView.starRatingView.visibility = View.VISIBLE
            }

            if (nativeAd.advertiser == null) {
                adView.advertiserView.visibility = View.INVISIBLE
            } else {
                (adView.advertiserView as TextView).text = nativeAd.advertiser
                adView.advertiserView.visibility = View.VISIBLE
            }
        }

        adView.setNativeAd(nativeAd)
    }

    private fun isShowAble(): Boolean {
        if (preferenceHelper.isPremiumUser()) return false
        return true
    }
}
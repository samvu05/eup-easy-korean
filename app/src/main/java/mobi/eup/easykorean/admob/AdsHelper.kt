package mobi.eup.easykorean.admob

/**
 * Created by Dinh Sam Vu on 2/28/2021.
 */
class AdsHelper(mState: State) {
    private var state: State = mState

    fun getState(): State {
        return state
    }
    enum class State {
        REMOVE_ADS
    }
}
package mobi.eup.easykorean.admob

import android.content.Context
import android.graphics.Color
import android.view.View
import android.view.ViewTreeObserver
import android.widget.LinearLayout
import com.google.android.gms.ads.*
import mobi.eup.easykorean.utils.app.GlobalHelper
import mobi.eup.easykorean.utils.app.PreferenceHelper

/**
 * Created by Dinh Sam Vu on 2/25/2021.
 */
class AdsBanner(activity: Context) {
    private val mActivity = activity
    private val bannerEvent: BannerEvent
    private val isPremium: Boolean

    private val preferenceHelper: PreferenceHelper =
        PreferenceHelper(activity, GlobalHelper.PREFERENCE_NAME_APP)

    init {
        bannerEvent = mActivity as BannerEvent
        isPremium = preferenceHelper.isPremiumUser()
    }

    fun showBannerAdsView(containerView: LinearLayout, isMedium: Boolean) {
        if (!isShowAble()) {
            return
        }

        val params = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT
        )

        val mAdView = AdView(mActivity)
        mAdView.adSize = if (isMedium) AdSize.MEDIUM_RECTANGLE else AdSize.BANNER
        mAdView.adUnitId = GlobalHelper.DEFAULT_ID_BANNER
        mAdView.layoutParams = params

        containerView.addView(mAdView)
        containerView.visibility = View.GONE
        val adRequest = AdRequest.Builder().build()
        mAdView.loadAd(adRequest)

        mAdView.adListener = object : AdListener() {
            override fun onAdLoaded() {
                containerView.visibility = View.VISIBLE
                containerView.setBackgroundColor(Color.WHITE)

                val observer = containerView.viewTreeObserver
                containerView.requestLayout()
                observer.addOnGlobalLayoutListener(object :
                    ViewTreeObserver.OnGlobalLayoutListener {
                    override fun onGlobalLayout() {
                        containerView.requestLayout()
                        if (!isMedium) {
                            bannerEvent.updateContentViewWithBanner(containerView.height)
                        }
                        containerView.viewTreeObserver.removeOnGlobalLayoutListener(this)
                    }
                })
            }

            override fun onAdFailedToLoad(adError: LoadAdError) {
            }

            override fun onAdOpened() {
            }

            override fun onAdClicked() {
            }

            override fun onAdLeftApplication() {
            }

            override fun onAdClosed() {
            }
        }
    }

    private fun isShowAble(): Boolean {
        if (preferenceHelper.isPremiumUser()) return false
        return true
    }
}
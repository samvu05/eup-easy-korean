package mobi.eup.easykorean.admob

/**
 * Created by Dinh Sam Vu on 2/25/2021.
 */
interface BannerEvent {
    fun updateContentViewWithBanner(height : Int)
}
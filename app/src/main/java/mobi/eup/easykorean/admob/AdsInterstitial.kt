package mobi.eup.easykorean.admob

import android.app.Activity
import android.content.Context
import android.os.Bundle
import com.google.ads.mediation.admob.AdMobAdapter
import com.google.android.gms.ads.AdError
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.FullScreenContentCallback
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import mobi.eup.easykorean.utils.app.GlobalHelper
import mobi.eup.easykorean.utils.app.PreferenceHelper

/**
 * Created by Dinh Sam Vu on 2/25/2021.
 */
class AdsInterstitial(activity: Context) {
    private val mActivity = activity
    private var mInterstitialAd: InterstitialAd? = null
    private var euConsent = false
    private val isPremium: Boolean

    private val preferenceHelper: PreferenceHelper =
        PreferenceHelper(activity, GlobalHelper.PREFERENCE_NAME_APP)

    init {
        isPremium = preferenceHelper.isPremiumUser()
    }

    init {
        if (euConsent) {
            //User Consented
            createPersonalizedAd()
        } else {
            //User did not Consent
            //User is not in Eu
            createNonPersonalizedAd()
        }

    }

    fun showAds() {
        if (!isShowAble() || mInterstitialAd == null) {
            return
        }
        mInterstitialAd!!.show(mActivity as Activity)
    }

    private fun createPersonalizedAd() {
        val adRequest = AdRequest.Builder().build()
        createInterstitialAd(adRequest)
    }

    private fun createNonPersonalizedAd() {
        val networkExtrasBundle = Bundle()
        networkExtrasBundle.putInt("rdp", 1)
        val adRequest = AdRequest.Builder()
            .addNetworkExtrasBundle(AdMobAdapter::class.java, networkExtrasBundle)
            .build()
        createInterstitialAd(adRequest)
    }

    private fun createInterstitialAd(adRequest: AdRequest) {
        InterstitialAd.load(
            mActivity,
            GlobalHelper.DEFAULT_ID_INTER,
            adRequest,
            object : InterstitialAdLoadCallback() {
                override fun onAdFailedToLoad(adError: LoadAdError) {
                    mInterstitialAd = null
                }

                override fun onAdLoaded(interstitialAd: InterstitialAd) {
                    mInterstitialAd = interstitialAd

                    mInterstitialAd?.fullScreenContentCallback =
                        object : FullScreenContentCallback() {
                            override fun onAdDismissedFullScreenContent() {
                                if (euConsent) {
                                    createPersonalizedAd()
                                } else {
                                    createNonPersonalizedAd()
                                }
                            }

                            override fun onAdFailedToShowFullScreenContent(adError: AdError?) {
                            }

                            override fun onAdShowedFullScreenContent() {
                                mInterstitialAd = null
                            }
                        }
                }
            })
    }

    private fun isShowAble(): Boolean {
        if (preferenceHelper.isPremiumUser()) return false
        return true
    }
}
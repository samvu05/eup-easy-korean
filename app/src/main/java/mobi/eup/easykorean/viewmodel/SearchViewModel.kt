package mobi.eup.easykorean.viewmodel

import android.content.Context
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.*
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import mobi.eup.easykorean.database.repo.MyDatabaseRepo
import mobi.eup.easykorean.data.model.ItemFeed
import mobi.eup.easykorean.data.network.rest_support.postbody.PostSearchBody
import mobi.eup.easykorean.data.network.NewsRepo
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class SearchViewModel(context : Context) : ViewModel() {
    val listData = MutableLiveData<MutableList<ItemFeed>>()
    var isLoading = ObservableBoolean(false)
    var isEmpty = ObservableBoolean(false)
    private val mDatabaseRepo = MyDatabaseRepo(context)
    private val repository: NewsRepo
    private var newsSizeLimit = 100

    init {
        isLoading.set(true)
        isEmpty.set(false)
        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl("https://api.mazii.net")
            .build()
        repository = retrofitBuilder.create(NewsRepo::class.java)
    }

    fun getDataOnce(key: String, type: String) {
        isLoading.set(true)
        repository.getDataSearch(PostSearchBody(key, newsSizeLimit, type))
            .enqueue(object : Callback<MutableList<ItemFeed>> {
                override fun onResponse(
                    call: Call<MutableList<ItemFeed>>,
                    response: Response<MutableList<ItemFeed>>
                ) {
                    if (response.body() != null) {
                        listData.value = response.body()
                        isLoading.set(false)
                    }
                }

                override fun onFailure(call: Call<MutableList<ItemFeed>>, t: Throwable) {
                    isLoading.set(true)
                }
            })
    }

    fun taskInsertItem(itemFeed: ItemFeed) = viewModelScope.launch {
        mDatabaseRepo.taskFeedInsert(itemFeed)
    }

    fun taskUpdateSeen(id: String, isSeen: Int) = viewModelScope.launch {
        mDatabaseRepo.taskFeedUpdateIsSeen(id, isSeen)
    }

    fun checkExits(id: String): Boolean {
        var result = false
        viewModelScope.launch {
            val isExist = async {
                mDatabaseRepo.taskFeedCheckExist(id)
            }
            result = isExist.await()
        }
        return result
    }

    class ViewModelFactory(
        private val context: Context
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(SearchViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return SearchViewModel(context) as T
            }
            throw IllegalArgumentException("Unable construct viewmodel")
        }

    }
}

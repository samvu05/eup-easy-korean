package mobi.eup.easykorean.viewmodel

import android.content.Context
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import mobi.eup.easykorean.database.repo.MyDatabaseRepo
import mobi.eup.easykorean.data.model.ItemFeed
import mobi.eup.easykorean.data.network.rest_support.postbody.PostNewsBody
import mobi.eup.easykorean.data.network.NewsRepo
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class HardCurrentViewModel(context: Context) : ViewModel() {
    val listData = MutableLiveData<MutableList<ItemFeed>>()
    var isLoading = ObservableBoolean(false)
    var isLoadmore = ObservableBoolean(false)
    private val mDatabaseRepo = MyDatabaseRepo(context)

    private val repository: NewsRepo
    private val newsType = "normal"
    private var newsSizeLimit = 15

    init {
        isLoading.set(true)
        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl("https://api.mazii.net")
            .build()
        repository = retrofitBuilder.create(NewsRepo::class.java)
    }

    fun getData(page: Int) {
        if (page == 1) isLoading.set(true) else isLoadmore.set(true)

        repository.getDataNews(PostNewsBody(newsType, page, newsSizeLimit))
            .enqueue(object : Callback<MutableList<ItemFeed>> {
                override fun onResponse(
                    call: Call<MutableList<ItemFeed>>,
                    response: Response<MutableList<ItemFeed>>
                ) {
                    if (response.body() != null) {
                        listData.value = response.body()
                        isLoadmore.set(false)
                        isLoading.set(false)
                    }
                }

                override fun onFailure(call: Call<MutableList<ItemFeed>>, t: Throwable) {
                    if (page == 1) isLoading.set(true) else isLoadmore.set(true)
                }
            })
    }

    fun taskInsertItem(itemFeed: ItemFeed) = viewModelScope.launch {
        mDatabaseRepo.taskFeedInsert(itemFeed)
    }

    fun taskUpdateSeen(id: String, isSeen: Int) = viewModelScope.launch {
        mDatabaseRepo.taskFeedUpdateIsSeen(id, isSeen)
    }

    fun checkExits(id: String): Boolean {
        var result = false
        viewModelScope.launch {
            val isExist = async {
                mDatabaseRepo.taskFeedCheckExist(id)
            }
            result = isExist.await()
        }
        return result
    }
}
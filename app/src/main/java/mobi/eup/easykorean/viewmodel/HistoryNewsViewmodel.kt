package mobi.eup.easykorean.viewmodel

import android.content.Context
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import mobi.eup.easykorean.database.repo.MyDatabaseRepo
import mobi.eup.easykorean.data.model.ItemFeed

class HistoryNewsViewmodel(context: Context) {
    val listData = MutableLiveData<MutableList<ItemFeed>>()
    val isLoading = ObservableBoolean(false)
    private val repoDatabase: MyDatabaseRepo

    init {
        isLoading.set(true)
        repoDatabase = MyDatabaseRepo(context)
    }

    fun getAll() = repoDatabase.taskFeedGetListSeen()
}

package mobi.eup.easykorean.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import mobi.eup.easykorean.database.repo.MyDatabaseRepo
import kotlinx.coroutines.launch

/**
 * Created by Dinh Sam Vu on 1/22/2021.
 */
class BSHistoryContainerViewModel(context: Context) : ViewModel() {
    private val mDatabaseRepo = MyDatabaseRepo(context)

    fun deleteAllWordHistory() = viewModelScope.launch {
        mDatabaseRepo.taskWordHistoryDeleteAll()
    }

    fun deleteAllNewsHistory() = viewModelScope.launch {
        mDatabaseRepo.taskFeedDeleteAllSeen()
    }


    class ViewModelFactory(
        private val context: Context
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(BSHistoryContainerViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return BSHistoryContainerViewModel(context) as T
            }
            throw IllegalArgumentException("Unable construct viewmodel")
        }

    }

}
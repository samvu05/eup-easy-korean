package mobi.eup.easykorean.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import mobi.eup.easykorean.data.network.LinkMp3Repo
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory

/**
 * Created by Dinh Sam Vu on 1/27/2021.
 */
class ListenRepeatViewModel : ViewModel() {
    val linkMp3 = MutableLiveData<String>()
    private val linkMp3Repo: LinkMp3Repo

    init {
        val retrofitBuilderSpeechLink = Retrofit.Builder()
            .addConverterFactory(ScalarsConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl("https://readspeaker.jp/")
            .build()
        linkMp3Repo = retrofitBuilderSpeechLink.create(LinkMp3Repo::class.java)
    }

    fun getLinkMp3(textOrigin: String, talkID: Int) {
        linkMp3Repo.getSpeechLink(
            textOrigin,
            talkID, //JIHUN 18(MALE), HYERYUN 14(FMALE)
            100,
            85,
            100,
            2,
            0
        )
            .enqueue(object : Callback<String> {
                override fun onResponse(call: Call<String>, response: Response<String>) {
                    if (response.body() != null) {
                        val temp = response.body()!!.substring(5).trim()
                        linkMp3.value =
                            "https://readspeaker.jp/ASLCLCLVVS/JMEJSYGDCHMSMHSRKPJL/$temp"
                    }
                }

                override fun onFailure(call: Call<String>, t: Throwable) {
                }
            })
    }

    class ViewModelFactory : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(ListenRepeatViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return ListenRepeatViewModel() as T
            }
            throw IllegalArgumentException("Unable construct viewmodel")
        }
    }
}
package mobi.eup.easykorean.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import mobi.eup.easykorean.data.model.Datum
import mobi.eup.easykorean.data.model.DatumContainer
import mobi.eup.easykorean.data.network.rest_support.postbody.PostTranslateBody
import mobi.eup.easykorean.data.network.rest_support.response.ReturnPostTranslate
import mobi.eup.easykorean.data.network.TranslateRepo
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Dinh Sam Vu on 1/18/2021.
 */
class TranslateViewModel(private val onGetLinkSussces: ((String) -> Unit)) : ViewModel() {
    val datas = MutableLiveData<MutableList<Datum>>()
    private var repository: TranslateRepo

    init {
        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl("https://api.mazii.net")
            .build()
        repository = retrofitBuilder.create(TranslateRepo::class.java)
    }

    fun getDataOnce(id: String, language: String, deviceID: String) {
        repository.getTranslateData(id, language, deviceID)
            .enqueue(object : Callback<DatumContainer> {
                override fun onResponse(
                    call: Call<DatumContainer>,
                    response: Response<DatumContainer>,
                ) {
                    if (response.body() != null) {
                        datas.value = response.body()!!.data
                    }
                }

                override fun onFailure(call: Call<DatumContainer>, t: Throwable) {
                }
            })
    }

    fun postData(body: PostTranslateBody) {
        repository.postTranlateData(body).enqueue(object : Callback<ReturnPostTranslate> {
            override fun onResponse(
                call: Call<ReturnPostTranslate>,
                response: Response<ReturnPostTranslate>
            ) {
                if (response.body() != null) {
                    onGetLinkSussces("done")
                }
            }

            override fun onFailure(call: Call<ReturnPostTranslate>, t: Throwable) {
                Log.d("logDB", "update error")
            }
        })
    }


}

package mobi.eup.easykorean.viewmodel

import android.content.Context
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.*
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import mobi.eup.easykorean.database.repo.MyDatabaseRepo
import mobi.eup.easykorean.data.network.rest_support.postbody.PostNewsBody
import mobi.eup.easykorean.data.network.NewsRepo
import mobi.eup.easykorean.data.network.LinkMp3Repo
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import mobi.eup.easykorean.data.model.*
import mobi.eup.easykorean.data.network.TranslateRepo
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

class DetailViewModel(context: Context) : ViewModel() {
    private val mDatabaseRepo = MyDatabaseRepo(context)
    var isLoading = ObservableBoolean(false)

    val listData = MutableLiveData<MutableList<ItemFeed>>()
    val itemData = MutableLiveData<ItemFeed>()
    val newsLinkMp3 = MutableLiveData<String>()
    val wordLinkMp3 = MutableLiveData<String>()
    private var tempList = mutableListOf<ItemFeed>()
    private var newsCurrentPage = 1
    val listTranslate = MutableLiveData<MutableList<Datum>>()

    private var newsSizeLimit = 100
    private val newsRepo: NewsRepo
    private val linkMp3Repo: LinkMp3Repo
    private var translateRepo: TranslateRepo

    init {
        isLoading.set(true)
        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl("https://api.mazii.net")
            .build()
        newsRepo = retrofitBuilder.create(NewsRepo::class.java)
        translateRepo = retrofitBuilder.create(TranslateRepo::class.java)

        val retrofitBuilderSpeechLink = Retrofit.Builder()
            .addConverterFactory(ScalarsConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl("https://readspeaker.jp/")
            .build()
        linkMp3Repo = retrofitBuilderSpeechLink.create(LinkMp3Repo::class.java)
    }

    fun getListSuggest(itemType: String) {
        newsRepo.getDataNews(
            PostNewsBody(
                itemType,
                newsCurrentPage,
                newsSizeLimit
            )
        )
            .enqueue(object : Callback<MutableList<ItemFeed>> {
                override fun onResponse(
                    call: Call<MutableList<ItemFeed>>,
                    response: Response<MutableList<ItemFeed>>
                ) {
                    if (response.body() != null) {
                        isLoading.set(false)
                        for (responseItem in response.body()!!) {
                            if (mDatabaseRepo.asyncTaskGetSeenStatus(responseItem.id) == 0) {
                                tempList.add(responseItem)
                            }
                            if (tempList.size >= 5) {
                                listData.value = tempList
                                break
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<MutableList<ItemFeed>>, t: Throwable) {
                    isLoading.set(true)
                }
            })
    }

    fun getNewsLinkMp3(textOrigin: String, talkID: Int) {
        linkMp3Repo.getSpeechLink(
            textOrigin,
            talkID, //JIHUN 18(MALE), HYERYUN 14(FMALE)
            100,
            85,
            100,
            2,
            0
        )
            .enqueue(object : Callback<String> {
                override fun onResponse(call: Call<String>, response: Response<String>) {
                    if (response.body() != null) {
                        val temp = response.body()!!.substring(5).trim()
                        newsLinkMp3.value =
                            "https://readspeaker.jp/ASLCLCLVVS/JMEJSYGDCHMSMHSRKPJL/$temp"
                    }
                }

                override fun onFailure(call: Call<String>, t: Throwable) {
                }
            })
    }

    fun getWordLinkMp3(textOrigin: String, talkID: Int) {
        linkMp3Repo.getSpeechLink(
            textOrigin,
            talkID, //JIHUN 18(MALE), HYERYUN 14(FMALE)
            100,
            85,
            100,
            2,
            0
        )
            .enqueue(object : Callback<String> {
                override fun onResponse(call: Call<String>, response: Response<String>) {
                    if (response.body() != null) {
                        val temp = response.body()!!.substring(5).trim()
                        wordLinkMp3.value =
                            "https://readspeaker.jp/ASLCLCLVVS/JMEJSYGDCHMSMHSRKPJL/$temp"
                    }
                }

                override fun onFailure(call: Call<String>, t: Throwable) {
                }
            })
    }

    fun getDataTranslate(id: String, language: String, deviceID: String) {
        translateRepo.getTranslateData(id, language, deviceID)
            .enqueue(object : Callback<DatumContainer> {
                override fun onResponse(
                    call: Call<DatumContainer>,
                    response: Response<DatumContainer>,
                ) {
                    if (response.body() != null) {
                        listTranslate.value = response.body()!!.data
                    }
                }

                override fun onFailure(call: Call<DatumContainer>, t: Throwable) {
                }
            })
    }


    fun taskAudioInsert(itemAudio: ItemAudio) = viewModelScope.launch {
        mDatabaseRepo.taskAudioInsert(itemAudio)
    }

    fun taskAudioClear(itemAudio: ItemAudio) = viewModelScope.launch {
        mDatabaseRepo.taskAudioDelete(itemAudio)
    }

    fun taskWordHistoryInsert(item: ItemWordHistory) = viewModelScope.launch {
        mDatabaseRepo.taskWordHistoryInsert(item)
    }

    fun taskFeedUpdateFavorite(id: String, isFavorite: Int) = viewModelScope.launch {
        mDatabaseRepo.taskFeedUpdateFavorite(id, isFavorite)
    }

    fun checkExits(id: String): Deferred<Boolean> = viewModelScope.async {
        mDatabaseRepo.taskFeedCheckExist(id)
    }

    fun taskInsertItem(itemFeed: ItemFeed) = viewModelScope.launch {
        mDatabaseRepo.taskFeedInsert(itemFeed)
    }

    fun taskUpdateSeen(id: String, isSeen: Int) = viewModelScope.launch {
        mDatabaseRepo.taskFeedUpdateIsSeen(id, isSeen)
    }

    fun getItemByDatabase(id: String) {
        viewModelScope.launch {
            val item = async {
                mDatabaseRepo.taskFeedGetItem(id)
            }
            itemData.value = item.await()
        }
    }

    fun getItemBySever(itemID: String) {
    }

    fun asyncTaskAudioCheckExist(itemID: String): Boolean =
        mDatabaseRepo.asyncTaskAudioCheckExist(itemID)

    class ViewModelFactory(
        private val context: Context,
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(DetailViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return DetailViewModel(context) as T
            }
            throw IllegalArgumentException("Unable construct viewmodel")
        }
    }
}
package mobi.eup.easykorean.viewmodel

import android.content.Context
import androidx.lifecycle.*
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import mobi.eup.easykorean.data.model.ItemWord
import mobi.eup.easykorean.data.network.LinkMp3Repo
import mobi.eup.easykorean.database.repo.MyDatabaseRepo
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory

/**
 * Created by Dinh Sam Vu on 1/27/2021.
 */
class WordWordViewModel(context: Context, listWordID: List<String>) : ViewModel() {
    private var mDatabaseRepo: MyDatabaseRepo = MyDatabaseRepo(context)
    private val mListWordID: List<String> = listWordID
    private val linkMp3Repo: LinkMp3Repo
    val wordLinkMp3 = MutableLiveData<String>()
    val listCurrent = MutableLiveData<MutableList<ItemWord>>()
    private val tempListCurrent = mutableListOf<ItemWord>()

    init {
        getListCurrent()
        val retrofitBuilderSpeechLink = Retrofit.Builder()
            .addConverterFactory(ScalarsConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl("https://readspeaker.jp/")
            .build()
        linkMp3Repo = retrofitBuilderSpeechLink.create(LinkMp3Repo::class.java)
    }

    fun getListFavorite(): LiveData<MutableList<ItemWord>> = mDatabaseRepo.taskWordGetListFavorite()

    fun insert(itemWord: ItemWord) = viewModelScope.launch {
        mDatabaseRepo.taskWordInsert(itemWord)
    }

    fun updateFavorite(wordID: String, isFavorite: Int) = viewModelScope.launch {
        mDatabaseRepo.taskWordUpdateFavorite(wordID, isFavorite)
    }

    fun getFavoriteAsync(wordID: String): Deferred<Int?> =
        viewModelScope.async { mDatabaseRepo.taskWordGetFavorite(wordID) }

    fun updateNote(wordID: String, note: String) = viewModelScope.launch {
        mDatabaseRepo.taskWordUpdateNote(wordID, note)
    }

    fun getNoteAsync(wordID: String): Deferred<String?> =
        viewModelScope.async { mDatabaseRepo.taskWordGetNote(wordID) }

    private fun getListCurrent() {
        for (wordID in mListWordID) {
            val isFavorite = 0
            val note = ""
            val phonetic = ""
            val mean = ""
            tempListCurrent.add(ItemWord(wordID, phonetic, mean, isFavorite, note))
        }
        listCurrent.value = tempListCurrent
    }

    fun checkExits(wordID: String): Boolean {
        var result = false
        viewModelScope.launch {
            val isExist = async {
                mDatabaseRepo.taskWordCheckExits(wordID)
            }
            result = isExist.await()
        }
        return result
    }

    fun getWordLinkMp3(textOrigin: String, talkID: Int) {
        linkMp3Repo.getSpeechLink(
            textOrigin,
            talkID, //JIHUN 18(MALE), HYERYUN 14(FMALE)
            100,
            85,
            100,
            2,
            0
        ).enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                if (response.body() != null) {
                    val temp = response.body()!!.substring(5).trim()
                    wordLinkMp3.value =
                        "https://readspeaker.jp/ASLCLCLVVS/JMEJSYGDCHMSMHSRKPJL/$temp"
                }
            }

            override fun onFailure(call: Call<String>, t: Throwable) {
            }
        })
    }

    class ViewModelFactory(
        private val context: Context,
        private val listWordID: List<String>
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(WordWordViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return WordWordViewModel(context, listWordID) as T
            }
            throw IllegalArgumentException("Unable construct viewmodel")
        }

    }
}
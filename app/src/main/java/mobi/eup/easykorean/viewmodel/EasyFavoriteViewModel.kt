package mobi.eup.easykorean.viewmodel

import android.content.Context
import androidx.lifecycle.*
import mobi.eup.easykorean.database.repo.MyDatabaseRepo
import mobi.eup.easykorean.data.model.ItemFeed
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class EasyFavoriteViewModel(context: Context) : ViewModel() {
    private var mDatabaseRepo: MyDatabaseRepo = MyDatabaseRepo(context)

    fun getListFavorite(): LiveData<MutableList<ItemFeed>> =
        mDatabaseRepo.taskNewsGetListFavorite("easy")

    fun taskInsertItem(itemFeed: ItemFeed) = viewModelScope.launch {
        mDatabaseRepo.taskFeedInsert(itemFeed)
    }

    fun taskUpdateSeen(id: String, isSeen: Int) = viewModelScope.launch {
        mDatabaseRepo.taskFeedUpdateIsSeen(id, isSeen)
    }

    fun checkExits(id: String): Boolean {
        var result = false
        viewModelScope.launch {
            val isExist = async {
                mDatabaseRepo.taskFeedCheckExist(id)
            }
            result = isExist.await()
        }
        return result
    }
}
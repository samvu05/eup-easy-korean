package mobi.eup.easykorean.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import mobi.eup.easykorean.database.repo.MyDatabaseRepo
import mobi.eup.easykorean.data.model.ItemAudio
import kotlinx.coroutines.launch

/**
 * Created by Dinh Sam Vu on 1/21/2021.
 */
class BSAudioViewModel(context: Context) : ViewModel() {
    private val repoDatabase = MyDatabaseRepo(context)

    fun getAll(): LiveData<List<ItemAudio>> = repoDatabase.taskAudioGetAll()

    fun deleteALl() {
        viewModelScope.launch {
            repoDatabase.taskAudioDeleteAll()
        }
    }

    class ViewModelFactory(
        private val context: Context
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(BSAudioViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return BSAudioViewModel(context) as T
            }
            throw IllegalArgumentException("Unable construct viewmodel")
        }

    }
}
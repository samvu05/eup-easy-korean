package mobi.eup.easykorean.data.network.rest_support.postbody

class PostNewsBody(
    val type: String,
    val page: Int,
    val limit: Int,
)
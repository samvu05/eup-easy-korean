package mobi.eup.easykorean.data.model

import com.google.gson.annotations.SerializedName

data class ItemFeedContent(

    @SerializedName("images")
    var images: MutableList<String>,

    @SerializedName("at_content")
    var atContent: String
)
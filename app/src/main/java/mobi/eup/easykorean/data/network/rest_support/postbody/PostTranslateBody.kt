package mobi.eup.easykorean.data.network.rest_support.postbody

/**
 * Created by Dinh Sam Vu on 1/19/2021.
 */
class PostTranslateBody(
    val uuid: String,
    val id: String,
    val contrycode: String,
    val username: String,
    val content: String
)
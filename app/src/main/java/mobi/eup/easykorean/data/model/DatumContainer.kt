package mobi.eup.easykorean.data.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Dinh Sam Vu on 1/18/2021.
 */
data class DatumContainer(

    @SerializedName("statusCode")
    val statusCode: Int,

    @SerializedName("data")
    val data: MutableList<Datum>,
)
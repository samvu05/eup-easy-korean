package mobi.eup.easykorean.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "table_word")
data class ItemWord(

    @PrimaryKey
    @ColumnInfo(name = "word")
    var word: String,

    @ColumnInfo(name = "phonetic",defaultValue = "")
    var phonetic: String,

    @ColumnInfo(name = "mean",defaultValue = "")
    var mean: String,

    @ColumnInfo(name = "is_favorite", defaultValue = 0.toString())
    var isFavorite: Int,

    @ColumnInfo(name = "note", defaultValue = "")
    var note: String
)

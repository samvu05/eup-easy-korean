package mobi.eup.easykorean.data.network.rest_support.postbody

class PostSearchBody(
    val key: String,
    val limit: Int,
    val type: String,
)
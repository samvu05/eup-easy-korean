package mobi.eup.easykorean.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Dinh Sam Vu on 1/21/2021.
 */

@Entity(tableName = "table_audio")
class ItemAudio(

    @ColumnInfo(name = "title")
    val title: String,

    @PrimaryKey
    @ColumnInfo(name = "id")
    val itemID: String,

    @ColumnInfo(name = "link_img")
    val linkImg: String)
package mobi.eup.easykorean.data.network.rest_support.response

import com.google.gson.annotations.SerializedName

/**
 * Created by Dinh Sam Vu on 1/19/2021.
 */
class ReturnPostTranslate(

    @SerializedName("statusCode")
    val statusCode: String,

    @SerializedName("message")
    val message: String
)
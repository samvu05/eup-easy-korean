package mobi.eup.easykorean.data.model

import android.graphics.drawable.Drawable

/**
 * Created by Dinh Sam Vu on 1/20/2021.
 */
data class ItemMore(
    val name: String,
    val desc: String,
    val icon: Drawable,
)

package mobi.eup.easykorean.data.model

import androidx.room.*
import com.google.gson.annotations.SerializedName

@Entity(tableName = "table_feed")
data class ItemFeed(

    @PrimaryKey
    @ColumnInfo(name = "id")
    @SerializedName("_id", alternate = ["id"])
    var id: String,

    @ColumnInfo(name = "title")
    @SerializedName("title")
    var title: String,

    @ColumnInfo(name = "link")
    @SerializedName("link")
    var link: String,

    @SerializedName("content")
    @Embedded
    val content: ItemFeedContent,

    @ColumnInfo(name = "type")
    @SerializedName("type")
    var type: String,

    @ColumnInfo(name = "word")
    @SerializedName("word")
    var word: List<String>,

    @ColumnInfo(name = "pub_date")
    @SerializedName("pubDate")
    var pubDate: String,

    @ColumnInfo(name = "is_favorite")
    var isFavorite: Int,

    @ColumnInfo(name = "is_seen")
    val isSeen: Int,
)
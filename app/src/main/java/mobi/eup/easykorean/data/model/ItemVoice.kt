package mobi.eup.easykorean.data.model

data class ItemVoice(val name: String, val talkID: Int, val srcImg: Int)
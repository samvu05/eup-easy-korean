package mobi.eup.easykorean.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Dinh Sam Vu on 1/18/2021.
 */

@Entity(tableName = "table_history_word")
data class ItemWordHistory(

    @PrimaryKey
    @ColumnInfo(name = "name")
    val name: String,

    @ColumnInfo(name = "date")
    val date: String,
)

package mobi.eup.easykorean.event

import android.view.View
import mobi.eup.easykorean.data.model.ItemWord

/**
 * Created by Dinh Sam Vu on 1/21/2021.
 */
interface WordsClickCallback {
    fun onItemClick(view: View, item: ItemWord, position: Int)
    fun audioClick(view: View, item: ItemWord, position: Int)
    fun addNoteCallback(view: View, item: ItemWord, position: Int, note: String)
    fun audioLongClick(word: String)
    fun favoriteClick(view: View, item: ItemWord, position: Int, isFavorite: Int)
}
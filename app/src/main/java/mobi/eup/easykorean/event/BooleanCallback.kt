package mobi.eup.easykorean.event

/**
 * Created by Dinh Sam Vu on 1/18/2021.
 */
interface BooleanCallback {
    fun execute(isSuccess: Boolean)
}
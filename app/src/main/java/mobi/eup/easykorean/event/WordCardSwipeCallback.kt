package mobi.eup.easykorean.event

import com.yuyakaido.android.cardstackview.Direction

interface WordCardSwipeCallback {
    fun execute(direction: Direction)
}
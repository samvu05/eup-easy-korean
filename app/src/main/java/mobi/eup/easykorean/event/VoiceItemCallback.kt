package mobi.eup.easykorean.event

import mobi.eup.easykorean.data.model.ItemVoice

interface VoiceItemCallback {
    fun onItemClick(item: ItemVoice)
}
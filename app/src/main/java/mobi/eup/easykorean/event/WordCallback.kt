package mobi.eup.easykorean.event

import mobi.eup.easykorean.data.model.WordJSONObject

/**
 * Created by Dinh Sam Vu on 1/24/2021.
 */
interface WordCallback {
    fun execute(wordObject: WordJSONObject?)
}
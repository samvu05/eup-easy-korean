package mobi.eup.easykorean.event

interface MainPagerListener {
    fun setCurrentStateSegmentControl(pos: Int, isFavorite: Boolean?)
}
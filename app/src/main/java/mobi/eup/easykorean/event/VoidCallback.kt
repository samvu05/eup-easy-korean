package mobi.eup.easykorean.event

/**
 * Created by Dinh Sam Vu on 1/21/2021.
 */
interface VoidCallback {
    fun execute()
}
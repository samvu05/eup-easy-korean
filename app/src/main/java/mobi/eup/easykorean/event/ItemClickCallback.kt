package mobi.eup.easykorean.event

import mobi.eup.easykorean.data.model.Datum

/**
 * Created by Dinh Sam Vu on 1/19/2021.
 */

interface ItemClickCallback {
    fun onClick(datum: Datum)
}
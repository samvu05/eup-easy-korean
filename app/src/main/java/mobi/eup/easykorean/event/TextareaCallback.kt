package mobi.eup.easykorean.event

/**
 * Created by Dinh Sam Vu on 1/18/2021.
 */
interface TextareaCallback {
    fun checkInput(json: String)
    fun onInputChange(text: String)
    fun onFocusChange(text: String)
}
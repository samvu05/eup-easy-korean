package mobi.eup.easykorean.event

/**
 * Created by Dinh Sam Vu on 1/18/2021.
 */
interface TextSelectCallback {
    fun onSelect(content: String, left: String, top: String, right: String, bottom: String)
}
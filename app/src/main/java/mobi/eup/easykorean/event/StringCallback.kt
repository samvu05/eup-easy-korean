package mobi.eup.easykorean.event

/**
 * Created by Dinh Sam Vu on 1/18/2021.
 */
interface StringCallback {
    fun execute(str: String)
}
package mobi.eup.easykorean.event

/**
 * Created by Dinh Sam Vu on 2/26/2021.
 */
interface IAPCallback {
    fun onUpgradePremiumClicked(skuID: String)
}
package mobi.eup.easykorean.event

/**
 * Created by Dinh Sam Vu on 1/20/2021.
 */
interface IntegerCallback {
    fun execute(position: Int)
}
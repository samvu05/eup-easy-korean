package mobi.eup.easykorean.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import mobi.eup.easykorean.databinding.ItemHistoryWordBinding
import mobi.eup.easykorean.data.model.ItemWordHistory

class RccHistoryWordAdapter(
    private var data: List<ItemWordHistory>,
    private val onItemClick: (String) -> Unit
) :
    RecyclerView.Adapter<RccHistoryWordAdapter.ViewHoler>() {

    class ViewHoler(val binding: ItemHistoryWordBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHoler {
        val binding =
            ItemHistoryWordBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHoler(binding)
    }

    override fun onBindViewHolder(holder: ViewHoler, position: Int) {
        holder.binding.tvWordHistory.text = data[position].name
        holder.binding.tvDate.text = data[position].date
        holder.binding.tvWordHistory.setOnClickListener {
            onItemClick(data[position].name)
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun setAdapterData(data: List<ItemWordHistory>) {
        data.also { it.also { it.also { this.data = it } } }
        notifyDataSetChanged()
    }
}

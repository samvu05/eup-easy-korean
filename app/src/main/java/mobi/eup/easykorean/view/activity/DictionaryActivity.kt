package mobi.eup.easykorean.view.activity

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import mobi.eup.easykorean.R
import mobi.eup.easykorean.databinding.ActivityDictionaryBinding
import mobi.eup.easykorean.utils.app.NetworkHelper
import mobi.eup.easykorean.utils.language.LanguageHelper
import mobi.eup.easykorean.view.base.BaseActivity

/**
 * Created by Dinh Sam Vu on 1/27/2021.
 */
@Suppress("DEPRECATION")
class DictionaryActivity : BaseActivity(), View.OnClickListener, View.OnTouchListener {
    private lateinit var binding: ActivityDictionaryBinding
    private lateinit var webView: WebView
    private lateinit var ivError: ImageView
    private lateinit var tvError: TextView
    private lateinit var imgBack: ImageView
    private lateinit var imgHome: ImageView
    private lateinit var imgNext: ImageView
    var value: String = ""

    @SuppressLint("ClickableViewAccessibility", "SetJavaScriptEnabled")
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =
            DataBindingUtil.setContentView(this@DictionaryActivity, R.layout.activity_dictionary)
        ivError = binding.imageView
        tvError = binding.tvError
        imgBack = binding.imgBack
        imgHome = binding.imgHome
        imgNext = binding.imgNext

        webView = binding.wvTuDien
        webView.webViewClient = WebViewClient()
        webView.settings.javaScriptEnabled = true
        webView.webChromeClient = WebChromeClient()

        if (intent != null) {
            value = intent.getStringExtra("query_key")!!
        }
        val currentLanguage = LanguageHelper.getDefaultLanguageCode(this)
        val url = "https://dict.naver.com/${currentLanguage}kodict/#/search?query=$value"

        if (NetworkHelper.isNetWork(this@DictionaryActivity)) {
            webView.loadUrl(url)
            webView.visibility = View.VISIBLE
            ivError.visibility = View.GONE
            tvError.visibility = View.GONE
            binding.pbBar.visibility = View.GONE

        } else {
            webView.visibility = View.GONE
            binding.pbBar.visibility = View.GONE
            ivError.visibility = View.VISIBLE
            tvError.visibility = View.VISIBLE
        }

        binding.imgRefresh.setOnClickListener {
            if (NetworkHelper.isNetWork(this@DictionaryActivity)) {
                webView.loadUrl(url)
                webView.visibility = View.VISIBLE
                ivError.visibility = View.GONE
                tvError.visibility = View.GONE
                binding.pbBar.visibility = View.GONE

            } else {
                webView.visibility = View.GONE
                binding.pbBar.visibility = View.GONE
                ivError.visibility = View.VISIBLE
                tvError.visibility = View.VISIBLE
            }
        }

        if (webView.canGoForward()) {
            imgNext.setAlpha(228)
        } else {
            imgNext.setAlpha(50)
        }
        if (webView.canGoBack()) {
            imgBack.setAlpha(228)
        } else {
            imgBack.setAlpha(50)
        }
        webView.setOnTouchListener(this)
        imgHome.setOnClickListener(this)
        imgBack.setOnClickListener(this)
        imgNext.setOnClickListener(this)

        binding.imgClose.setOnClickListener {
            finish()
        }
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.imgHome -> webView.loadUrl("https://dict.naver.com/")
            R.id.imgNext -> if (webView.canGoForward()) {
                webView.goForward()
            }
            R.id.imgBack -> if (webView.canGoBack()) {
                webView.goBack()
            }

        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouch(view: View, p1: MotionEvent?): Boolean {
        if (webView.canGoForward()) {
            imgNext.setAlpha(228)
        } else {
            imgNext.setAlpha(50)
        }


        if (webView.canGoBack()) {
            imgBack.setAlpha(228)
        } else {
            imgBack.setAlpha(50)
        }
        return false
    }
}

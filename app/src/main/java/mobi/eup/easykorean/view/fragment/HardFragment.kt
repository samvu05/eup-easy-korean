package mobi.eup.easykorean.view.fragment

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.viewpager.widget.ViewPager
import mobi.eup.easykorean.databinding.FragmentHardBinding
import mobi.eup.easykorean.event.MainPagerListener
import mobi.eup.easykorean.view.adapter.SlidePagerAdapterCommon
import mobi.eup.easykorean.view.animation.DepthPageTransformer
import mobi.eup.easykorean.view.base.BaseFragment


@Suppress("DEPRECATION")
class HardFragment : BaseFragment(), ViewPager.OnPageChangeListener {
    private lateinit var binding: FragmentHardBinding
    private lateinit var hardCurrentFragment: HardCurrentFragment
    private lateinit var hardFavoriteFragment: HardFavoriteFragment
    private lateinit var mPaper: ViewPager

    private var listener: MainPagerListener? = null

    companion object {
        fun newInstance(): HardFragment {
            val args = Bundle()
            val fragment =
                HardFragment()
            fragment.setArguments(args)
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHardBinding.inflate(inflater, container, false)
        mPaper = binding.viewpaperHardFrag

        mPaper.setPageTransformer(true, DepthPageTransformer())
        mPaper.addOnPageChangeListener(this)
        hardCurrentFragment = HardCurrentFragment()
        hardFavoriteFragment = HardFavoriteFragment()

        val fragManager: FragmentManager = mContext.supportFragmentManager
        val pagerAdapter = SlidePagerAdapterCommon(fragManager)
        pagerAdapter.addFragment(hardCurrentFragment, "Current")
        pagerAdapter.addFragment(this.hardFavoriteFragment, "Faforite")
        mPaper.adapter = pagerAdapter
        mPaper.offscreenPageLimit = pagerAdapter.count

        return binding.root
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        if (mContext is MainPagerListener) listener = mContext as MainPagerListener
    }

    fun setCurrentItem(pos: Int) {
        mPaper.setCurrentItem(1)
        mPaper.setCurrentItem(pos, true)
    }

    override fun onDetach() {
        listener = null
        super.onDetach()
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
        if (listener != null) {
            listener!!.setCurrentStateSegmentControl(1, position == 1)
        }
    }

    override fun onPageScrollStateChanged(state: Int) {
    }
}
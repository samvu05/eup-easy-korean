@file:Suppress("DEPRECATION")

package mobi.eup.easykorean.view.bottomsheet

import android.app.Activity
import android.content.Context
import android.graphics.Paint
import android.os.AsyncTask
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.viewpager.widget.ViewPager
import biz.laenger.android.vpbs.ViewPagerBottomSheetDialogFragment
import com.anjlab.android.iab.v3.BillingProcessor
import com.anjlab.android.iab.v3.SkuDetails
import com.rd.PageIndicatorView
import kotlinx.android.synthetic.main.bottom_sheet_premium.*
import mobi.eup.easykorean.R
import mobi.eup.easykorean.event.IAPCallback
import mobi.eup.easykorean.utils.app.GlobalHelper
import mobi.eup.easykorean.utils.app.PreferenceHelper
import mobi.eup.easykorean.utils.iap.GetPriceHelper
import mobi.eup.easykorean.view.adapter.PagerIndicatorReviewAdapter
import java.text.NumberFormat
import kotlin.random.Random

/**
 * Created by Dinh Sam Vu on 2/24/2021.
 */
@Suppress("DEPRECATION")
class BSPremium :
    ViewPagerBottomSheetDialogFragment,
    RadioGroup.OnCheckedChangeListener {
    private lateinit var btnClose: ImageView
    private lateinit var viewPagerPremium: ViewPager
    private lateinit var layoutSub3: RelativeLayout
    private lateinit var layoutSub12: RelativeLayout
    private lateinit var layoutPremium: RelativeLayout
    private lateinit var pageIndicatorView: PageIndicatorView

    private lateinit var tvPriceSub3: TextView
    private lateinit var tvSaleSub3: TextView
    private lateinit var tvOldPriceSub3: TextView
    private lateinit var tvPriceSub12: TextView
    private lateinit var tvSaleSub12: TextView
    private lateinit var tvOldPriceSub12: TextView
    private lateinit var tvPricePremium: TextView
    private lateinit var tvSalePremium: TextView
    private lateinit var tvOldPricePremium: TextView


    private var mPageAdapter: PagerIndicatorReviewAdapter? = null
    private lateinit var timer: CountDownTimer
    private var currentPage = 0
    private var percent = "0"

    private var iapHelper: IAPCallback? = null
    private var bp: BillingProcessor? = null
    private var isFullScreen = false
    private lateinit var preferenceHelper: PreferenceHelper
    private lateinit var mActivity: Activity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        preferenceHelper = PreferenceHelper(mActivity, GlobalHelper.PREFERENCE_NAME_APP)
    }

    constructor()
    constructor(billingProcessor: BillingProcessor, isFullScr: Boolean) {
        this@BSPremium.bp = billingProcessor
        this@BSPremium.isFullScreen = isFullScr
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        val view = inflater.inflate(R.layout.bottom_sheet_premium, container, false)
        initView(view)
        setDataIndicator()
        reCreateViewBySaleData()
        initEvent()
        return view
    }

    private fun initView(view: View) {
        viewPagerPremium = view.findViewById(R.id.view_pager_premium)
        btnClose = view.findViewById(R.id.btn_close)
        layoutSub3 = view.findViewById(R.id.layout_sub3)
        layoutSub12 = view.findViewById(R.id.layout_sub12)
        layoutPremium = view.findViewById(R.id.layout_premium)
        pageIndicatorView = view.findViewById(R.id.pageIndicatorView)

        tvSaleSub3 = view.findViewById(R.id.tv_sale_sub3)
        tvSaleSub12 = view.findViewById(R.id.tv_sale_sub12)
        tvSalePremium = view.findViewById(R.id.tv_sale_premium)

        tvPriceSub3 = view.findViewById(R.id.tv_price_sub3)
        tvPriceSub12 = view.findViewById(R.id.tv_price_sub12)
        tvPricePremium = view.findViewById(R.id.tv_price_premium)

        tvOldPriceSub3 = view.findViewById(R.id.tv_old_price_sub3)
        tvOldPriceSub12 = view.findViewById(R.id.tv_old_price_sub12)
        tvOldPricePremium = view.findViewById(R.id.tv_old_price_premium)
    }

    private fun reCreateViewBySaleData() {
        percent = "0" // percent of sale - format : _30,_40...
        try {
            val getPriceCallback = GetPriceHelper.GetPriceCallback { skuDetails ->
                val skuPremium: SkuDetails = skuDetails[0]

                tvPricePremium.text = skuPremium.priceText
                setTextSaving(89, tvOldPricePremium)

                val sku12: SkuDetails = skuDetails[1]
                tvPriceSub12.text = sku12.priceText
                setTextSaving(53, tvOldPriceSub12)

                val sku3: SkuDetails = skuDetails[2]
                tvPriceSub3.text = sku3.priceText

                if (percent != "0") {
//                    val time: String = item.getResults().getTime()
                    val time = Random.nextInt(5).toString()

                    val percentInt = percent.toInt()
                    val format = NumberFormat.getCurrencyInstance()

                    val oldPricePremium =
                        format.format(skuPremium.priceValue * 100 / (100 - percentInt))
                    setOldPrice(oldPricePremium, tvOldPricePremium)

                    val oldPrice12 =
                        format.format(sku12.priceValue * 100 / (100 - percentInt))
                    setOldPrice(oldPrice12, tvOldPriceSub12)

                    val oldPrice3 =
                        format.format(sku3.priceValue * 100 / (100 - percentInt))
                    setOldPrice(oldPrice3, tvOldPriceSub3)

                    setSaleOffText(percent, time, tvSalePremium)
                    setSaleOffText(percent, time, tvSaleSub12)
                    setSaleOffText(percent, time, tvSaleSub3)
                }
            }

            GetPriceHelper(getPriceCallback, percent)
                .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, bp)
        } catch (ignored: java.lang.IllegalStateException) {
        }
    }

    private fun initEvent() {
        val sale = if (percent != "0") "_$percent" else ""
        btnClose.setOnClickListener {
            dismiss()
        }
        layoutSub3.setOnClickListener {
            if (iapHelper != null) {
                iapHelper!!.onUpgradePremiumClicked(GlobalHelper.SKU_SUB3 + sale)
            }
        }
        layoutSub12.setOnClickListener {
            if (iapHelper != null) {
                iapHelper!!.onUpgradePremiumClicked(GlobalHelper.SKU_SUB12 + sale)
            }
        }
        layoutPremium.setOnClickListener {
            if (iapHelper != null) {
                iapHelper!!.onUpgradePremiumClicked(GlobalHelper.SKU_PREMIUM + sale)
            }
        }
    }

    private fun setDataIndicator() {
        val fragments: MutableList<ReviewPremiumFragment> = ArrayList()
        fragments.add(ReviewPremiumFragment.newInstance(1))
        fragments.add(ReviewPremiumFragment.newInstance(2))
        fragments.add(ReviewPremiumFragment.newInstance(3))
        if (mPageAdapter == null) {
            mPageAdapter = PagerIndicatorReviewAdapter(childFragmentManager)
            mPageAdapter!!.addAllFragments(fragments)
            viewPagerPremium.isSaveEnabled = false
            viewPagerPremium.adapter = mPageAdapter
            setupViewPager()
        } else {
            mPageAdapter!!.setNewFragments(fragments)
        }
    }

    private fun setupViewPager() {
        setupAutoPager()
        viewPagerPremium.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }
            override fun onPageSelected(position: Int) {
                pageIndicatorView.selection = position % 3
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })
    }

    private fun setupAutoPager() {
        timer = object : CountDownTimer(5000, 1000) {
            override fun onTick(millisUntilFinished: Long) {}
            override fun onFinish() {
                currentPage = viewPagerPremium.currentItem + 1
                if (currentPage == 3) currentPage = 0
                try {
                    viewPagerPremium.setCurrentItem(currentPage, true)
                } catch (e: OutOfMemoryError) {
                    Log.e("error", "OutOfMemoryError")
                } catch (e: IllegalStateException) {
                    Log.e("error", "IllegalStateException")
                }
                timer.start()
            }
        }
        timer.start()
    }

    override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
        TODO("Not yet implemented")
    }

    private fun setTextSaving(percent: Int, textView: TextView) {
        try {
            val saving = java.lang.String.format(getString(R.string.saving), percent)
            textView.text = saving
        } catch (ignored: java.lang.IllegalStateException) {
        }
    }

    private fun setOldPrice(oldPrice: String, tv: TextView) {
        tv.text = oldPrice
        tv.paintFlags = tv.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
    }

    private fun setSaleOffText(percent: String, time: String, tv: TextView) {
        try {
            val spanned = Html.fromHtml(
                getString(R.string.sale) + ": <font color=yellow><big><b>-" + percent + "%</b></big></font>   " +
                        getString(R.string.date) + ": $time"
            )
            tv.text = spanned
        } catch (ignored: java.lang.IllegalStateException) {
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mActivity = context as Activity
        if (context is IAPCallback) {
            iapHelper = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        iapHelper = null
    }

    override fun onDestroy() {
        super.onDestroy()
        timer.cancel()
        iapHelper = null
    }
}
package mobi.eup.easykorean.view.bottomsheet

import android.app.Dialog
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.SwitchCompat
import mobi.eup.easykorean.R
import mobi.eup.easykorean.databinding.BottomSheetSettingDetailsBinding
import mobi.eup.easykorean.view.base.BaseFragment

class BSSettingDetail : BaseFragment(), CompoundButton.OnCheckedChangeListener,
    View.OnClickListener {
    private lateinit var binding: BottomSheetSettingDetailsBinding
    private lateinit var btnDarkmode: SwitchCompat

    private lateinit var btnSelectSpeaker: Button
    private lateinit var ivSpeaker: ImageView
    private lateinit var ivVoiceSetting: ImageView

    private lateinit var btnChangeFontSize: Button

    private lateinit var speakerList : Array<String>
    private val fontSizeList =
        arrayOf("12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25")
    private lateinit var btnYourname: Button

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = BottomSheetSettingDetailsBinding.inflate(inflater, container, false)
        binding.border1
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        speakerList = arrayOf(resources.getString(R.string.hyeryun),resources.getString(R.string.jihun))
        initView(view)
        initEvent()
    }

    fun initView(view: View) {
        btnDarkmode = view.findViewById(R.id.nightmode_sc)

        btnYourname = view.findViewById(R.id.your_name_btn)
        btnYourname.text = preferenceHelper.getLocalName()
        btnYourname.setOnClickListener(this)

        btnSelectSpeaker = view.findViewById(R.id.select_voice_btn)
        ivSpeaker = view.findViewById(R.id.select_voice_iv)
        ivVoiceSetting = view.findViewById(R.id.iv_voice_settings)

        btnChangeFontSize = view.findViewById(R.id.font_size_btn)
        btnChangeFontSize.text = preferenceHelper.getFontSize().toString()
        btnChangeFontSize.setOnClickListener(this)


        refreshSpeaker()
        btnSelectSpeaker.setOnClickListener(this)

    }

    private fun initEvent() {
        btnDarkmode.isChecked = preferenceHelper.isNightMode()

        btnDarkmode.setOnCheckedChangeListener(this)
    }

    override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
        when (buttonView.id) {
            R.id.nightmode_sc -> {
                preferenceHelper.setNightMode(isChecked)
            }
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.your_name_btn -> showDialogChangeName()
            R.id.select_voice_btn -> showDialogChangeSpeaker()
            R.id.font_size_btn -> showDialogChangeFontSize()
        }
    }

    private fun showDialogChangeName() {
        val myDialog = Dialog(mContext)
        myDialog.setContentView(R.layout.dialog_name)
        val edtContent: EditText
        val btnCancel: Button
        val btnAgree: Button

        edtContent = myDialog.findViewById(R.id.edtContent)
        btnCancel = myDialog.findViewById(R.id.btn_cancel)
        btnAgree = myDialog.findViewById(R.id.btn_agree)
        edtContent.setText(preferenceHelper.getLocalName())

        btnAgree.setOnClickListener {
            val content: String = if (TextUtils.isEmpty(edtContent.text.toString())) {
                preferenceHelper.getLocalName().toString()
            } else {
                edtContent.text.toString()
            }
            preferenceHelper.setLocalName(content)
            btnYourname.text = content
            myDialog.cancel()
        }

        btnCancel.setOnClickListener {
            myDialog.cancel()
        }
        myDialog.show()
    }

    private fun showDialogChangeSpeaker() {
        val mAlertDialogBuilder = AlertDialog.Builder(mContext)
        mAlertDialogBuilder.setTitle(resources.getString(R.string.choose_speaker))
        mAlertDialogBuilder.setIcon(R.drawable.ic_headphones)
        mAlertDialogBuilder.setItems(speakerList) { _, which ->
            when (which) {
                0 -> {
                    preferenceHelper.setSpeakerID(14)
                    refreshSpeaker()
                }
                1 -> {
                    preferenceHelper.setSpeakerID(18)
                    refreshSpeaker()
                }
            }
        }
        mAlertDialogBuilder.setNeutralButton(resources.getString(R.string.close)) { _, _ ->
        }
        val mAlertDialog = mAlertDialogBuilder.create()
        mAlertDialog.show()
    }

    private fun refreshSpeaker() {
        when {
            this.preferenceHelper.getSpeakerID() == 14 -> {
                btnSelectSpeaker.text = speakerList[0]
                ivSpeaker.setImageResource(R.drawable.ava_hyeryun)
                ivVoiceSetting.setImageResource(R.drawable.ava_hyeryun)

            }
            this.preferenceHelper.getSpeakerID() == 18 -> {
                btnSelectSpeaker.text = speakerList[1]
                ivSpeaker.setImageResource(R.drawable.ava_jihun)
                ivVoiceSetting.setImageResource(R.drawable.ava_jihun)
            }
            else -> return
        }
    }

    private fun showDialogChangeFontSize() {
        val mAlertDialogBuilder = AlertDialog.Builder(mContext)
        mAlertDialogBuilder.setTitle(resources.getString(R.string.fontsize))
        mAlertDialogBuilder.setIcon(R.drawable.ic_font_size)
        mAlertDialogBuilder.setItems(fontSizeList) { _, which ->
            preferenceHelper.setFontSize(fontSizeList[which].toInt())
            btnChangeFontSize.text = fontSizeList[which]
        }
        mAlertDialogBuilder.setNeutralButton(resources.getString(R.string.close)) { _, _ ->
        }
        val mAlertDialog = mAlertDialogBuilder.create()
        mAlertDialog.show()
    }

}
package mobi.eup.easykorean.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import mobi.eup.easykorean.R
import mobi.eup.easykorean.databinding.ItemVoiceSelectBinding
import mobi.eup.easykorean.event.VoiceItemCallback
import mobi.eup.easykorean.data.model.ItemVoice

@Suppress("DEPRECATED_IDENTITY_EQUALS")
class RccVoiceSelectAdapter(val voiceItemCallback: VoiceItemCallback) :
    RecyclerView.Adapter<RccVoiceSelectAdapter.ViewHolder>() {
    private lateinit var mContext: Context
    private val items: MutableList<ItemVoice>

    init {
        items = mutableListOf<ItemVoice>()
        items.add(ItemVoice("HYERYUN", 14, R.drawable.ava_hyeryun))
        items.add(ItemVoice("JIHUN", 18, R.drawable.ava_jihun))
    }

    class ViewHolder(val binding: ItemVoiceSelectBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ItemVoiceSelectBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        mContext = parent.context
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if (position < items.size) {
            val item: ItemVoice = items[position]
            try {
                holder.binding.cImg.setImageResource(item.srcImg)
            } catch (e: OutOfMemoryError) {
                e.printStackTrace()
            }
            holder.binding.cImg.setOnClickListener {
                notifyDataSetChanged()
                voiceItemCallback.onItemClick(item)
            }
        }
    }

    override fun getItemCount(): Int = items.size
    fun getPositionByTalkId(talkId: Int): Int {
        for (i in items.indices) {
            if (items[i].talkID === talkId) return i
        }
        return 0
    }

    fun getItemByTalkId(talkId: Int): ItemVoice? {
        for (i in items.indices) {
            if (items[i].talkID === talkId) return items.get(i)
        }
        return items.get(0)
    }

}
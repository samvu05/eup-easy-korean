package mobi.eup.easykorean.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import mobi.eup.easykorean.event.ItemClickCallback
import mobi.eup.easykorean.R
import mobi.eup.easykorean.data.model.Datum

/**
 * Created by Dinh Sam Vu on 1/19/2021.
 */
class RccItemTranslateAdapter(
    private var context: Context,
    private var newListTranslates: List<Datum>,
    itemClickCallback: ItemClickCallback
) : RecyclerView.Adapter<RccItemTranslateAdapter.ViewHolder1>() {

    private var itemClickCallback: ItemClickCallback? = itemClickCallback

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder1 {
        val view: View =
            LayoutInflater.from(context).inflate(R.layout.item_translate, parent, false)
        return ViewHolder1(view)
    }

    override fun onBindViewHolder(holder: ViewHolder1, position: Int) {
        val newListTranslate: Datum = newListTranslates[position]
        holder.tvUserName.text = newListTranslate.username
        holder.txtCountLike.text = java.lang.String.valueOf(newListTranslate.newsLike)
        holder.txtCountDislike.text = java.lang.String.valueOf(newListTranslate.newsDislike)

        holder.itemView.setOnClickListener {
            itemClickCallback?.onClick(newListTranslate)
        }
    }

    override fun getItemCount(): Int {
        return newListTranslates.size
    }

    class ViewHolder1(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txtCountDislike: TextView = itemView.findViewById(R.id.txt_count_dislike)
        val txtCountLike: TextView = itemView.findViewById(R.id.txt_count_like)
        val tvUserName: TextView = itemView.findViewById(R.id.tv_user_name)
    }
}

package mobi.eup.easykorean.view.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

@Suppress("DEPRECATION")
class SlidePagerAdapterCommon(fm: FragmentManager) :
    FragmentStatePagerAdapter(fm) {

    private val mFragmentList = mutableListOf<Fragment>()
    private val mTitleList = mutableListOf<String>()

    fun addFragment(fragment: Fragment, title: String) {
        mFragmentList.add(fragment)
        mTitleList.add(title)
    }

    override fun getCount(): Int = mFragmentList.size

    override fun getItem(position: Int): Fragment = mFragmentList[position]

    override fun getPageTitle(position: Int): CharSequence {
        return mTitleList[position]
    }
}
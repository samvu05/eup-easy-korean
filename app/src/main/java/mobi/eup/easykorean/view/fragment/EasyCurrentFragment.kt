package mobi.eup.easykorean.view.fragment

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import mobi.eup.easykorean.data.model.ItemFeed
import mobi.eup.easykorean.databinding.FragmentEasyCurrentBinding
import mobi.eup.easykorean.view.adapter.RccNewsAdapter
import mobi.eup.easykorean.view.base.BaseFragment
import mobi.eup.easykorean.viewmodel.EasyCurrentViewModel


class EasyCurrentFragment : BaseFragment() {

    private lateinit var binding: FragmentEasyCurrentBinding
    private lateinit var model: EasyCurrentViewModel
    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mAdapter: RccNewsAdapter
    private lateinit var layoutManager: LinearLayoutManager
    private val listData = mutableListOf<ItemFeed>()
    var page = 1

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentEasyCurrentBinding.inflate(inflater, container, false)

        model = ViewModelProvider(
            this@EasyCurrentFragment,
            EasyCurrentViewModel.ViewModelFactory(mContext)
        ).get(EasyCurrentViewModel::class.java)

        binding.rccData = model
        binding.lifecycleOwner = this
        mRecyclerView = binding.recyclerviewEasyCurrent
        mAdapter =
            RccNewsAdapter(
                if (model.listData.value == null) mutableListOf() else model.listData.value!!,
                mContext,
                onItemClick
            )

        layoutManager = LinearLayoutManager(inflater.context)
        mRecyclerView.layoutManager = layoutManager
        mRecyclerView.adapter = mAdapter
        mRecyclerView.setHasFixedSize(true)
        (mRecyclerView.itemAnimator as DefaultItemAnimator).supportsChangeAnimations = false

        model.getData(page)

        model.listData.observe(mContext, {
            val currentPos: Int = mAdapter.itemCount
            listData += it
            mAdapter.insertAdapterData(listData, currentPos)
        })

        mRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) {
                    val visibleItemCount = layoutManager.childCount
                    val pastVisibleItem = layoutManager.findFirstCompletelyVisibleItemPosition()
                    val total = mAdapter.itemCount

                    if (!model.isLoadmore.get()) {
                        if (visibleItemCount + pastVisibleItem >= total) {
                            page++
                            model.getData(page)
                        }
                    }
                }
                super.onScrolled(recyclerView, dx, dy)
            }
        })

        return binding.root
    }

    private val onItemClick: (ItemFeed) -> Unit = {
        model.taskInsertItem(it)
        model.taskUpdateSeen(it.id, 1)
    }
}
package mobi.eup.easykorean.view.bottomsheet

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import mobi.eup.easykorean.databinding.BottomSheetHistoryWordBinding
import mobi.eup.easykorean.view.activity.DictionaryActivity
import mobi.eup.easykorean.view.adapter.RccHistoryWordAdapter
import mobi.eup.easykorean.view.base.BaseFragment

@Suppress("DEPRECATION")
class BSHistoryWord : BaseFragment() {
    private lateinit var binding: BottomSheetHistoryWordBinding
    private lateinit var mAdapter: RccHistoryWordAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = BottomSheetHistoryWordBinding.inflate(inflater, container, false)
        initView(inflater)
        return binding.root
    }

    private fun initView(inflater: LayoutInflater) {
        binding.lifecycleOwner = this
        mAdapter = RccHistoryWordAdapter(mutableListOf(), onItemClick())
        binding.rccHistoryNew.apply {
            adapter = mAdapter
            layoutManager = LinearLayoutManager(inflater.context)
        }
        repoDatabase.taskWordHistoryGetAll().observe(viewLifecycleOwner, {
            mAdapter.setAdapterData(it)
            binding.included.placeHolder.visibility = if (it.isEmpty()) View.VISIBLE else View.GONE
        })
    }

    private fun onItemClick(): (String) -> Unit = {
        startActivity(
            Intent(
                mContext,
                DictionaryActivity::class.java
            ).putExtra("query_key", it)
        )
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        mContext = activity as FragmentActivity
    }
}
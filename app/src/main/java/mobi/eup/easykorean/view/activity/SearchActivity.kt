package mobi.eup.easykorean.view.activity

import android.graphics.Color
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.ads.*
import mobi.eup.easykorean.R
import mobi.eup.easykorean.admob.AdsNative
import mobi.eup.easykorean.admob.BannerEvent
import mobi.eup.easykorean.data.model.ItemFeed
import mobi.eup.easykorean.databinding.ActivitySearchBinding
import mobi.eup.easykorean.view.adapter.RccNewsAdapter
import mobi.eup.easykorean.view.base.BaseActivity
import mobi.eup.easykorean.view.bottomsheet.BSHistoryContainer
import mobi.eup.easykorean.viewmodel.SearchViewModel

class SearchActivity : BaseActivity(), BannerEvent {
    private lateinit var binding: ActivitySearchBinding
    private lateinit var model: SearchViewModel
    private lateinit var recyclerView: RecyclerView
    private lateinit var mAdapter: RccNewsAdapter
    private lateinit var searchView: SearchView

    private lateinit var btnEasy: Button
    private lateinit var btnDifficult: Button
    private var isDifficult = false

    private val drawableLeft = R.drawable.tab_left_bg
    private val drawableLeftSelected = R.drawable.tab_left_bg_selected
    private val drawableRight = R.drawable.tab_right_bg
    private val drawableRightSelected = R.drawable.tab_right_bg_selected

    private val colorTextBtnSelected = Color.parseColor("#FFFFFF")
    private val colorTextBtnUnselected = Color.parseColor("#34436c")
    private var queryText = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search)
        model = ViewModelProvider(
            this@SearchActivity,
            SearchViewModel.ViewModelFactory(this@SearchActivity)
        ).get(SearchViewModel::class.java)
        initView()
        getDataFromIntent()
        initAds()
    }

    private fun initView() {
        searchView = binding.searchView
        recyclerView = binding.recyclerviewSearch
        binding.viewData = model

        mAdapter =
            RccNewsAdapter(
                if (model.listData.value == null) mutableListOf() else model.listData.value!!,
                this@SearchActivity, onItemClick
            )

        binding.lifecycleOwner = this@SearchActivity
        recyclerView.apply {
            adapter = mAdapter
            layoutManager =
                LinearLayoutManager(this@SearchActivity)
        }

        binding.btnBack.setOnClickListener {
            mAdsInterstitial.showAds()
            super.onBackPressed()
        }
        binding.btnHistory.setOnClickListener {
            val bottomSheetHistory = BSHistoryContainer()
            bottomSheetHistory.show(supportFragmentManager, bottomSheetHistory.tag)
        }

        btnEasy = binding.btnEasy
        btnDifficult = binding.btnDifficult

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText != null && newText != "") {
                    this@SearchActivity.queryText = newText
                    if (isDifficult) {
                        model.getDataOnce(queryText, "normal")
                    } else model.getDataOnce(queryText, "easy")
                }
                return true
            }
        })

        btnEasy.setOnClickListener {
            if (queryText != "") {
                model.getDataOnce(queryText, "easy")
            }
            setEasySelect()
        }

        btnDifficult.setOnClickListener {
            if (queryText != "") {
                model.getDataOnce(queryText, "normal")
            }
            setDifficultSelect()
        }
    }

    private fun initAds() {
        if (preferenceHelper.isPremiumUser()) {
            return
        }
//        mAdsBanner = AdsBanner(this@MainActivity)
//        mAdsBanner.showBannerAdsView(binding.includedViewAdsBanner.adView, false)

        adsNative = AdsNative(this@SearchActivity)
        adsNative.showNativeAdsView(binding.includedViewAdsBanner.adView, false)
    }

    private val onItemClick: (ItemFeed) -> Unit = {
        model.taskInsertItem(it)
        model.taskUpdateSeen(it.id, 1)
    }

    private fun getDataFromIntent() {
        val intent = intent
        if (intent != null) isDifficult = intent.getBooleanExtra("isDifficult", false)
        if (isDifficult) {
            setDifficultSelect()
        } else setEasySelect()
    }

    private fun setEasySelect() {
        btnEasy.setBackgroundResource(drawableLeftSelected)
        btnEasy.setTextColor(colorTextBtnSelected)
        btnDifficult.setBackgroundResource(drawableRight)
        btnDifficult.setTextColor(colorTextBtnUnselected)
        isDifficult = false
    }

    private fun setDifficultSelect() {
        btnEasy.setBackgroundResource(drawableLeft)
        btnEasy.setTextColor(colorTextBtnUnselected)
        btnDifficult.setBackgroundResource(drawableRightSelected)
        btnDifficult.setTextColor(colorTextBtnSelected)
        isDifficult = true
    }

    override fun updateContentViewWithBanner(height: Int) {
        binding.contentLayout.setPadding(0, 0, 0, height)
        binding.contentLayout.requestLayout()
    }

}
package mobi.eup.easykorean.view.adapter

import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import mobi.eup.easykorean.view.fragment.DictionaryFragment
import mobi.eup.easykorean.view.fragment.EasyFragment
import mobi.eup.easykorean.view.fragment.HardFragment


@Suppress("DEPRECATION")
class SlidePagerAdapterMain(fm: FragmentManager) :
    FragmentStatePagerAdapter(fm) {

    private val mTitleList = mutableListOf("Easy", "Difficult", "Dictionary")
    private lateinit var easyFragment: EasyFragment
    private lateinit var hardFragment: HardFragment
    private lateinit var dictionaryFragment : DictionaryFragment

    override fun getCount(): Int = 3

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> {
                easyFragment = EasyFragment.newInstance()
                return easyFragment
            }
            1 -> {
                hardFragment = HardFragment.newInstance()
                return hardFragment
            }
            2 -> {
                dictionaryFragment = DictionaryFragment()
                return dictionaryFragment
            }
        }
        return Fragment()
    }

    override fun getPageTitle(position: Int): CharSequence {
        return mTitleList[position]
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val createdFragment = super.instantiateItem(container, position) as Fragment
        // save the appropriate reference depending on position
        when (position) {
            0 -> easyFragment = createdFragment as EasyFragment
            1 -> hardFragment = createdFragment as HardFragment
            2 -> dictionaryFragment = createdFragment as DictionaryFragment
        }
        return createdFragment
    }

    fun toggleEasyFragment(isFavorite: Boolean) {
        easyFragment.setCurrentItem(if (isFavorite) 1 else 0)
    }

    fun toggleHardFragment(isFavorite: Boolean) {
        hardFragment.setCurrentItem(if (isFavorite) 1 else 0)
    }
}
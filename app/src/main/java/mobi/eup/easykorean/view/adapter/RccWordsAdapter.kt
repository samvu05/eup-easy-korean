package mobi.eup.easykorean.view.adapter

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import mobi.eup.easykorean.R
import mobi.eup.easykorean.data.model.ItemWord
import mobi.eup.easykorean.databinding.ItemRccWordsDetailBinding
import mobi.eup.easykorean.event.WordsClickCallback

class RccWordsAdapter(
    listNewWords: MutableList<ItemWord>,
    context: Context,
    listener: WordsClickCallback,
) : RecyclerView.Adapter<RccWordsAdapter.ViewHolder>() {

    private var data: MutableList<ItemWord> = listNewWords
    private val mContext = context
    private val mListener: WordsClickCallback = listener

    class ViewHolder(val binding: ItemRccWordsDetailBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ItemRccWordsDetailBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = data.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        holder.binding.tvWord.text = item.word

        if (item.phonetic != "") {
            holder.binding.tvPhonetic.text = " 「"+ item.phonetic +"」"
            holder.binding.pbPhonetic.visibility = View.GONE
        }

        if (item.mean != "") {
            holder.binding.tvTranslate.text = item.mean
            holder.binding.pbMean.visibility = View.GONE
        }


        var isFavorite = item.isFavorite
        if (isFavorite == 1) {
            holder.binding.btnFavoriteWords.setImageResource(R.drawable.ic_favorite_gray_filled)
        } else {
            holder.binding.btnFavoriteWords.setImageResource(R.drawable.ic_favorite_gray)
        }
        holder.binding.btnFavoriteWords.setOnClickListener {
            isFavorite = revertIsFavorite(isFavorite)
            if (isFavorite == 1) {
                holder.binding.btnFavoriteWords.setImageResource(R.drawable.ic_favorite_gray_filled)
            } else {

                holder.binding.btnFavoriteWords.setImageResource(R.drawable.ic_favorite_gray)
            }
            mListener.favoriteClick(it, item, position, isFavorite)
        }

        val existNote = item.note
        if (existNote != "") {
            holder.binding.tvAddNoteWords.text = existNote
        }
        holder.binding.tvAddNoteWords.setOnClickListener {
            val myDialog = Dialog(mContext)
            myDialog.setContentView(R.layout.dialog_name)
            val edtContent: EditText = myDialog.findViewById(R.id.edtContent)
            val btnCancel: Button = myDialog.findViewById(R.id.btn_cancel)
            val btnAgree: Button = myDialog.findViewById(R.id.btn_agree)
            val tvTitle: TextView = myDialog.findViewById(R.id.tv_title_dialog)

            tvTitle.text = mContext.resources.getText(R.string.add_note)
            edtContent.setText(existNote)

            btnAgree.setOnClickListener {
                val result = edtContent.text.toString()
                if (result != "") {
                    holder.binding.tvAddNoteWords.text = result
                    mListener.addNoteCallback(it, item, position, result)
                }
                myDialog.cancel()
            }

            btnCancel.setOnClickListener {
                myDialog.cancel()
            }
            myDialog.show()
        }

        holder.binding.btnSpeak.setOnClickListener {
            mListener.audioClick(it, item, position)
        }


    }

    fun setAdapterData(data: MutableList<ItemWord>) {
        this.data = data
        notifyDataSetChanged()
    }

    private fun revertIsFavorite(origin: Int): Int {
        return if (origin == 1) {
            0
        } else 1
    }
}

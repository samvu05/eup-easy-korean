package mobi.eup.easykorean.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import mobi.eup.easykorean.databinding.ItemRccAudioManagerBinding
import mobi.eup.easykorean.data.model.ItemAudio

/**
 * Created by Dinh Sam Vu on 1/21/2021.
 */
class RccAudioAdapter(
    private var data: List<ItemAudio>,
    private val onItemClick: (String) -> Unit,
    private val onSeemoreBtnClick: (String) -> Unit
) :
    RecyclerView.Adapter<RccAudioAdapter.ViewHoler>() {
    class ViewHoler(val binding: ItemRccAudioManagerBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHoler {
        val binding =
            ItemRccAudioManagerBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return ViewHoler(binding)
    }

    override fun onBindViewHolder(holder: ViewHoler, position: Int) {
        val itemData = data[position]
        holder.binding.itemData = itemData
        holder.binding.root.setOnClickListener {
            onItemClick(itemData.itemID)
        }

        holder.binding.btnGoto.setOnClickListener {
            onSeemoreBtnClick(itemData.itemID)
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun setAdapterData(data: List<ItemAudio>) {
        data.also { it.also { it.also { this.data = it } } }
        notifyDataSetChanged()
    }
}

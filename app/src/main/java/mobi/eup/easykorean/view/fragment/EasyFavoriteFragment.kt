package mobi.eup.easykorean.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import mobi.eup.easykorean.databinding.FragmentEasyFavoriteBinding
import mobi.eup.easykorean.data.model.ItemFeed
import mobi.eup.easykorean.view.adapter.RccNewsAdapter
import mobi.eup.easykorean.view.base.BaseFragment
import mobi.eup.easykorean.viewmodel.EasyFavoriteViewModel

class EasyFavoriteFragment : BaseFragment() {
    private lateinit var binding: FragmentEasyFavoriteBinding
    private lateinit var model: EasyFavoriteViewModel
    private lateinit var mAdapter: RccNewsAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentEasyFavoriteBinding.inflate(inflater, container, false)
        model = EasyFavoriteViewModel(mContext)
        binding.lifecycleOwner = this
        mAdapter = RccNewsAdapter(mutableListOf(), mContext, onItemClick)
        binding.recyclerviewEasyFavorite.setHasFixedSize(true)
        binding.recyclerviewEasyFavorite.apply {
            adapter = mAdapter
            layoutManager = LinearLayoutManager(inflater.context)
        }

        model.getListFavorite().observe(viewLifecycleOwner, {
            if (it.isEmpty()){
                binding.included.placeHolder.visibility = View.VISIBLE
            }
            else{
                binding.included.placeHolder.visibility = View.GONE
            }
            mAdapter.setAdapterData(it)
        })
        return binding.root
    }

    private val onItemClick: (ItemFeed) -> Unit = {
        model.taskUpdateSeen(it.id, 1)
    }
}
package mobi.eup.easykorean.view.activity

import android.media.MediaPlayer
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import mobi.eup.easykorean.R
import mobi.eup.easykorean.admob.AdsNative
import mobi.eup.easykorean.admob.BannerEvent
import mobi.eup.easykorean.data.model.ItemVoice
import mobi.eup.easykorean.databinding.ActivityListenAndRepeatBinding
import mobi.eup.easykorean.event.VoiceItemCallback
import mobi.eup.easykorean.utils.app.Utils
import mobi.eup.easykorean.view.adapter.RccVoiceSelectAdapter
import mobi.eup.easykorean.view.base.BaseActivity
import mobi.eup.easykorean.view.bottomsheet.BSSettingContainer
import mobi.eup.easykorean.viewmodel.ListenRepeatViewModel

class ListenAndRepeatActivity : BaseActivity(), View.OnClickListener, VoiceItemCallback,
    BannerEvent {

    private lateinit var binding: ActivityListenAndRepeatBinding
    private lateinit var toolbar: Toolbar
    private lateinit var progessBar: ProgressBar
    private lateinit var model: ListenRepeatViewModel

    private var listContent = mutableListOf<String>()
    private var currentSentence = 0
    private val mediaPlayer = MediaPlayer()
    private lateinit var currentItem: ItemVoice
    private lateinit var rccVoiceSelectAdapter: RccVoiceSelectAdapter
    private var contentData: String = ""
    private var contentImg: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_listen_and_repeat)
        model = ViewModelProvider(
            this@ListenAndRepeatActivity,
            ListenRepeatViewModel.ViewModelFactory()
        ).get(ListenRepeatViewModel::class.java)

        getItemDataFromIntent()
        initView()
        initVoice()
        initAds()
    }

    private fun initAds() {
        if (preferenceHelper.isPremiumUser()) {
            return
        }
//        mAdsBanner = AdsBanner(this@MainActivity)
//        mAdsBanner.showBannerAdsView(binding.includedViewAdsBanner.adView, false)

        adsNative = AdsNative(this@ListenAndRepeatActivity)
        adsNative.showNativeAdsView(binding.includedViewAdsBanner.adView, false)
    }

    private fun getItemDataFromIntent() {
        val bundle = intent.extras
        if (bundle != null) {
            contentData = bundle.getString(DetailActivity.SEND_CONTENT_TO_LISTEN_ACTIVITY)!!
            contentData = intent.getStringExtra(DetailActivity.SEND_CONTENT_TO_LISTEN_ACTIVITY)!!
            contentData = Utils.getCorrectForSplit(contentData)
            listContent = contentData.split(".") as MutableList<String>
            contentImg = bundle.getString(DetailActivity.SEND_LINK_IMG_TO_LISTEN_ACTIVITY)!!
        }
    }

    private fun initView() {
        progessBar = binding.pb
        progessBar.max = listContent.size
        progessBar.progress = currentSentence + 1
        rccVoiceSelectAdapter = RccVoiceSelectAdapter(this)
        Utils.loadImage(binding.imvListenRepeat, contentImg)

        toolbar = binding.toolBar
        setSupportActionBar(toolbar)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayShowTitleEnabled(true)
            supportActionBar!!.title = "Listening"
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
        binding.rccChooseSpeaker.apply {
            adapter = this@ListenAndRepeatActivity.rccVoiceSelectAdapter
            layoutManager =
                LinearLayoutManager(
                    this@ListenAndRepeatActivity,
                    LinearLayoutManager.HORIZONTAL,
                    false
                )
        }
        binding.tvCurrentText.text = listContent[0]
        binding.btnNext.setOnClickListener(this)
        binding.btnPrevious.setOnClickListener(this)
    }

    private fun initVoice() {
        val speakerID = preferenceHelper.getSpeakerID()
        currentItem = if (speakerID == 14) ItemVoice("HYERYUN", 14, R.drawable.ava_hyeryun)
        else ItemVoice("JIHUN", 18, R.drawable.ava_jihun)

        binding.nameVoiceSelectedTv.text = currentItem.name
        binding.headsetBtn.setImageResource(currentItem.srcImg)

        model.getLinkMp3(listContent[0], currentItem.talkID)
        model.linkMp3.observe(this@ListenAndRepeatActivity, {
            playAudio(it)
        })
    }

    private fun playAudio(link: String) {
        try {
            if (mediaPlayer.isPlaying) mediaPlayer.stop()
            mediaPlayer.reset()
            mediaPlayer.setDataSource(link)
            mediaPlayer.prepareAsync()
            mediaPlayer.setOnPreparedListener {
                mediaPlayer.start()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                binding.btnNext.id -> {
                    if (currentSentence < listContent.size - 1) {
                        currentSentence++
                    }
                    loadData()
                }
                binding.btnPrevious.id -> {
                    if (currentSentence >= 1) {
                        currentSentence--
                    }
                    loadData()
                }
            }
        }
    }

    private fun loadData() {
        if (mediaPlayer.isPlaying) {
            mediaPlayer.pause()
        }
        if (listContent.isEmpty()) return
        progessBar.progress = currentSentence + 1
        binding.tvCurrentText.text = listContent[currentSentence]
        when (currentSentence) {
            0 -> {
                binding.btnPrevious.setBackgroundResource(R.color.colorTextHint)
                binding.btnNext.setBackgroundResource(R.color.colorBackgroundTabSelect)
            }
            listContent.size - 1 -> {
                binding.btnPrevious.setBackgroundResource(R.color.colorBackgroundTabSelect)
                binding.btnNext.setBackgroundResource(R.color.colorTextHint)
            }
            else -> {
                binding.btnPrevious.setBackgroundResource(R.color.colorBackgroundTabSelect)
                binding.btnNext.setBackgroundResource(R.color.colorBackgroundTabSelect)
            }
        }
        model.getLinkMp3(listContent[currentSentence], currentItem.talkID)
    }

    override fun onItemClick(item: ItemVoice) {
        currentItem = item
        binding.nameVoiceSelectedTv.text = item.name
        binding.headsetBtn.setImageResource(item.srcImg)
        loadData()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.listen_repeat_toolbar_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.item_settings_listen_repeat -> {
                val bottomSheetSettingAll = BSSettingContainer(false)
                bottomSheetSettingAll.show(supportFragmentManager, bottomSheetSettingAll.tag)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onPause() {
        super.onPause()
        mediaPlayer.release()
    }

    override fun onDestroy() {
        super.onDestroy()
        mediaPlayer.release()
    }

    override fun updateContentViewWithBanner(height: Int) {
        val params = binding.contentLayout.layoutParams as RelativeLayout.LayoutParams
        params.setMargins(0, 0, 0, height)
        binding.contentLayout.layoutParams = params
        binding.contentLayout.requestLayout()
    }
}
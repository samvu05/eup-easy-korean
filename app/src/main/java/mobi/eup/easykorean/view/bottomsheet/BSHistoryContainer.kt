package mobi.eup.easykorean.view.bottomsheet

import android.app.Activity
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioGroup
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import biz.laenger.android.vpbs.ViewPagerBottomSheetDialogFragment
import mobi.eup.easykorean.database.repo.MyDatabaseRepo
import mobi.eup.easykorean.databinding.BottomSheetHistoryContainerBinding
import mobi.eup.easykorean.view.adapter.SlidePagerAdapterCommon
import mobi.eup.easykorean.viewmodel.BSHistoryContainerViewModel
import info.hoang8f.android.segmented.SegmentedGroup

@Suppress("DEPRECATION")
class BSHistoryContainer : ViewPagerBottomSheetDialogFragment(),
    RadioGroup.OnCheckedChangeListener,
    ViewPager.OnPageChangeListener {
    private lateinit var binding: BottomSheetHistoryContainerBinding
    private lateinit var frBottomSheetHistoryNews: BSHistoryNews
    private lateinit var frBottomSheetHistoryWord: BSHistoryWord
    private lateinit var mPaper: ViewPager
    private lateinit var mContext: FragmentActivity
    private lateinit var pagerAdapter: SlidePagerAdapterCommon
    private lateinit var mSegmentControl: SegmentedGroup
    private lateinit var myDatabaseRepo: MyDatabaseRepo
    private lateinit var model: BSHistoryContainerViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = BottomSheetHistoryContainerBinding.inflate(inflater, container, false)
        model = ViewModelProvider(this, BSHistoryContainerViewModel.ViewModelFactory(mContext)).get(
            BSHistoryContainerViewModel::class.java
        )
        initView()
        return binding.root
    }

    private fun initView() {
        myDatabaseRepo = MyDatabaseRepo(mContext)
        frBottomSheetHistoryNews = BSHistoryNews()
        frBottomSheetHistoryWord = BSHistoryWord()
        mSegmentControl = binding.segmentControlHistory
        mPaper = binding.viewPagerHistoryContainer
        mPaper.addOnPageChangeListener(this)

        pagerAdapter = SlidePagerAdapterCommon(childFragmentManager)
        pagerAdapter.addFragment(frBottomSheetHistoryNews, "News")
        pagerAdapter.addFragment(frBottomSheetHistoryWord, "Words")
        mPaper.adapter = pagerAdapter
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mPaper.isNestedScrollingEnabled = true
        }
        mPaper.offscreenPageLimit = pagerAdapter.count

        mSegmentControl.check(binding.btnHistoryNews.id)
        mSegmentControl.setOnCheckedChangeListener(this)

        binding.btnCloseHistory.setOnClickListener {
            dismiss()
        }
        binding.btnClearAllHistory.setOnClickListener {
            if (mPaper.currentItem == 0) {
                model.deleteAllNewsHistory()
            } else if (mPaper.currentItem == 1) {
                model.deleteAllWordHistory()
            }
        }
    }

    override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
        when (checkedId) {
            binding.btnHistoryNews.id -> {
                mPaper.currentItem = 0
            }
            binding.btnHistoryWord.id -> {
                mPaper.currentItem = 1
            }
        }
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
        if (position == 0) {
            mSegmentControl.check(binding.btnHistoryNews.id)
        } else mSegmentControl.check(binding.btnHistoryWord.id)
    }

    override fun onPageScrollStateChanged(state: Int) {
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        mContext = activity as FragmentActivity
    }
}
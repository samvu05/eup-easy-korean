package mobi.eup.easykorean.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import mobi.eup.easykorean.databinding.FragmentHardFavoriteBinding
import mobi.eup.easykorean.data.model.ItemFeed
import mobi.eup.easykorean.view.adapter.RccNewsAdapter
import mobi.eup.easykorean.view.base.BaseFragment
import mobi.eup.easykorean.viewmodel.HardFavoriteViewModel

class HardFavoriteFragment : BaseFragment() {
    private lateinit var binding: FragmentHardFavoriteBinding
    private lateinit var model: HardFavoriteViewModel
    private lateinit var mAdapter : RccNewsAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentHardFavoriteBinding.inflate(inflater, container, false)
        model = HardFavoriteViewModel(mContext)
        binding.rccData = model
        binding.lifecycleOwner = this
        mAdapter = RccNewsAdapter(mutableListOf(), mContext, onItemClick)
        binding.recyclerviewEasyFavorite.hasFixedSize()
        binding.recyclerviewEasyFavorite.apply {
            adapter = mAdapter
            layoutManager = LinearLayoutManager(inflater.context)
        }

        model.getListFavorite().observe(viewLifecycleOwner, {
            mAdapter.setAdapterData(it)
            if (it.isEmpty()){
                binding.included.placeHolder.visibility = View.VISIBLE
            }
            else{
                binding.included.placeHolder.visibility = View.GONE
            }

        })
        return binding.root
    }

    private val onItemClick: (ItemFeed) -> Unit = {
        model.taskaUpdateSeen(it.id, 1)
    }

}
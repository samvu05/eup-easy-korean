package mobi.eup.easykorean.view.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.recyclerview.widget.RecyclerView
import com.yuyakaido.android.cardstackview.Direction
import mobi.eup.easykorean.R
import mobi.eup.easykorean.data.model.ItemWord
import mobi.eup.easykorean.databinding.ItemWordsCardBinding
import mobi.eup.easykorean.event.WordCardSwipeCallback
import mobi.eup.easykorean.event.WordsClickCallback
import kotlin.random.Random

class WordsCardAdapter(
    listNewWords: MutableList<ItemWord>,
    val context: Context,
    private val swipeCallback: WordCardSwipeCallback,
    listener: WordsClickCallback
) : RecyclerView.Adapter<WordsCardAdapter.ViewHolder>() {

    private var data: MutableList<ItemWord> = listNewWords
    private val mListener: WordsClickCallback = listener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ItemWordsCardBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = data.size

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.pageTv.text = (position + 1).toString() + "/" + data.size

        val item = data[position]
        holder.binding.itemWordsCardName.text = item.word
        holder.binding.itemWordsCardPhoneticBack.text = "「" + item.phonetic + "」"
        holder.binding.itemWordsCardMeanBack.text = item.mean

        val colorBackground: Int
//        val randomColor = Random.nextInt(1,5)
//        when (randomColor) {
//            1 -> colorBackground = R.drawable.bg_round_red_card
//            2 -> colorBackground = R.drawable.bg_round_orange_card
//            3 -> colorBackground = R.drawable.bg_round_yellow_card
//            4 -> colorBackground = R.drawable.bg_round_green_card
//            5 -> colorBackground = R.drawable.bg_round_blue_card
//            else -> {
//                colorBackground = R.drawable.bg_round_green_card
//            }
//        }
//        holder.binding.relativeCardFront.setBackgroundResource(colorBackground);

        var isFavorite = item.isFavorite
        if (isFavorite == 1) {
            holder.binding.btnFavorite.setImageResource(R.drawable.ic_favorite_filled)
        } else {
            holder.binding.btnFavorite.setImageResource(R.drawable.ic_favorite)
        }

        holder.binding.btnFavorite.setOnClickListener {
            isFavorite = revertIsFavorite(isFavorite)
            if (isFavorite == 1) {
                holder.binding.btnFavorite.setImageResource(R.drawable.ic_favorite_filled)
            } else {

                holder.binding.btnFavorite.setImageResource(R.drawable.ic_favorite)
            }
            mListener.favoriteClick(it, item, position, isFavorite)
        }

        holder.binding.btnRemember.setOnClickListener {
            swipeCallback.execute(Direction.Right)
        }

        holder.binding.btnNotSure.setOnClickListener {
            swipeCallback.execute(Direction.Top)
        }

        holder.binding.btnDontKnow.setOnClickListener {
            swipeCallback.execute(Direction.Left)
        }
        holder.binding.btnSpeak.setOnClickListener {
            mListener.audioClick(it, item, position)
        }
    }

    fun setAdapterData(data: MutableList<ItemWord>) {
        this.data = data
        notifyDataSetChanged()
    }

    private fun revertIsFavorite(origin: Int): Int {
        return if (origin == 1) {
            0
        } else 1
    }

    class ViewHolder(val binding: ItemWordsCardBinding) :
        RecyclerView.ViewHolder(binding.root)
}

package mobi.eup.easykorean.view.bottomsheet

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import mobi.eup.easykorean.R
import mobi.eup.easykorean.databinding.FragmentReviewPremiumBinding
import mobi.eup.easykorean.view.base.BaseFragment

@Suppress("DEPRECATION")
class ReviewPremiumFragment : BaseFragment() {
    private lateinit var binding : FragmentReviewPremiumBinding
    private var pos = 0

    companion object {
        fun newInstance(pos: Int): ReviewPremiumFragment {
            val args = Bundle()
            args.putInt("POSITION", pos)
            val fragment = ReviewPremiumFragment()
            fragment.arguments = args
            return fragment
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentReviewPremiumBinding.inflate(inflater,container,false)
        return binding.root
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val bundle: Bundle? = arguments
        if (bundle != null) pos = bundle.getInt("POSITION")
        try {
            when (pos) {
                3 -> {
                    binding.ivReview.setImageDrawable(resources.getDrawable(R.drawable.review_premium_no_ads))
                    binding.tvTitle.text = getString(R.string.no_ads)
                    binding.tvContent.text = getString(R.string.review_no_ads_desc)
                }
                2 -> {
                    binding.ivReview.setImageDrawable(resources.getDrawable(R.drawable.review_premium_news))
                    binding.tvTitle.text = getString(R.string.review_news_offline)
                    binding.tvContent.text = getString(R.string.review_news_offline_desc)
                }
                1 -> {
                    binding.ivReview.setImageDrawable(resources.getDrawable(R.drawable.review_premium_listen_audio))
                    binding.tvTitle.text = getString(R.string.review_download_audio_offline)
                    binding.tvContent.text = getString(R.string.review_download_audio_offline_desc)
                }
            }
        } catch (e: OutOfMemoryError) {
            e.printStackTrace()
        }
    }
}
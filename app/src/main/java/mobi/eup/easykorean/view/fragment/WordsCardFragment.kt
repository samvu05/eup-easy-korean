package mobi.eup.easykorean.view.fragment

import android.media.MediaPlayer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import android.view.animation.LinearInterpolator
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import mobi.eup.easykorean.data.model.ItemWord
import mobi.eup.easykorean.databinding.FragmentWordCardBinding
import mobi.eup.easykorean.event.WordCardSwipeCallback
import mobi.eup.easykorean.event.WordsClickCallback
import mobi.eup.easykorean.utils.app.NetworkHelper
import mobi.eup.easykorean.utils.translate.GetGoogleTranslateHelper
import mobi.eup.easykorean.view.adapter.WordsCardAdapter
import mobi.eup.easykorean.view.base.BaseFragment
import mobi.eup.easykorean.viewmodel.WordCardViewModel
import com.yuyakaido.android.cardstackview.*
import kotlinx.coroutines.*
import mobi.eup.easykorean.R

@Suppress("DEPRECATION")
class WordsCardFragment(private val listWordID: List<String>) : BaseFragment(), CardStackListener,
    WordCardSwipeCallback {
    private lateinit var binding: FragmentWordCardBinding
    private lateinit var cardStackView: CardStackView
    private lateinit var manager: CardStackLayoutManager
    private lateinit var mAdapter: WordsCardAdapter
    private lateinit var model: WordCardViewModel

    private var listCurrent = mutableListOf<ItemWord>()
    private var listFavorite = mutableListOf<ItemWord>()
    private val mediaPlayer = MediaPlayer()
    private var isFavorite = false
    private val scope = MainScope()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentWordCardBinding.inflate(inflater, container, false)
        model = ViewModelProvider(
            this@WordsCardFragment,
            WordCardViewModel.ViewModelFactory(mContext, listWordID)
        ).get(WordCardViewModel::class.java)

        cardStackView = binding.cardStackView
        binding.lifecycleOwner = this
        binding.placeHolderTv.visibility = View.GONE

        manager = CardStackLayoutManager(mContext, this)
        mAdapter = WordsCardAdapter(
            mutableListOf(),
            mContext, this, mListener
        )

        setupCardStackView()

        model.listCurrent.observe(viewLifecycleOwner, {
            this@WordsCardFragment.listCurrent = it
            if (!isFavorite) {
                mAdapter.setAdapterData(listCurrent)
            }
            if (listCurrent.size == listWordID.size && listCurrent.size > 0) {
                updateMean()
            }
            binding.placeHolderTv.visibility =
                if (listFavorite.isEmpty() && !this@WordsCardFragment.isFavorite) View.VISIBLE else View.GONE
        })

        model.getListFavorite().observe(viewLifecycleOwner, {
            this@WordsCardFragment.listFavorite = it
            binding.placeHolderTv.visibility =
                if (listFavorite.isEmpty() && this@WordsCardFragment.isFavorite) View.VISIBLE else View.GONE
        })
        return binding.root
    }

    private fun setupCardStackView() {
        manager.setStackFrom(StackFrom.Top)
        manager.setVisibleCount(3)
        manager.setTranslationInterval(8.0f)
        manager.setScaleInterval(0.95f)
        manager.setSwipeThreshold(0.3f)
        manager.setMaxDegree(20.0f)
        manager.setDirections(Direction.HORIZONTAL)
        manager.setCanScrollHorizontal(true)
        manager.setCanScrollVertical(true)
        manager.setSwipeableMethod(SwipeableMethod.AutomaticAndManual)
        manager.setOverlayInterpolator(LinearInterpolator())
        cardStackView.layoutManager = manager
        cardStackView.adapter = mAdapter
        cardStackView.itemAnimator.apply {
            if (this is DefaultItemAnimator) {
                supportsChangeAnimations = false
            }
        }
    }

    private fun updateMean() {
        if (!NetworkHelper.isNetWork(mContext)) return
        val currentLanguage = preferenceHelper.getCurrentLanguageCode()
        scope.launch {
            val listJob = mutableListOf<Deferred<String>>()
            for (item in listCurrent) {
                val job = CoroutineScope(Dispatchers.IO).async {
                    GetGoogleTranslateHelper.getMeanAndPhoneticByWord(
                        "ko",
                        currentLanguage,
                        item.word
                    )
                }
                listJob.add(job)
            }
            for (i in listJob.indices) {
                val data = listJob[i].await().split("_")
                if (data.size > 2) {
                    listCurrent[i].mean = data[1]
                    listCurrent[i].phonetic = data[2]
                }
            }
            if (!isFavorite) {
                mAdapter.setAdapterData(listCurrent)
            }

        }
    }

    override fun onCardDragging(direction: Direction?, ratio: Float) {
    }

    override fun onCardSwiped(direction: Direction?) {
    }

    override fun onCardRewound() {
    }

    override fun onCardCanceled() {
    }

    override fun onCardAppeared(view: View?, position: Int) = Unit

    override fun onCardDisappeared(view: View?, position: Int) {
    }

    override fun execute(direction: Direction) {
        when (direction) {
            Direction.Left -> {
                val setting = SwipeAnimationSetting.Builder()
                    .setDirection(Direction.Left)
                    .setDuration(Duration.Normal.duration)
                    .setInterpolator(AccelerateInterpolator())
                    .build()
                manager.setSwipeAnimationSetting(setting)
                cardStackView.swipe()
            }
            Direction.Right -> {
                val setting = SwipeAnimationSetting.Builder()
                    .setDirection(Direction.Right)
                    .setDuration(Duration.Normal.duration)
                    .setInterpolator(AccelerateInterpolator())
                    .build()
                manager.setSwipeAnimationSetting(setting)
                cardStackView.swipe()
            }
            Direction.Top -> {
                val setting = RewindAnimationSetting.Builder()
                    .setDirection(Direction.Top)
                    .setDuration(Duration.Normal.duration)
                    .setInterpolator(DecelerateInterpolator())
                    .build()
                manager.setRewindAnimationSetting(setting)
                cardStackView.swipe()
            }
            else -> return
        }
    }

    fun onActionFavorite(isFavorite: Boolean) {
        this@WordsCardFragment.isFavorite = isFavorite
        if (isFavorite) {
            mAdapter.setAdapterData(listFavorite)
            binding.placeHolderTv.visibility =
                if (listFavorite.isEmpty()) View.VISIBLE else View.GONE
        } else {
            mAdapter.setAdapterData(listCurrent)
            binding.placeHolderTv.visibility =
                if (listCurrent.isEmpty()) View.VISIBLE else View.GONE
        }
    }

    private val mListener = object : WordsClickCallback {
        override fun onItemClick(view: View, item: ItemWord, position: Int) {
        }

        override fun audioClick(view: View, item: ItemWord, position: Int) {
            model.getWordLinkMp3(item.word, preferenceHelper.getSpeakerID())
            model.wordLinkMp3.observe(this@WordsCardFragment, {
                playAudio(it)
            })
        }

        override fun addNoteCallback(view: View, item: ItemWord, position: Int, note: String) {
        }

        override fun audioLongClick(word: String) {
        }

        override fun favoriteClick(view: View, item: ItemWord, position: Int, isFavorite: Int) {
            if (!model.checkExits(item.word)) {
                model.insert(item)
            }
            model.updateFavorite(item.word, isFavorite)
            showToastFavoriteAction(isFavorite == 1)
        }
    }

    fun playAudio(link: String) {
        try {
            mediaPlayer.reset()
            mediaPlayer.setDataSource(link)
            mediaPlayer.prepareAsync()
            mediaPlayer.setOnPreparedListener {
                mediaPlayer.start()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun showToastFavoriteAction(isFavorite: Boolean) {
        Toast.makeText(
            mContext,
            if (isFavorite) {
                resources.getString(R.string.add_word_to_favorite)
            } else {
                resources.getString(R.string.remove_word_from_favorite)
            },
            Toast.LENGTH_SHORT
        ).show()
    }

    override fun onDetach() {
        super.onDetach()
        mediaPlayer.release()
        scope.cancel()
    }
}

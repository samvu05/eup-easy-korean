package mobi.eup.easykorean.view.bottomsheet

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import mobi.eup.easykorean.databinding.BottomSheetHistoryNewsBinding
import mobi.eup.easykorean.utils.app.GlobalHelper
import mobi.eup.easykorean.view.activity.DetailActivity
import mobi.eup.easykorean.view.adapter.RccHistoryNewsAdapter
import mobi.eup.easykorean.view.base.BaseFragment
import mobi.eup.easykorean.viewmodel.HistoryNewsViewmodel

@Suppress("DEPRECATION")
class BSHistoryNews : BaseFragment() {
    private lateinit var binding: BottomSheetHistoryNewsBinding
    private lateinit var model: HistoryNewsViewmodel
    private lateinit var mAdapter: RccHistoryNewsAdapter


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = BottomSheetHistoryNewsBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        initView(inflater)
        return binding.root
    }

    private fun initView(inflater: LayoutInflater) {
        model = HistoryNewsViewmodel(mContext)
        binding.rccData = model
        binding.lifecycleOwner = this
        mAdapter = RccHistoryNewsAdapter( mutableListOf(), onItemClick)
        binding.rccHistoryNew.apply {
            adapter = mAdapter
            layoutManager = LinearLayoutManager(inflater.context)
        }

        model.getAll().observe(viewLifecycleOwner, {
            mAdapter.setAdapterData(it)
            binding.included.placeHolder.visibility = if (it.isEmpty()) View.VISIBLE else View.GONE
        })

    }

    private val onItemClick: (String) -> Unit = {
        val intent = Intent(mContext, DetailActivity::class.java)
        intent.putExtra(GlobalHelper.ITEM_ID_EXTRA, it)
        mContext.startActivity(intent)

    }
}
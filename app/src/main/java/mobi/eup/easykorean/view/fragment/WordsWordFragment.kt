package mobi.eup.easykorean.view.fragment

import android.media.MediaPlayer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import mobi.eup.easykorean.data.model.ItemWord
import mobi.eup.easykorean.databinding.FragmentWordWordBinding
import mobi.eup.easykorean.event.WordsClickCallback
import mobi.eup.easykorean.utils.app.NetworkHelper
import mobi.eup.easykorean.utils.translate.GetGoogleTranslateHelper
import mobi.eup.easykorean.view.adapter.RccWordsAdapter
import mobi.eup.easykorean.view.base.BaseFragment
import mobi.eup.easykorean.viewmodel.WordWordViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import mobi.eup.easykorean.R
import java.util.*


@Suppress("DEPRECATION")
open class WordsWordFragment(private val listWordID: List<String>) : BaseFragment() {
    private lateinit var binding: FragmentWordWordBinding
    private lateinit var model: WordWordViewModel
    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mAdapter: RccWordsAdapter

    private var listCurrent = mutableListOf<ItemWord>()
    private var listFavorite = mutableListOf<ItemWord>()
    private val mediaPlayer = MediaPlayer()
    private var isFavorite = false
    private val scope = MainScope()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentWordWordBinding.inflate(inflater, container, false)
        model = ViewModelProvider(
            this@WordsWordFragment,
            WordWordViewModel.ViewModelFactory(mContext, listWordID)
        ).get(WordWordViewModel::class.java)

        binding.lifecycleOwner = this
        binding.placeHolderTv.visibility = View.GONE
        mRecyclerView = binding.rccWordCurrent
        mAdapter = RccWordsAdapter(
            mutableListOf(), mContext, mListener
        )

        model.listCurrent.observe(viewLifecycleOwner, {
            this@WordsWordFragment.listCurrent = it
            if (!isFavorite) {
                mAdapter.setAdapterData(listCurrent)
            }
            if (listCurrent.size == listWordID.size && listCurrent.size > 0) {
                updateMean()
                updateNote()
                updateFavorite()
            }
        })

        model.getListFavorite().observe(viewLifecycleOwner, {
            this@WordsWordFragment.listFavorite = it
            updateFavorite()
            updateNote()
            if (isFavorite) {
                mAdapter.setAdapterData(listFavorite)
            }
            binding.placeHolderTv.visibility =
                if (listFavorite.isEmpty() && this@WordsWordFragment.isFavorite) View.VISIBLE else View.GONE
        })

        binding.rccWordCurrent.apply {
            adapter = mAdapter
            layoutManager = LinearLayoutManager(inflater.context)
            hasFixedSize()
        }
        return binding.root
    }

    private fun updateMean() {
        if (!NetworkHelper.isNetWork(mContext)) return
        val currentLanguage = preferenceHelper.getCurrentLanguageCode()
        scope.launch {
            val listJob = mutableListOf<Deferred<String>>()
            for (item in listCurrent) {
                val job = CoroutineScope(IO).async {
                    GetGoogleTranslateHelper.getMeanAndPhoneticByWord(
                        "ko",
                        currentLanguage,
                        item.word
                    )
                }
                listJob.add(job)
            }
            for (i in listJob.indices) {
                val data = listJob[i].await().split("_")
                if (data.size > 2) {
                    listCurrent[i].mean = data[1]
                    listCurrent[i].phonetic = data[2]
                }
            }
            if (!isFavorite) {
                mAdapter.setAdapterData(listCurrent)
            }
        }
    }

    private fun updateNote() {
        scope.launch {
            val listJob = mutableListOf<Deferred<String?>>()
            for (item in listCurrent) {
                val job = model.getNoteAsync(item.word)
                listJob.add(job)
            }
            for (i in listJob.indices) {
                listCurrent[i].note = if (listJob[i].await() == null) "" else listJob[i].await()!!
            }
            if (!isFavorite) {
                mAdapter.setAdapterData(listCurrent)
            }
        }
    }

    private fun updateFavorite() {
        scope.launch {
            val listJob = mutableListOf<Deferred<Int?>>()
            for (item in listCurrent) {
                val job = model.getFavoriteAsync(item.word)
                listJob.add(job)
            }
            for (i in listJob.indices) {
                listCurrent[i].isFavorite =
                    if (listJob[i].await() == null) 0 else listJob[i].await()!!
            }
            if (!isFavorite) {
                mAdapter.setAdapterData(listCurrent)
            }
        }
    }

    fun onActionFavorite(isFavorite: Boolean) {
        this@WordsWordFragment.isFavorite = isFavorite
        if (isFavorite) {
            mAdapter.setAdapterData(listFavorite)
            binding.placeHolderTv.visibility =
                if (listFavorite.isEmpty()) View.VISIBLE else View.GONE
        } else {
            mAdapter.setAdapterData(listCurrent)
            binding.placeHolderTv.visibility = View.GONE
        }
    }

    private val mListener = object : WordsClickCallback {
        override fun onItemClick(view: View, item: ItemWord, position: Int) {
        }

        override fun audioClick(view: View, item: ItemWord, position: Int) {
            model.getWordLinkMp3(item.word, preferenceHelper.getSpeakerID())
            model.wordLinkMp3.observe(this@WordsWordFragment, {
                playAudio(it)
            })
        }

        override fun addNoteCallback(view: View, item: ItemWord, position: Int, note: String) {
            if (!model.checkExits(item.word)) {
                model.insert(item)
            }
            model.updateNote(item.word, note)
            if (!this@WordsWordFragment.isFavorite) {
                listCurrent[position].note = note
                mAdapter.setAdapterData(listCurrent)
            }
        }

        override fun audioLongClick(word: String) {
        }

        override fun favoriteClick(view: View, item: ItemWord, position: Int, isFavorite: Int) {
            if (!model.checkExits(item.word)) {
                model.insert(item)
            }
            model.updateFavorite(item.word, isFavorite)
            showToastFavoriteAction(isFavorite == 1)
            if (!this@WordsWordFragment.isFavorite) {
                listCurrent[position].isFavorite = isFavorite
                mAdapter.setAdapterData(listCurrent)
            }
        }
    }

    fun playAudio(link: String) {
        try {
            mediaPlayer.reset()
            mediaPlayer.setDataSource(link)
            mediaPlayer.prepareAsync()
            mediaPlayer.setOnPreparedListener {
                mediaPlayer.start()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun showToastFavoriteAction(isFavorite: Boolean) {
        Toast.makeText(
            mContext,
            if (isFavorite) {
                resources.getString(R.string.add_word_to_favorite)
            } else {
                resources.getString(R.string.remove_word_from_favorite)
            },
            Toast.LENGTH_SHORT
        ).show()
    }

    override fun onDetach() {
        super.onDetach()
        mediaPlayer.release()
        scope.cancel()
    }
}

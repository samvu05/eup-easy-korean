package mobi.eup.easykorean.view.bottomsheet

import android.app.Activity
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioGroup
import androidx.fragment.app.FragmentActivity
import androidx.viewpager.widget.ViewPager
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import biz.laenger.android.vpbs.ViewPagerBottomSheetDialogFragment
import info.hoang8f.android.segmented.SegmentedGroup
import mobi.eup.easykorean.R
import mobi.eup.easykorean.databinding.BottomSheetSettingsContainerBinding
import mobi.eup.easykorean.utils.app.GlobalHelper
import mobi.eup.easykorean.utils.app.PreferenceHelper
import mobi.eup.easykorean.utils.eventbus.EventBusState
import mobi.eup.easykorean.utils.eventbus.EventSettingsHelper
import mobi.eup.easykorean.view.adapter.PagerIndicatorReviewAdapter
import mobi.eup.easykorean.view.adapter.SlidePagerAdapterCommon
import org.greenrobot.eventbus.EventBus
import java.util.*

@Suppress("DEPRECATION")
class BSSettingContainer(private val isShowUpgradeButton: Boolean) :
    ViewPagerBottomSheetDialogFragment(),
    RadioGroup.OnCheckedChangeListener,
    OnPageChangeListener {
    private lateinit var binding: BottomSheetSettingsContainerBinding
    private lateinit var frBSSettingNews: BSSettingNews
    private lateinit var frBSSettingDetail: BSSettingDetail
    private lateinit var mPaper: ViewPager
    private lateinit var mContext: FragmentActivity
    private lateinit var pagerAdapter: SlidePagerAdapterCommon
    private lateinit var mSegmentControl: SegmentedGroup
    private lateinit var preferenceHelper: PreferenceHelper

    private var viewPagerPremiumAdapter: PagerIndicatorReviewAdapter? = null
    private lateinit var timer: CountDownTimer
    private var currentPage = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = BottomSheetSettingsContainerBinding.inflate(inflater, container, false)
        preferenceHelper = PreferenceHelper(mContext, GlobalHelper.PREFERENCE_NAME_APP)
        initView()
        checkIfPremium()
        return binding.root
    }

    private fun initView() {
        frBSSettingNews = BSSettingNews()
        frBSSettingDetail = BSSettingDetail()
        mSegmentControl = binding.segmentControlSetting
        mPaper = binding.viewPagerSettingContainer
        mPaper.addOnPageChangeListener(this)

        pagerAdapter = SlidePagerAdapterCommon(childFragmentManager)
        pagerAdapter.addFragment(frBSSettingNews, "News")
        pagerAdapter.addFragment(frBSSettingDetail, "Detail")
        mPaper.adapter = pagerAdapter
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mPaper.isNestedScrollingEnabled = true
        }
        mPaper.offscreenPageLimit = pagerAdapter.count

        mSegmentControl.check(binding.btnSettingDetail.id)
        mSegmentControl.setOnCheckedChangeListener(this)
        mPaper.currentItem = 1

        setDataIndicator()

        binding.btnCloseSetting.setOnClickListener {
            dismiss()
        }
        binding.btnWhyAdsSetting.setOnClickListener {
            dismiss()
        }

        if (isShowUpgradeButton) {
            binding.btnUpgrade.visibility = View.VISIBLE
        } else {
            binding.btnUpgrade.visibility = View.GONE
        }
        binding.btnUpgrade.setOnClickListener {
            EventBus.getDefault().post(EventSettingsHelper(EventBusState.UPGRADE_PREMIUM))

        }
    }

    override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
        when (checkedId) {
            binding.btnSettingAll.id -> {
                mPaper.currentItem = 0
            }
            binding.btnSettingDetail.id -> {
                mPaper.currentItem = 1
            }
        }
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
        if (position == 0) {
            mSegmentControl.check(binding.btnSettingAll.id)
        } else mSegmentControl.check(binding.btnSettingDetail.id)
    }

    override fun onPageScrollStateChanged(state: Int) {
    }

    private fun setDataIndicator() {
        val fragments: MutableList<ReviewPremiumFragment> = ArrayList<ReviewPremiumFragment>()
        fragments.add(ReviewPremiumFragment.newInstance(1))
        fragments.add(ReviewPremiumFragment.newInstance(2))
        fragments.add(ReviewPremiumFragment.newInstance(3))
        if (viewPagerPremiumAdapter == null) {
            viewPagerPremiumAdapter = PagerIndicatorReviewAdapter(childFragmentManager)
            viewPagerPremiumAdapter!!.addAllFragments(fragments)
            binding.viewPagerPremium.adapter = viewPagerPremiumAdapter
            setupViewPager()
        } else {
            viewPagerPremiumAdapter!!.setNewFragments(fragments)
        }
    }

    private fun setupViewPager() {
        setupAutoPager()
        binding.viewPagerPremium.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                binding.pageIndicatorView.selection = position % 3
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })
    }

    private fun setupAutoPager() {
        timer = object : CountDownTimer(5000, 1000) {
            override fun onTick(millisUntilFinished: Long) {}
            override fun onFinish() {
                currentPage = binding.viewPagerPremium.currentItem + 1
                if (currentPage == 3) currentPage = 0
                try {
                    binding.viewPagerPremium.setCurrentItem(currentPage, true)
                } catch (e: OutOfMemoryError) {
                    Log.e("error", "OutOfMemoryError")
                } catch (e: IllegalStateException) {
                    Log.e("error", "IllegalStateException")
                }
                timer.start()
            }
        }
        timer.start()
    }

    /**
     * check if user is premium: change layout
     */
    private fun checkIfPremium() {
        if (!preferenceHelper.isPremiumUser()) return
        binding.btnUpgrade.setBackgroundResource(R.drawable.bg_round_yellow_card)
        binding.btnUpgrade.setText(getString(R.string.you_are_a_premium_user))
        binding.tvActive.setVisibility(View.GONE)
        binding.tvRestore.setVisibility(View.GONE)
        binding.ivInfoActive.setVisibility(View.GONE)
    }


    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        mContext = activity as FragmentActivity
    }

    override fun onDetach() {
        super.onDetach()
    }

    override fun onDestroy() {
        super.onDestroy()
        timer.cancel()
    }

}
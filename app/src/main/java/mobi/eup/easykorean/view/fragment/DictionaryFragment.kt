package mobi.eup.easykorean.view.fragment

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.view.*
import android.view.View.*
import android.view.ViewTreeObserver.OnScrollChangedListener
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import mobi.eup.easykorean.R
import mobi.eup.easykorean.databinding.FragmentDictionaryBinding
import mobi.eup.easykorean.utils.app.NetworkHelper

@Suppress("DEPRECATION")
class DictionaryFragment : Fragment(), OnClickListener, OnTouchListener {
    private lateinit var binding: FragmentDictionaryBinding
    private lateinit var mOnScrollChangedListener: OnScrollChangedListener
    private lateinit var webView: WebView
    private lateinit var rfLayout: SwipeRefreshLayout
    private lateinit var ivError: ImageView
    private lateinit var tvError: TextView
    private lateinit var imgBack: ImageView
    private lateinit var imgHome: ImageView
    private lateinit var imgNext: ImageView

    @SuppressLint("SetJavaScriptEnabled", "ClickableViewAccessibility")
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDictionaryBinding.inflate(inflater, container, false)
        ivError = binding.imageView
        tvError = binding.tvError
        imgBack = binding.imgBack
        imgHome = binding.imgHome
        imgNext = binding.imgNext
        rfLayout = binding.rfLayout

        webView = binding.webviewDictionary
        webView.webViewClient = WebViewClient()
        webView.settings.javaScriptEnabled = true
        webView.webChromeClient = WebChromeClient()

        if (NetworkHelper.isNetWork(inflater.context)) {
            webView.loadUrl("https://dict.naver.com/")
            webView.visibility = VISIBLE
            ivError.visibility = GONE
            tvError.visibility = GONE
        } else {
            webView.visibility = GONE
            ivError.visibility = VISIBLE
            tvError.visibility = VISIBLE
        }

        webView.viewTreeObserver.addOnScrollChangedListener(OnScrollChangedListener {
            rfLayout.isEnabled = webView.scrollY == 0
        }.also { mOnScrollChangedListener = it })

        rfLayout.setOnRefreshListener {
            if (NetworkHelper.isNetWork(inflater.context)) {
                webView.loadUrl("https://dict.naver.com/")
                webView.visibility = VISIBLE
                ivError.visibility = GONE
                tvError.visibility = GONE
                rfLayout.isRefreshing = false
            } else {
                webView.visibility = GONE
                ivError.visibility = VISIBLE
                tvError.visibility = VISIBLE
                rfLayout.isRefreshing = false
            }
        }

        if (webView.canGoForward()) {
            imgNext.setAlpha(228)
        } else {
            imgNext.setAlpha(50)
        }
        if (webView.canGoBack()) {
            imgBack.setAlpha(228)
        } else {
            imgBack.setAlpha(50)
        }
        webView.setOnTouchListener(this)
        imgHome.setOnClickListener(this)
        imgBack.setOnClickListener(this)
        imgNext.setOnClickListener(this)

        return binding.root
    }

    override fun onStop() {
        binding.rfLayout.viewTreeObserver
            .removeOnScrollChangedListener(mOnScrollChangedListener)
        super.onStop()
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.imgHome -> webView.loadUrl("https://dict.naver.com/")
            R.id.imgNext -> if (webView.canGoForward()) {
                webView.goForward()
            }
            R.id.imgBack -> if (webView.canGoBack()) {
                webView.goBack()
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouch(view: View?, p1: MotionEvent?): Boolean {
        if (webView.canGoForward()) {
            imgNext.setAlpha(228)
        } else {
            imgNext.setAlpha(50)
        }


        if (webView.canGoBack()) {
            imgBack.setAlpha(228)
        } else {
            imgBack.setAlpha(50)
        }
        return false
    }
}
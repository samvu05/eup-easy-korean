package mobi.eup.easykorean.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import mobi.eup.easykorean.databinding.ItemHistoryNewsBinding
import mobi.eup.easykorean.data.model.ItemFeed

class RccHistoryNewsAdapter(
    private var data: MutableList<ItemFeed>,
    val onItemClick: (String) -> Unit
) :
    RecyclerView.Adapter<RccHistoryNewsAdapter.ViewHoler>() {

    class ViewHoler(val binding: ItemHistoryNewsBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHoler {
        val binding =
            ItemHistoryNewsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHoler(binding)
    }

    override fun onBindViewHolder(holder: ViewHoler, position: Int) {
        val itemData = data[position]
        holder.binding.itemData = itemData
        holder.binding.root.setOnClickListener {
           onItemClick(itemData.id)
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun setAdapterData(data: MutableList<ItemFeed>) {
        data.also { it.also { it.also { this.data = it } } }
        notifyDataSetChanged()
    }
}

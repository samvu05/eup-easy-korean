package mobi.eup.easykorean.view.bottomsheet

import android.app.Activity
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import biz.laenger.android.vpbs.ViewPagerBottomSheetDialogFragment
import mobi.eup.easykorean.databinding.BottomSheetAudioManagerBinding
import mobi.eup.easykorean.utils.app.GlobalHelper
import mobi.eup.easykorean.view.activity.DetailActivity
import mobi.eup.easykorean.view.adapter.RccAudioAdapter
import mobi.eup.easykorean.viewmodel.BSAudioViewModel
import java.io.File

/**
 * Created by Dinh Sam Vu on 1/21/2021.
 */
@Suppress("DEPRECATION")
class BSAudioManager : ViewPagerBottomSheetDialogFragment() {
    private lateinit var binding: BottomSheetAudioManagerBinding
    private lateinit var mContext: FragmentActivity
    private lateinit var mAdapter: RccAudioAdapter
    private lateinit var mediaPlayer: MediaPlayer
    private lateinit var model: BSAudioViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = BottomSheetAudioManagerBinding.inflate(inflater, container, false)
        model =
            ViewModelProvider(this@BSAudioManager, BSAudioViewModel.ViewModelFactory(mContext)).get(
                BSAudioViewModel::class.java
            )
        mediaPlayer = MediaPlayer()
        initView()
        return binding.root
    }

    private fun initView() {
        mAdapter = RccAudioAdapter(
            mutableListOf(),
            onItemClick,
            onSeemoreBtnClick
        )
        binding.rv.apply {
            adapter = mAdapter
            layoutManager = LinearLayoutManager(mContext)
            itemAnimator = DefaultItemAnimator()
        }

        binding.btnClose.setOnClickListener {
            dismiss()
        }

        model.getAll().observe(this, {
            binding.btnDeleteAll.visibility = if (it.isEmpty()) View.GONE else View.VISIBLE
            binding.included.placeHolder.visibility = if (it.isEmpty()) View.VISIBLE else View.GONE
            mAdapter.setAdapterData(it)
        })

        binding.btnDeleteAll.setOnClickListener {
            mediaPlayer.release()
            model.deleteALl()
        }
    }

    private val onItemClick: (String) -> Unit = {
        playAudio(it)
    }

    private val onSeemoreBtnClick: (String) -> Unit = {
        val intent = Intent(mContext, DetailActivity::class.java)
        intent.putExtra(GlobalHelper.ITEM_ID_EXTRA, it)
        mContext.startActivity(intent)

    }

    //play audio file
    private fun playAudio(itemID: String) {
        try {
            mediaPlayer.reset()
            //set file path
            if (getUriFileAudio(itemID) == null) {
                Toast.makeText(mContext, "Error load file from external !", Toast.LENGTH_SHORT)
                    .show()
                return
            } else {
                mediaPlayer.setDataSource(
                    mContext, getUriFileAudio(itemID)!!
                )
            }

            mediaPlayer.prepareAsync()
            mediaPlayer.setOnPreparedListener {
                mediaPlayer.start()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun getUriFileAudio(fileName: String): Uri? {
        val directory =
            File(mContext.getExternalFilesDir(null)!!.path + File.separator + "Downloaded Audio/" + fileName)
        if (!directory.exists()) {
            return null
        }
        return Uri.parse(directory.path)
    }


    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        mContext = activity as FragmentActivity
    }

    override fun onDetach() {
        super.onDetach()
        mediaPlayer.release()
    }

}
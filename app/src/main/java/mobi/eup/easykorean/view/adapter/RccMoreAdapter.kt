package mobi.eup.easykorean.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import mobi.eup.easykorean.data.model.ItemMore
import mobi.eup.easykorean.databinding.ItemRccListAppsBottomSheetBinding
import mobi.eup.easykorean.event.IntegerCallback

/**
 * Created by Dinh Sam Vu on 1/20/2021.
 */
class RccMoreAdapter(
    private val items: MutableList<ItemMore>,
    private var type: Int,
    private val integerCallback: IntegerCallback
) : RecyclerView.Adapter<RccMoreAdapter.ViewHolder>() {

    override fun getItemViewType(position: Int): Int {
        return type
    }

    class ViewHolder(val binding: ItemRccListAppsBottomSheetBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ItemRccListAppsBottomSheetBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (position < items.size) {
            val item = items[position]
            holder.binding.imageView.setImageDrawable(item.icon)
            holder.binding.titleTv.text = item.name
            holder.binding.descTv.text = item.desc
            holder.itemView.setOnClickListener {
                integerCallback.execute(position)
            }
        }
    }

    override fun getItemCount(): Int = items.size

}
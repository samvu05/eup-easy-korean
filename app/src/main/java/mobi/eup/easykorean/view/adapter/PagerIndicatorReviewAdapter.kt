package mobi.eup.easykorean.view.adapter

import android.os.Bundle
import android.os.Parcelable
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import mobi.eup.easykorean.view.bottomsheet.ReviewPremiumFragment


@Suppress("DEPRECATION")
class PagerIndicatorReviewAdapter(fm: FragmentManager) :
    FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private var mFragmentList = mutableListOf<ReviewPremiumFragment>()

    fun addFragment(fragment: ReviewPremiumFragment) {
        mFragmentList.add(fragment)
    }

    fun addAllFragments(listFragment: MutableList<ReviewPremiumFragment>) {
        mFragmentList.addAll(listFragment)
    }

    fun setNewFragments(listNewFragments: MutableList<ReviewPremiumFragment>) {
        mFragmentList = listNewFragments
        notifyDataSetChanged()
    }

    override fun getCount(): Int = mFragmentList.size

    override fun getItem(position: Int): Fragment{
        val index = position % mFragmentList.size
        return mFragmentList[index]
    }

    override fun getPageTitle(position: Int): CharSequence {
        return ""
    }

    override fun saveState(): Parcelable? {
        val bundle = super.saveState() as Bundle?
        bundle?.putParcelableArray(
            "states",
            null
        ) // Never maintain any states from the base class, just null it out
        return bundle
    }

}

package mobi.eup.easykorean.view.activity

import android.annotation.SuppressLint
import android.graphics.Color.*
import android.os.Bundle
import android.provider.Settings
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.webkit.WebView
import android.widget.*
import androidx.appcompat.widget.Toolbar
import androidx.core.view.ViewCompat
import androidx.core.view.ViewPropertyAnimatorListener
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import mobi.eup.easykorean.*
import mobi.eup.easykorean.data.model.Datum
import mobi.eup.easykorean.data.network.rest_support.postbody.PostTranslateBody
import mobi.eup.easykorean.event.*
import mobi.eup.easykorean.utils.app.Utils
import mobi.eup.easykorean.utils.js_interface.JavaScriptInterfaceTranslate
import mobi.eup.easykorean.utils.language.StringHelper
import mobi.eup.easykorean.utils.translate.HtmlHelper
import mobi.eup.easykorean.utils.translate.VoteHelper
import mobi.eup.easykorean.view.adapter.RccItemTranslateAdapter
import mobi.eup.easykorean.view.base.BaseActivity
import mobi.eup.easykorean.view.bottomsheet.BSSettingContainer
import mobi.eup.easykorean.viewmodel.TranslateViewModel
import java.util.*

/**
 * Created by Dinh Sam Vu on 1/18/2021.
 */
@Suppress(
    "DEPRECATION", "DEPRECATED_IDENTITY_EQUALS", "TYPE_INFERENCE_ONLY_INPUT_TYPES_WARNING",
    "NAME_SHADOWING"
)
class TranslateActivity : BaseActivity() {
    private var originTextList = mutableListOf<String>()
    private lateinit var model: TranslateViewModel

    private lateinit var toolBar: Toolbar
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var newsBodyWv: WebView

    private lateinit var likeDislike: RelativeLayout
    private lateinit var btnLike: ImageButton
    private lateinit var textCountLike: TextView
    private lateinit var btnDislike: ImageButton
    private lateinit var textCountDislike: TextView
    private lateinit var accountIb: ImageButton
    private lateinit var tvUserName: TextView
    private lateinit var saveBtn: LinearLayout
    private lateinit var saveIV: ImageView
    private lateinit var saveTV: TextView
    private lateinit var tvTotalTranslate: TextView
    private lateinit var rv: RecyclerView
    private lateinit var placeHolder: RelativeLayout
    private lateinit var htmlHelper: HtmlHelper
    private lateinit var itemID: String
    private lateinit var menu: Menu
    private lateinit var androidID: String
    private var localName: String = ""
    private var value = 0
    private var countLike = 0
    private var countDisLike = 0
    private var isSort = true
    private var isCreat = true
    private lateinit var datum: Datum
    private var currentLanguage = "en"

    private var datasListDatum = mutableListOf<Datum>()
    private var datasListRv = arrayListOf<Datum>()
    private lateinit var adapterItemTranslate: RccItemTranslateAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_translate)
        model = TranslateViewModel(onPostDoneCalback)
        initView()
        setupWebView()
        initLikeEvent()

        model.getDataOnce(itemID, currentLanguage, androidID)
        model.datas.observe(this@TranslateActivity, {
            this.datasListDatum = it
            refreshWebView(it)
        })
    }

    @SuppressLint("HardwareIds")
    private fun initView() {
        val bundle = intent.extras
        itemID = bundle!!.getString("id")!!
        val title = bundle.getString("title")
        val noiDung = bundle.getString("noiDung").toString()
        val regex = "[.]"

        toolBar = findViewById(R.id.tool_bar)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        newsBodyWv = findViewById(R.id.news_body_wv)
        likeDislike = findViewById(R.id.like_dislike)
        btnLike = findViewById(R.id.btn_like)
        textCountLike = findViewById(R.id.text_count_like)
        btnDislike = findViewById(R.id.btn_dislike)
        textCountDislike = findViewById(R.id.text_count_dislike)
        accountIb = findViewById(R.id.account_ib)
        tvUserName = findViewById(R.id.tv_user_name)
        saveBtn = findViewById(R.id.save_btn)
        saveIV = findViewById(R.id.save_IV)
        saveTV = findViewById(R.id.save_TV)
        tvTotalTranslate = findViewById(R.id.tv_total_translate)
        rv = findViewById(R.id.rv)
        placeHolder = findViewById(R.id.place_holder)
        htmlHelper = HtmlHelper(this)
        swipeRefresh.isRefreshing = false
        androidID = Settings.Secure.getString(this.contentResolver, Settings.Secure.ANDROID_ID)

        setSupportActionBar(toolBar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        localName = preferenceHelper.getLocalName()!!

        ArrayList(
            listOf(
                *Utils.getCorrectForSplit(noiDung).split(regex.toRegex()).toTypedArray()
            )
        ).also {
            originTextList = it
        }
        originTextList.add(0, title!!)

        if (preferenceHelper.getCurrentLanguageCode() != null) {
            currentLanguage = preferenceHelper.getCurrentLanguageCode()!!
        }
        swipeRefresh.setOnRefreshListener {
            model.getDataOnce(itemID, currentLanguage, androidID)
            swipeRefresh.isRefreshing = false
        }

    }

    private fun initLikeEvent() {
        btnLike.setOnClickListener {
            scaleAnimation(btnLike)
            when (value) {
                1 -> {
                    countLike -= 1
                    textCountLike.text = countLike.toString()
                    btnLike.setColorFilter(resources.getColor(R.color.colorButtonGray))
                    value = -1
                    val voteHelpter = VoteHelper(this@TranslateActivity, object : BooleanCallback {
                        override fun execute(isSuccess: Boolean) {
                        }
                    })
                    voteHelpter.execute(2.toString(), datum.newsID, datum.uuid)
                }
                -1 -> {
                    countLike += 1
                    textCountLike.text = countLike.toString()
                    btnLike.setColorFilter(BLUE)
                    value = 1
                    val voteHelpter = VoteHelper(this@TranslateActivity, object : BooleanCallback {
                        override fun execute(isSuccess: Boolean) {
                        }
                    })
                    voteHelpter.execute(1.toString(), datum.newsID, datum.uuid)
                }
                2 -> {
                    val voteHelpter = VoteHelper(this@TranslateActivity, object : BooleanCallback {
                        override fun execute(isSuccess: Boolean) {
                            countDisLike -= 1
                            textCountDislike.text = countDisLike.toString()
                            btnDislike.setColorFilter(resources.getColor(R.color.colorButtonGray))
                            countLike += 1
                            textCountLike.text = countLike.toString()
                            btnLike.setColorFilter(BLUE)
                            value = 1
                            if (isSuccess) {
                                val vote =
                                    VoteHelper(this@TranslateActivity, object : BooleanCallback {
                                        override fun execute(isSuccess: Boolean) {
                                        }
                                    })
                                vote.execute(1.toString(), datum.newsID, datum.uuid)
                            }
                        }
                    })
                    voteHelpter.execute((-2).toString(), datum.newsID, datum.uuid)
                }
            }
        }

        btnDislike.setOnClickListener {
            scaleAnimation(btnDislike)
            when (value) {
                1 -> {
                    countLike -= 1
                    textCountLike.text = countLike.toString()
                    btnLike.setColorFilter(resources.getColor(R.color.colorButtonGray))

                    countDisLike += 1
                    textCountDislike.text = countDisLike.toString()
                    btnDislike.setColorFilter(RED)
                    value = 2

                    val voteHelpter = VoteHelper(this@TranslateActivity, object : BooleanCallback {
                        override fun execute(isSuccess: Boolean) {
                            val vote = VoteHelper(this@TranslateActivity, object : BooleanCallback {
                                override fun execute(isSuccess: Boolean) {
                                }
                            })
                            vote.execute((-1).toString(), datum.newsID, datum.uuid)
                        }
                    })
                    voteHelpter.execute(2.toString(), datum.newsID, datum.uuid)
                }
                -1 -> {
                    countDisLike += 1
                    textCountDislike.text = countDisLike.toString()
                    btnDislike.setColorFilter(RED)
                    value = 2

                    val vote = VoteHelper(this@TranslateActivity, object : BooleanCallback {
                        override fun execute(isSuccess: Boolean) {
                        }
                    })
                    vote.execute((-1).toString(), datum.newsID, datum.uuid)
                }
                2 -> {
                    countDisLike -= 1
                    textCountDislike.text = countDisLike.toString()
                    btnDislike.setColorFilter(resources.getColor(R.color.colorButtonGray))
                    value = -2
                    val vote = VoteHelper(this@TranslateActivity, object : BooleanCallback {
                        override fun execute(isSuccess: Boolean) {
                        }
                    })
                    vote.execute((-2).toString(), datum.newsID, datum.uuid)
                }
                -2 -> {
                    countDisLike += 1
                    textCountDislike.text = countDisLike.toString()
                    btnDislike.setColorFilter(RED)
                    value = 2
                    val vote = VoteHelper(this@TranslateActivity, object : BooleanCallback {
                        override fun execute(isSuccess: Boolean) {
                        }
                    })
                    vote.execute((-1).toString(), datum.newsID, datum.uuid)
                }
                0 -> {
                    countDisLike += 1
                    textCountDislike.text = countDisLike.toString()
                    btnDislike.setColorFilter(RED)
                    value = 2
                    val vote = VoteHelper(this@TranslateActivity, object : BooleanCallback {
                        override fun execute(isSuccess: Boolean) {
                        }
                    })
                    vote.execute((-1).toString(), datum.newsID, datum.uuid)
                }
                else -> {
                    return@setOnClickListener
                }
            }
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun setupWebView() {
        val wvSetting = newsBodyWv.settings
        newsBodyWv.run {
            wvSetting.javaScriptEnabled = true
            setBackgroundColor(TRANSPARENT)
            addJavascriptInterface(
                JavaScriptInterfaceTranslate(
                    onHighlightClicked,
                    textareaCallback
                ), "JSInterface"
            )
        }
    }

    @SuppressLint("SetTextI18n")
    private fun refreshWebView(datas: MutableList<Datum>) {
        swipeRefresh.isRefreshing = false
        placeHolder.visibility = View.GONE
        if (datas.isEmpty()) {
            likeDislike.visibility = View.GONE
            tvTotalTranslate.text = resources.getString(R.string.no_translate)
            saveBtn.visibility = View.VISIBLE
            saveIV.visibility = View.VISIBLE
            saveTV.text = getString(R.string.save_test)
            saveTV.visibility = View.VISIBLE
            val data = htmlHelper.convertDict2Html(this, null, originTextList, false)
            val settings = getSharedPreferences("stateSwich", 0)
            val isCheck = settings.getBoolean("swich", false)
            val colorText: String
            colorText = if (isCheck) {
                "#FFFFFF"
            } else {
                "#000000"
            }
            val dataa = data.replace("blackaa", colorText).replace("\"}", "")
            newsBodyWv.loadDataWithBaseURL(null, dataa, "text/html", "utf-8", null)
            saveBtn.setOnClickListener {
                newsBodyWv.loadUrl("javascript:checkInput()")
            }
        } else {
            if (isSort) {
                datum = datas[0]
                if (datum.status === 1) {
                    btnLike.setColorFilter(BLUE)
                } else if (datum.status === 2) {
                    btnDislike.setColorFilter(RED)
                }

                if (this.androidID == datum.uuid) {
                    menu.getItem(0).setIcon(R.drawable.ic_fix)
                    isCreat = false
                }
                tvUserName.text = datum.username
                textCountLike.text = datum.newsLike.toString()
                textCountDislike.text = datum.newsDislike.toString()
                tvTotalTranslate.text = resources.getString(R.string.another_translate)

                val data = htmlHelper.convertDict2Html(this, datum, originTextList, false)
                val settings = getSharedPreferences("stateSwich", 0)
                val isCheck = settings.getBoolean("swich", false)
                val colorText: String
                colorText = if (isCheck) {
                    "#FFFFFF"
                } else {
                    "#000000"
                }
                val dataa = data.replace("blackaa", colorText).replace("\"}", "")
                newsBodyWv.loadDataWithBaseURL(null, dataa, "text/html", "utf-8", null)

                datasListRv = arrayListOf()
                val trash = mutableListOf<Datum>()
                this.datasListRv.addAll(datas)
                for (i in datasListRv.indices) {
                    if (datum.uuid == datasListRv[i].uuid) {
                        trash.add(datasListRv[i])
                    }
                }
                if (data in trash) datasListRv.remove(data)
                adapterItemTranslate =
                    RccItemTranslateAdapter(this, datasListRv, object : ItemClickCallback {
                        override fun onClick(datum: Datum) {
                            this@TranslateActivity.datum = datum
                            datasListRv.clear()
                            datasListRv.addAll(datas)
                            val trash = mutableListOf<Datum>()
                            for (tempDatum in datasListRv) {
                                if (tempDatum.uuid == this@TranslateActivity.datum.uuid) {
                                    trash.add(tempDatum)
                                }
                            }
                            for (tempDatum in trash) datasListRv.remove(tempDatum)

                            notifiItem()
                            val data1 =
                                htmlHelper.convertDict2Html(
                                    this@TranslateActivity,
                                    this@TranslateActivity.datum,
                                    originTextList,
                                    false
                                )
                            val settings1 = getSharedPreferences("stateSwich", 0)
                            val isCheck1 = settings1.getBoolean("swich", false)
                            val colorText1: String
                            colorText1 = if (isCheck1) {
                                "#FFFFFF"
                            } else {
                                "#000000"
                            }
                            val dataa1 =
                                data1.replace("blackaa", colorText1).replace("\"}", "")
                            newsBodyWv.loadDataWithBaseURL(
                                null,
                                dataa1,
                                "text/html",
                                "utf-8",
                                null
                            )
                            countLike = this@TranslateActivity.datum.newsLike
                            countDisLike = this@TranslateActivity.datum.newsDislike
                            tvUserName.text = this@TranslateActivity.datum.username
                            textCountLike.text = countLike.toString()
                            textCountDislike.text = countDisLike.toString()
                            value = this@TranslateActivity.datum.status
                            btnLike.setColorFilter(resources.getColor(R.color.colorButtonGray))
                            btnDislike.setColorFilter(resources.getColor(R.color.colorButtonGray))
                            if (this@TranslateActivity.datum.status == 1) {
                                btnLike.setColorFilter(BLUE)
                            } else if (this@TranslateActivity.datum.status === 2) {
                                btnDislike.setColorFilter(RED)
                            }
                            isCreat = if (androidID == this@TranslateActivity.datum.uuid) {
                                menu.getItem(0).setIcon(R.drawable.ic_fix)
                                false
                            } else {
                                menu.getItem(0).setIcon(R.drawable.ic_add)
                                true
                            }
                        }

                    })

                val layoutManager: RecyclerView.LayoutManager
                rv.setHasFixedSize(true)
                layoutManager = LinearLayoutManager(this)
                rv.layoutManager = layoutManager

                rv.adapter = adapterItemTranslate
                value = datum.status
                countLike = datum.newsLike
                countDisLike = datum.newsDislike
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
            }
            R.id.itTune -> {
                val bottomSheetSetting = BSSettingContainer(false)
                bottomSheetSetting.show(
                    supportFragmentManager,
                    bottomSheetSetting.tag
                )
            }
            R.id.itAddOrFix -> {
                likeDislike.visibility = View.GONE
                tvTotalTranslate.text = getString(R.string.another_translate)
                saveBtn.visibility = View.VISIBLE
                saveIV.visibility = View.VISIBLE
                saveTV.text = getString(R.string.save_test)
                saveTV.visibility = View.VISIBLE
                val settings = getSharedPreferences("stateSwich", 0)
                val isCheck = settings.getBoolean("swich", false)
                val colorText: String
                colorText = if (isCheck) {
                    "#FFFFFF"
                } else {
                    "#000000"
                }
                val data: String = if (isCreat) {
                    htmlHelper.convertDict2Html(this, null, originTextList, false)
                } else {
                    htmlHelper.convertDict2Html(this, datum, originTextList, true)
                }
                val dataa =
                    data.replace("blackaa", colorText).replace("\"}", "")
                newsBodyWv.loadDataWithBaseURL(
                    null,
                    dataa,
                    "text/html",
                    "utf-8",
                    null
                )
                saveBtn.setOnClickListener {
                    newsBodyWv.loadUrl("javascript:checkInput()")
                }
            }
        }
        return true
    }

    private
    val mDURATION: Long = 300

    private
    val mSCALE = 0.9f

    private fun scaleAnimation(view: View?) {
        ViewCompat.animate(view!!)
            .setDuration(mDURATION)
            .scaleX(mSCALE)
            .scaleY(mSCALE)
            .rotationX(mSCALE)
            .rotationXBy(mSCALE)
            .rotation(mSCALE)
            .setListener(object : ViewPropertyAnimatorListener {
                override fun onAnimationStart(view: View) {
                    view.isClickable = false
                }

                override fun onAnimationEnd(view: View) {
                    view.isClickable = true
                }

                override fun onAnimationCancel(view: View) {}
            })
            .start()
    }

    private val textareaCallback: TextareaCallback = object : TextareaCallback {
        override fun checkInput(json: String) {
            val hashMap: HashMap<*, *> = StringHelper.getContentTranslate(json)
            if (hashMap.size < originTextList.size) Toast.makeText(
                this@TranslateActivity,
                String.format(
                    "Please enter translation content for the line %d",
                    hashMap.size + 1
                ),
                Toast.LENGTH_SHORT
            ).show() else runOnUiThread {
                model.postData(PostTranslateBody(androidID, itemID, currentLanguage, localName, json))

            }
        }

        override fun onInputChange(text: String) {
        }

        override fun onFocusChange(text: String) {
        }
    }

    private val onPostDoneCalback: (String) -> Unit = {
        model.getDataOnce(itemID, currentLanguage, androidID)
        saveBtn.visibility = View.GONE
        likeDislike.visibility = View.VISIBLE
    }

    private fun notifiItem() {
        adapterItemTranslate.notifyDataSetChanged()
    }

    private
    val onHighlightClicked: StringCallback = object : StringCallback {
        override fun execute(str: String) {
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        this.menu = menu!!
        menuInflater.inflate(R.menu.translate_menu, menu)
        return true
    }
}
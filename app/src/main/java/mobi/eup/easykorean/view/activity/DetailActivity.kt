@file:Suppress("DEPRECATION")

package mobi.eup.easykorean.view.activity

import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.annotation.SuppressLint
import android.app.DownloadManager
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Color
import android.graphics.Typeface
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.text.Html
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.*
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_audio.*
import kotlinx.coroutines.*
import mobi.eup.easykorean.*
import mobi.eup.easykorean.data.model.ItemAudio
import mobi.eup.easykorean.data.model.ItemFeed
import mobi.eup.easykorean.data.model.ItemWordHistory
import mobi.eup.easykorean.data.model.WordJSONObject
import mobi.eup.easykorean.databinding.ActivityDetailBinding
import mobi.eup.easykorean.databinding.LayoutAudioBinding
import mobi.eup.easykorean.databinding.LayoutQuickSearchBinding
import mobi.eup.easykorean.databinding.PlaceHolderBinding
import mobi.eup.easykorean.event.StringCallback
import mobi.eup.easykorean.event.TextSelectCallback
import mobi.eup.easykorean.event.VoidCallback
import mobi.eup.easykorean.event.WordCallback
import mobi.eup.easykorean.utils.app.GlobalHelper
import mobi.eup.easykorean.utils.app.NetworkHelper
import mobi.eup.easykorean.utils.app.Utils
import mobi.eup.easykorean.utils.enum.DictQuickSearchType
import mobi.eup.easykorean.utils.js_interface.JavaScriptInterface
import mobi.eup.easykorean.utils.translate.KindMapHelper
import mobi.eup.easykorean.utils.translate.SearchHelper
import mobi.eup.easykorean.utils.view.AlertHelper
import mobi.eup.easykorean.utils.view.UIHelper
import mobi.eup.easykorean.view.adapter.RccNewsAdapter
import mobi.eup.easykorean.view.base.BaseActivity
import mobi.eup.easykorean.view.bottomsheet.BSHistoryContainer
import mobi.eup.easykorean.view.bottomsheet.BSSettingContainer
import mobi.eup.easykorean.viewmodel.DetailViewModel
import java.io.BufferedReader
import java.io.File
import java.io.IOException
import java.io.InputStreamReader
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt

@Suppress("DEPRECATION")
class DetailActivity : BaseActivity(),
    ClipboardManager.OnPrimaryClipChangedListener {
    private lateinit var binding: ActivityDetailBinding
    private lateinit var bindingSV: LayoutQuickSearchBinding        // Search view binding
    private lateinit var bindingAV: LayoutAudioBinding              // Audio view binding
    private lateinit var bindingPlaceHolder: PlaceHolderBinding     // Placeholder binding

    private var itemID: String = ""
    private var itemType: String = ""
    private var itemData: ItemFeed? = null
    private lateinit var model: DetailViewModel
    private var isFavorite = 0
    private var bodyContent = ""
    private var isNight = false
    private var speakerID = 14 // default
    private val mpNews = MediaPlayer()
    private val mpWord = MediaPlayer()
    private val coroutineScope = MainScope()

    private lateinit var itemFavorite: MenuItem
    private lateinit var itemWordsReview: MenuItem
    private lateinit var itemListen: MenuItem
    private lateinit var itemTranslate: MenuItem
    private lateinit var badgeWordsTv: TextView
    private lateinit var badgeListenTv: TextView
    private lateinit var badgeTranslateTv: TextView
    private var numWords = 0
    private var numHeadphone = 0
    private var numTranslate = 0

    private var isMediaLoop = false
    private lateinit var mainHandler: Handler
    private var linkMp3News = ""
    private var isFirstCreate = true
    private var isPrepared = false

    private lateinit var webView: WebView
    private var pasteData: String = ""

    private lateinit var btnCurrentLang: Button
    private lateinit var layoutQuickSearch: RelativeLayout
    private lateinit var btnKoEn: Button
    private lateinit var btnKoKo: Button
    private lateinit var tvWord: TextView
    private lateinit var tvMean: TextView
    private var textSelection = ""
    private var searchHelper: SearchHelper<WordJSONObject>? = null
    private var currentQuickSearchType: DictQuickSearchType = DictQuickSearchType.CURRENT

    private val colorTextDefault = R.color.colorTextDefault
    private val colorTextDefaultNight = R.color.colorTextDefaultNight
    private val colorAccent = R.color.colorAccent
    private val colorAccentNight = R.color.colorAccentNight

    companion object {
        const val SEND_ID_TO_WORD_ACTIVITY = "send_id_to_word_actvity"
        const val SEND_CONTENT_TO_LISTEN_ACTIVITY = "send_content_to_listen_actvity"
        const val SEND_LINK_IMG_TO_LISTEN_ACTIVITY = "send_link_img_to_listen_actvity"
        const val RequestPermissionCode = 1
    }

    private var msg: String? = ""
    private var lastMsg = ""
    private lateinit var itemAudio: ItemAudio
    private var androidID = ""

    @SuppressLint("NewApi", "HardwareIds")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getItemIDFromIntent()
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail)
        bindingSV = binding.includedViewSearch
        bindingAV = binding.includedViewAudio
        bindingPlaceHolder = binding.includedPlaceholder

        isNight = preferenceHelper.isNightMode()
        speakerID = preferenceHelper.getSpeakerID()
        androidID = Settings.Secure.getString(this.contentResolver, Settings.Secure.ANDROID_ID)
        model = ViewModelProvider(
            this@DetailActivity,
            DetailViewModel.ViewModelFactory(this@DetailActivity)
        ).get(DetailViewModel::class.java)

        coroutineScope.launch {
            val isExist: Boolean = model.checkExits(itemID).await()
            if (isExist) {
                model.getItemByDatabase(itemID)
            } else {
                model.getItemBySever(itemID)
            }
        }

        model.itemData.observe(this, {
            if (it != null) {
                reloadData(it)
                showHidePlaceholder(false)
            } else {
                showHidePlaceholder(true)
            }
        })

        itemType = if (itemData != null) itemData!!.type else "easy"
        bodyContent =
            if (itemData != null) Utils.getCorrectForHtml(itemData!!.content.atContent.trim()) else GlobalHelper.DEFAULT_TEXT_BODY_EMPTY

        binding.viewData = model
        binding.lifecycleOwner = this

        initViews()
        mainHandler = Handler(this@DetailActivity.mainLooper)
    }

    private fun getItemIDFromIntent() {
        if (intent != null) {
            itemID = intent.getStringExtra(GlobalHelper.ITEM_ID_EXTRA)!!
        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun showLoadingPlaceHolder(){
        bindingPlaceHolder.placeHolderTv.text = resources.getString(R.string.loading)
        bindingPlaceHolder.placeHolderIv.setImageDrawable(resources.getDrawable(R.drawable.antenna))
    }

    private fun showHidePlaceholder(isShow: Boolean) {
        bindingPlaceHolder.placeHolder.visibility = if (isShow) View.VISIBLE else View.GONE
    }

    private fun reloadData(item: ItemFeed) {
        itemData = item
        itemType = if (itemData != null) itemData!!.type else "easy"
        bodyContent =
            if (itemData != null) Utils.getCorrectForHtml(itemData!!.content.atContent.trim()) else GlobalHelper.DEFAULT_TEXT_BODY_EMPTY
        invalidateOptionsMenu()
        refreshWebView()
        settupBottomAudioBar()
        reloadTranslateItemTextview()
        model.getListSuggest(if (itemType == "") "easy" else itemType)
    }

    private fun reloadTranslateItemTextview() {
        if (itemData == null) return
        model.getDataTranslate(itemID, preferenceHelper.getCurrentLanguageCode(), androidID)
        model.listTranslate.observe(this, {
            numTranslate = it.size
            invalidateOptionsMenu()
        })
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initViews() {
        setSupportActionBar(binding.toolBar)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayShowTitleEnabled(false)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
        binding.toolBar.setNavigationOnClickListener {
            onBackPressed()
        }

        setupWebView()
        setupRecyclerView()
        setupQuickSearch()
        setupTheme()
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun setupWebView() {
        webView = binding.webview
        webView.webViewClient = WebViewClient()
        val wvSetting = webView.settings
        webView.run {
            wvSetting.javaScriptEnabled = true
            setBackgroundColor(Color.TRANSPARENT)
            addJavascriptInterface(
                JavaScriptInterface(
                    onHighlightClicked,
                    onSelectText
                ), "JSInterface"
            )
        }
        webView.setPadding(0, 0, 0, 0)
    }

    private fun refreshWebView() {
        webView.loadDataWithBaseURL(null, readFile(), "text/html", "UTF-8", null)
    }

    private fun setupRecyclerView() {
        binding.rccSuggest.apply {
            layoutManager = LinearLayoutManager(this@DetailActivity)
            adapter =
                RccNewsAdapter(
                    if (model.listData.value == null) mutableListOf() else model.listData.value!!,
                    this@DetailActivity,
                    onItemRccClick
                )
        }
    }

    private val onItemRccClick: (ItemFeed) -> Unit = {
        model.taskInsertItem(it)
        model.taskUpdateSeen(it.id, 1)
    }

    @SuppressLint("SimpleDateFormat")
    private fun settupBottomAudioBar() {
        bindingAV.seekBar.isEnabled = false
        bindingAV.playPauseBtn.visibility = View.GONE
        bindingAV.progressBarPlay.visibility = View.VISIBLE

        if (itemData != null) {
            ItemAudio(
                itemData!!.title,
                itemID,
                if (itemData!!.content.images.isNotEmpty())
                    itemData!!.content.images[0] else ""
            ).also { itemAudio = it }
        } else itemAudio =
            ItemAudio("Cant be load title", itemID, GlobalHelper.DEFAULT_APP_IMG_LINK)

        if (!model.asyncTaskAudioCheckExist(itemID)) {
            getFileAudio().delete()
        }

        initAudioData()

        model.newsLinkMp3.observe(this@DetailActivity, {
            linkMp3News = it
            setAudioData()
        })

        bindingAV.downloadAudioBtn.setImageResource(
            if (getFileAudio().exists()) R.drawable.ic_delete_file else
                R.drawable.ic_download
        )

        bindingAV.playPauseBtn.setOnClickListener {
            if (!mpNews.isPlaying) {
                mpNews.start()
                bindingAV.playPauseBtn.setImageResource(R.drawable.ic_pause_filled)
            } else {
                mpNews.pause()
                bindingAV.playPauseBtn.setImageResource(R.drawable.ic_play_filled)
            }
        }
        bindingAV.skipNextBtn.setOnClickListener {
            if (mpNews.duration - mpNews.currentPosition >= 3000) {
                mpNews.seekTo(mpNews.currentPosition + 3000)
                bindingAV.seekBar.progress = mpNews.currentPosition + 3000
            } else return@setOnClickListener
        }

        bindingAV.skipBackBtn.setOnClickListener {
            if (mpNews.currentPosition >= 3000) {
                mpNews.seekTo(mpNews.currentPosition - 3000)
                bindingAV.seekBar.progress = mpNews.currentPosition - 3000
            } else return@setOnClickListener
        }

        bindingAV.replayAudioBtn.setImageResource(
            if (preferenceHelper.isReplayAudio()) R.drawable.ic_replay_checked
            else R.drawable.ic_replay_unchecked
        )
        bindingAV.replayAudioBtn.setOnClickListener {
            this.isMediaLoop = !isMediaLoop
            bindingAV.replayAudioBtn.setImageResource(if (isMediaLoop) R.drawable.ic_replay_checked else R.drawable.ic_replay_unchecked)
            preferenceHelper.setReplayAudio(isMediaLoop)
            mpNews.isLooping = isMediaLoop
        }

        bindingAV.downloadAudioBtn.setOnClickListener {
            if (preferenceHelper.isPremiumUser()) {
                if (checkPermission()) {
                    checkDownloadDeleteAudio()
                } else {
                    requestPermission()
                }
                return@setOnClickListener
            } else {
                AlertHelper.showTipAlert(
                    this@DetailActivity,
                    R.drawable.img_premium,
                    resources.getString(R.string.oops),
                    resources.getString(R.string.audio_manager_premium_only),
                    null
                )
            }

        }
        bindingAV.seekBar.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                if (!fromUser) return
                mpNews.seekTo(progress)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })
    }

    private fun initAudioData() {
        if (getFileAudio().exists()) {
            setAudioData()
        }

        if (linkMp3News != "") return
        if (itemData != null) {
            model.getNewsLinkMp3(itemData!!.content.atContent, speakerID)
        } else {
            model.getNewsLinkMp3(
                GlobalHelper.DEFAULT_TEXT_BODY_EMPTY,
                speakerID
            )
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun setAudioData() {
        mpNews.reset()
        if (getFileAudio().exists()) {
            mpNews.setDataSource(this@DetailActivity, Uri.parse(getFileAudio().path))
        } else {
            if (linkMp3News == "") return
            mpNews.setDataSource(linkMp3News)
        }
        mpNews.prepareAsync()
        mpNews.setAudioStreamType(AudioManager.STREAM_MUSIC)
        mpNews.isLooping = preferenceHelper.isReplayAudio()
        mpNews.setOnCompletionListener { resetAudioMP() }

        mpNews.setOnPreparedListener {
            bindingAV.playPauseBtn.setImageResource(R.drawable.ic_play_filled)
            val format = SimpleDateFormat("mm:ss")
            bindingAV.totalTimeTv.text = (format.format(mpNews.duration))
            bindingAV.seekBar.max = mpNews.duration
            bindingAV.seekBar.isEnabled = true
            bindingAV.playPauseBtn.visibility = View.VISIBLE
            bindingAV.progressBarPlay.visibility = View.GONE
            isPrepared = true
        }
    }

    @SuppressLint("SetTextI18n")
    private fun resetAudioMP() {
        bindingAV.seekBar.progress = 0
        bindingAV.currentTimeTv.text = "00:00"
        if (!preferenceHelper.isReplayAudio()) {
            bindingAV.playPauseBtn.setImageResource(R.drawable.ic_play_filled)
        }
    }

    private val updateTextTask = object : Runnable {
        override fun run() {
            getCurrentPos()
            mainHandler.postDelayed(this, 1000)
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun getCurrentPos() {
        if (!mpNews.isPlaying || !isPrepared) {
            return
        }

        val format = SimpleDateFormat("mm:ss")
        bindingAV.currentTimeTv.text = (format.format(mpNews.currentPosition))
        bindingAV.seekBar.progress = mpNews.currentPosition
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.detail_activity_toolbar_menu, menu)
        if (itemData != null) {
            isFavorite = itemData!!.isFavorite
            numWords = itemData!!.word.size
            numHeadphone = itemData!!.content.atContent.split(".").size
        } else {
            isFavorite = 0
            numWords = 0
            numHeadphone = 0
        }

        itemFavorite = menu.findItem(R.id.item_favorite)
        itemFavorite.setIcon(if (isFavorite == 1) R.drawable.ic_favorite_filled else R.drawable.ic_favorite)

        itemWordsReview = menu.findItem(R.id.item_words_review)
        val wordsActionView = itemWordsReview.actionView
        (wordsActionView.findViewById<View>(R.id.ic_badge) as ImageView).setImageResource(R.drawable.ic_words)

        badgeWordsTv =
            wordsActionView.findViewById(R.id.notifcation_item_menu_textview)
        if (numWords > 0) {
            badgeWordsTv.text = numWords.toString()
        }
        badgeWordsTv.visibility = if (numWords > 0) View.VISIBLE else View.GONE

        wordsActionView.setOnClickListener {
            onOptionsItemSelected(itemWordsReview)
        }


        itemListen = menu.findItem(R.id.item_listen)
        itemListen.setActionView(R.layout.badge_menu_item)
        val listenActionView = itemListen.actionView
        (listenActionView.findViewById<View>(R.id.ic_badge) as ImageView).setImageResource(R.drawable.ic_headphones)
        badgeListenTv =
            listenActionView.findViewById(R.id.notifcation_item_menu_textview)


        if (numHeadphone > 0) {
            badgeListenTv.text = numHeadphone.toString()
        }
        badgeListenTv.visibility = if (numHeadphone > 0) View.VISIBLE else View.GONE
        listenActionView.setOnClickListener {
            onOptionsItemSelected(itemListen)
        }

        itemTranslate = menu.findItem(R.id.item_translate)
        itemTranslate.setActionView(R.layout.badge_menu_item)
        val translateActionView = itemTranslate.actionView
        (translateActionView.findViewById<View>(R.id.ic_badge) as ImageView).setImageResource(R.drawable.ic_translate)
        badgeTranslateTv = translateActionView.findViewById(R.id.notifcation_item_menu_textview)
        if (numTranslate > 0) {
            badgeTranslateTv.text = numTranslate.toString()
        }
        badgeTranslateTv.visibility = if (numTranslate > 0) View.VISIBLE else View.GONE
        translateActionView.setOnClickListener {
            onOptionsItemSelected(itemTranslate)
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.item_favorite -> {
                if (itemData == null) {
                    notifyUser(this, "Content data empty, try again !")
                } else {
                    isFavorite = revertIsFavorite(isFavorite)
                    itemFavorite.setIcon(if (isFavorite == 1) R.drawable.ic_favorite_filled else R.drawable.ic_favorite)
                    model.taskFeedUpdateFavorite(itemID, isFavorite)
                    showToastFavoriteAction(isFavorite == 1)
                }
                true
            }
            R.id.item_words_review -> {
                if (itemData == null) {
                    notifyUser(this, "Content data empty, try again !")
                } else {
                    val intent = Intent(this@DetailActivity, WordsActivity::class.java)
                    val bundle = Bundle()
                    val words = itemData!!.word
                    bundle.putString(SEND_ID_TO_WORD_ACTIVITY, Gson().toJson(words))
                    intent.putExtras(bundle)
                    startActivity(intent)
                }
                true
            }
            R.id.item_settings -> {
                val bottomSheetSettingAll = BSSettingContainer(false)
                bottomSheetSettingAll.show(supportFragmentManager, bottomSheetSettingAll.tag)
                true
            }
            R.id.item_history -> {
                val bottomSheetHistory = BSHistoryContainer()
                bottomSheetHistory.show(supportFragmentManager, bottomSheetHistory.tag)
                true
            }
            R.id.item_listen -> {
                if (itemData == null) {
                    notifyUser(this, "Content data empty, try again !")
                } else {
                    val intent = Intent(this@DetailActivity, ListenAndRepeatActivity::class.java)
                    val bundle = Bundle()
                    bundle.putString(
                        SEND_CONTENT_TO_LISTEN_ACTIVITY,
                        itemData!!.content.atContent.trim()
                    )
                    bundle.putString(
                        SEND_LINK_IMG_TO_LISTEN_ACTIVITY,
                        if (itemData!!.content.images.isNotEmpty()) itemData!!.content.images[0] else GlobalHelper.DEFAULT_APP_IMG_LINK
                    )
                    intent.putExtras(bundle)
                    startActivity(intent)
                }
                true
            }

            R.id.item_translate -> {
                if (itemData == null) {
                    notifyUser(this, "Content data empty, try again !")
                } else {
                    val intent = Intent(this@DetailActivity, TranslateActivity::class.java)
                    val bundle = Bundle()
                    bundle.putString("id", itemID)
                    if (itemData != null) {
                        bundle.putString("title", itemData!!.title)
                        bundle.putString("noiDung", itemData!!.content.atContent)
                    } else {
                        bundle.putString("title", "")
                        bundle.putString("noiDung", "")
                    }
                    intent.putExtras(bundle)
                    startActivity(intent)
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun showToastFavoriteAction(isFavorite: Boolean) {
        Toast.makeText(
            this@DetailActivity,
            if (isFavorite) {
                resources.getString(R.string.add_to_favorite)
            } else {
                resources.getString(R.string.remove_word_from_favorite)
            },
            Toast.LENGTH_SHORT
        ).show()
    }

    override fun onPrimaryClipChanged() {
    }

    private fun setupTheme() {

        bindingAV.bottomLl.setBackgroundResource(if (isNight) R.color.colorPrimaryNight else R.color.colorPrimary)
        binding.rccSuggest.setBackgroundResource(if (isNight) R.color.colorBackgroundDark else R.color.colorBackgroundLight)

        // quick search
        val top = bindingSV.layoutContent
        val menu = bindingSV.menuAction
        val menuDict = bindingSV.menuDict

        top.setBackgroundResource(if (isNight) R.drawable.bg_round_rect else R.drawable.bg_round_rect_dark)
        menu.setBackgroundResource(if (isNight) R.drawable.bg_round_rect else R.drawable.bg_round_rect_dark)
        menuDict.setBackgroundResource(if (isNight) R.drawable.bg_round_rect else R.drawable.bg_round_rect_dark)

        bindingSV.btnSpeak.setColorFilter(if (isNight) colorTextDefault else colorTextDefaultNight)
        bindingSV.btnFavorite.setColorFilter(if (isNight) colorTextDefault else colorTextDefaultNight)
        bindingSV.wordTv.setTextColor(resources.getColor(if (isNight) colorTextDefault else colorTextDefaultNight))
        btnCurrentLang = layoutQuickSearch.findViewById(R.id.btn_current)

        val buttons = arrayOf(
            bindingSV.searchBtn,
            bindingSV.btnCopy,
            bindingSV.closeBtn,
            btnCurrentLang,
            bindingSV.btnKoEn,
            bindingSV.btnKoKo
        )
        UIHelper.buttonSetTextColor(
            buttons,
            isNight,
            resources.getColor(colorTextDefault),
            resources.getColor(colorTextDefaultNight)
        )
    }

    private fun readFile(): String {
        val fontSize = preferenceHelper.getFontSize()
        val size = if (fontSize == 0) {
            18
        } else {
            fontSize
        }

        val source: String =
            if (itemData != null) {
                if (itemData!!.type == "easy") {
                    getString(R.string.text_nguon) + " " + getString(R.string.text_nguon_de)
                } else {
                    getString(R.string.text_nguon) + " " + getString(R.string.text_nguon_kho)
                }
            } else getString(R.string.text_nguon) + " " + getString(R.string.text_nguon_kho)

        var contents = ""
        val reader: BufferedReader?
        if (itemData != null) {
            try {
                reader =
                    BufferedReader(InputStreamReader(assets.open("html/news_html.html"), "UTF-8"))
                for (mLine in reader.readLines()) {
                    contents += "\n" + mLine
                }
                return contents.replace("title", itemData!!.title)
                    .replace("18", size.toString())
                    .replace("ngay", itemData!!.pubDate)
                    .replace(
                        "link",
                        if (itemData!!.content.images.size > 0) itemData!!.content.images[0] else GlobalHelper.DEFAULT_APP_IMG_LINK
                    )
                    .replace("noidung", bodyContent)
                    .replace("nguon", source)
            } catch (ignored: IOException) {
                ignored.printStackTrace()
            }
        }
        return contents.replace("title", "콘텐츠 없음")
            .replace("19", size.toString())
            .replace("ngay", "2021년 1월 1일")
            .replace(
                "link", GlobalHelper.DEFAULT_APP_IMG_LINK
            )
            .replace("noidung", bodyContent)
            .replace("nguon", source)
    }

    private fun revertIsFavorite(origin: Int): Int {
        return if (origin == 1) {
            0
        } else 1
    }

    /* region PERMISSION */
    private fun checkPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(
            applicationContext,
            WRITE_EXTERNAL_STORAGE
        )

        val result1 = ContextCompat.checkSelfPermission(
            applicationContext,
            READ_EXTERNAL_STORAGE
        )
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            this@DetailActivity,
            arrayOf(WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE), RequestPermissionCode
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        if (requestCode == RequestPermissionCode) {
            if (grantResults.isNotEmpty()) {
                val writePermission = grantResults[0] == PackageManager.PERMISSION_GRANTED
                val readPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED
                if (writePermission && readPermission) {
                    checkDownloadDeleteAudio()
                } else {
                    notifyUser(this@DetailActivity, resources.getString(R.string.permissin_denied))
                }
            }
        }
    }
/* endregion */

    /* region DOWNLOAD_AUDIO */
    private fun checkDownloadDeleteAudio() {
        val fileAudio = getFileAudio()
        if (!fileAudio.exists()) {
            startDownloadAudio()
        } else {
            val title = resources.getString(R.string.delete_audio_title)
            val desc = resources.getString(R.string.delete_audio_desc)
            AlertHelper.showYesNoAlert(
                this,
                R.drawable.alert,
                title,
                desc,
                object : VoidCallback {
                    override fun execute() {
                        if (fileAudio.delete()) {
                            bindingAV.downloadAudioBtn.setImageResource(R.drawable.ic_download)
                            model.taskAudioClear(itemAudio)
                            notifyUser(
                                this@DetailActivity,
                                resources.getString(R.string.delete_audio_successfully)
                            )
                        } else {
                            notifyUser(
                                this@DetailActivity,
                                resources.getString(R.string.delete_audio_failed)
                            )
                        }
                    }
                },
                null
            )
        }
    }

    private fun getFileAudio(): File =
        File(applicationContext.getExternalFilesDir(null)!!.path + File.separator + "Downloaded Audio/" + itemID)

    private fun startDownloadAudio() {
        if (isFinishing || isDestroyed) return
        if (NetworkHelper.isNetWork(this@DetailActivity)) {

            if (linkMp3News == "") {
                notifyUser(this@DetailActivity, "Wait any second to get link and try a gain !")
                return
            }

            val targetDirectory =
                File(applicationContext.getExternalFilesDir(null)!!.path + File.separator + "Downloaded Audio")
            if (!targetDirectory.exists()) {
                targetDirectory.mkdir()
            }

            val downloadManager =
                this.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
            val downloadUri = Uri.parse(linkMp3News)

            val request = DownloadManager.Request(downloadUri).apply {
                setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI or DownloadManager.Request.NETWORK_MOBILE)
                    .setAllowedOverRoaming(false)
                    .setTitle("Easy Korean")
                    .setDescription("Downloading Audio")
                    .setDestinationInExternalFilesDir(
                        this@DetailActivity,
                        "Downloaded Audio",
                        itemID
                    )
            }

            val downloadId = downloadManager.enqueue(request)
            val query = DownloadManager.Query().setFilterById(downloadId)
            Thread(Runnable {
                var downloading = true
                while (downloading) {
                    val cursor: Cursor = downloadManager.query(query)
                    cursor.moveToFirst()
                    if (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_SUCCESSFUL) {
                        downloading = false
                    }
                    val status =
                        cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS))
                    msg = statusMessage(status)
                    if (msg != lastMsg) {
                        this.runOnUiThread {
                            notifyUser(this@DetailActivity, msg + "")
                        }
                        lastMsg = msg ?: ""
                    }
                    this.runOnUiThread {
                        if (status == DownloadManager.STATUS_SUCCESSFUL) {
                            bindingAV.downloadAudioBtn.setImageResource(R.drawable.ic_delete_file)
                            model.taskAudioInsert(itemAudio)
                        }
                    }
                    cursor.close()
                }

            }).start()
        } else {
            notifyUser(
                this@DetailActivity,
                resources.getString(R.string.download_audio_no_interner)
            )
        }
    }

    private fun statusMessage(status: Int): String {
        return when (status) {
            DownloadManager.STATUS_FAILED -> "Download has been failed, please try again"
            DownloadManager.STATUS_PAUSED -> "Paused"
            DownloadManager.STATUS_PENDING -> "Pending..."
            DownloadManager.STATUS_RUNNING -> "Downloading..."
            DownloadManager.STATUS_SUCCESSFUL -> "Downloaded successfully !"
            else -> "There's nothing to download"
        }
    }
/* endregion */

    //region QUICK_SEARCH
    private
    val onHighlightClicked: StringCallback = object : StringCallback {
        override fun execute(str: String) {

        }
    }

    private
    val onSelectText = object : TextSelectCallback {
        override fun onSelect(
            content: String,
            left: String,
            top: String,
            right: String,
            bottom: String
        ) {
            runOnUiThread {
                val converted = content.trim()
                if (converted.isEmpty()) {
                    layoutQuickSearch.visibility = View.GONE
                    return@runOnUiThread
                }
                if (textSelection == converted && layoutQuickSearch.visibility == View.VISIBLE) {
                    return@runOnUiThread
                }
                textSelection = converted
                val layoutParams =
                    layoutQuickSearch.layoutParams as RelativeLayout.LayoutParams
                var paddingTop = (bottom.toFloat() + 32).roundToInt()
                val scale = resources.displayMetrics.density
                paddingTop = (paddingTop * scale).toInt()
                layoutParams.topMargin = paddingTop
                layoutQuickSearch.layoutParams = layoutParams
                layoutQuickSearch.requestLayout()

                layoutQuickSearch.visibility = View.VISIBLE
                requestQuickSearch()
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setupCurrentLang() {
        val isEnglishOrKorean: Boolean = preferenceHelper.isEnglishOrKorean()
        if (isEnglishOrKorean) {
            changeSelectButtonSelect(DictQuickSearchType.KOEN)
        } else {
            changeSelectButtonSelect(DictQuickSearchType.CURRENT)
        }
        btnCurrentLang.text =
            preferenceHelper.getCurrentFlag() + (
                    " KO-" + preferenceHelper.getCurrentLanguageCode()
                        .toUpperCase(Locale.ROOT)
                    )
        btnCurrentLang.visibility = if (isEnglishOrKorean) View.GONE else View.VISIBLE
    }

    @SuppressLint("SetTextI18n", "SimpleDateFormat")
    private fun setupQuickSearch() {
        layoutQuickSearch = bindingSV.layoutQuickSearch
        layoutQuickSearch.visibility = View.GONE
        btnCurrentLang = bindingSV.btnCurrent
        btnKoEn = bindingSV.btnKoEn
        btnKoKo = bindingSV.btnKoKo
        tvMean = bindingSV.meanTv
        tvWord = bindingSV.wordTv
        bindingSV.progressBar.visibility = View.VISIBLE
        setupCurrentLang()

        bindingSV.closeBtn.setOnClickListener {
            layoutQuickSearch.visibility = View.GONE
        }
        bindingSV.btnCopy.setOnClickListener {
            copyToClipboard()
        }
        bindingSV.searchBtn.setOnClickListener {
            val fmt = SimpleDateFormat("yyyy-MM-dd hh:mm")
            val now = Date()
            val strDate = fmt.format(now)
            model.taskWordHistoryInsert(
                ItemWordHistory(
                    textSelection, strDate
                )
            )
            startActivity(
                Intent(
                    this@DetailActivity,
                    DictionaryActivity::class.java
                ).putExtra("query_key", textSelection)
            )
        }

        btnCurrentLang.setOnClickListener {
            changeSelectButtonSelect(DictQuickSearchType.CURRENT)
            requestQuickSearch()
        }
        btnKoKo.text = "\uD83C\uDDF0\uD83C\uDDF7" + " KO-KO" // South Korea Flag
        btnKoKo.setOnClickListener {
            changeSelectButtonSelect(DictQuickSearchType.KOKO)
            requestQuickSearch()
        }
        btnKoEn.text = "🏴󠁧󠁢󠁥󠁮󠁧󠁿" + " KO-EN"
        btnKoEn.setOnClickListener {
            changeSelectButtonSelect(DictQuickSearchType.KOEN)
            requestQuickSearch()
        }
        model.wordLinkMp3.observe(this@DetailActivity, {
            if (layoutQuickSearch.visibility == View.GONE) return@observe
            playAudioWord(it)
        })
    }

    //play audio file
    private fun playAudioWord(linkMp3: String) {
        try {
            var mpNewsIsPlaying = false
            if (mpNews.isPlaying) {
                mpNewsIsPlaying = true
                mpNews.pause()
                bindingAV.playPauseBtn.setImageResource(R.drawable.ic_play_filled)
                bindingAV.playPauseBtn.isEnabled = false
            }
            mpWord.reset()
            mpWord.setDataSource(linkMp3)
            mpWord.prepareAsync()
            mpWord.setAudioStreamType(AudioManager.STREAM_MUSIC)
            mpWord.setOnPreparedListener {
                mpWord.start()
            }
            mpWord.setOnCompletionListener {
                if (!mpNews.isPlaying) {
                    bindingAV.playPauseBtn.isEnabled = true
                    if (mpNewsIsPlaying) {
                        bindingAV.playPauseBtn.setImageResource(R.drawable.ic_pause_filled)
                        mpNews.start()
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun changeSelectButtonSelect(type: DictQuickSearchType) {
        currentQuickSearchType = type
        val defaultTextColor: Int =
            resources.getColor(if (preferenceHelper.isNightMode()) colorTextDefault else colorTextDefaultNight)
        val selectedTextColor: Int =
            resources.getColor(if (preferenceHelper.isNightMode()) colorAccent else colorAccentNight)

        btnCurrentLang.setTextColor(if (type === DictQuickSearchType.CURRENT) selectedTextColor else defaultTextColor)
        btnCurrentLang.setTypeface(
            null,
            if (type === DictQuickSearchType.CURRENT) Typeface.BOLD else Typeface.NORMAL
        )
        btnKoEn.setTextColor(if (type === DictQuickSearchType.KOEN) selectedTextColor else defaultTextColor)
        btnKoEn.setTypeface(
            null,
            if (type === DictQuickSearchType.KOEN) Typeface.BOLD else Typeface.NORMAL
        )
        btnKoKo.setTextColor(if (type === DictQuickSearchType.KOKO) selectedTextColor else defaultTextColor)
        btnKoKo.setTypeface(
            null,
            if (type === DictQuickSearchType.KOKO) Typeface.BOLD else Typeface.NORMAL
        )
    }

    @SuppressLint("SimpleDateFormat")
    private fun requestQuickSearch() {
        if (this.searchHelper != null) {
            if (!this.searchHelper!!.isCancelled) {
                searchHelper!!.cancel(true)
            }
        }

        tvWord.text = textSelection
        bindingSV.progressBar.visibility = View.VISIBLE
        tvMean.visibility = View.GONE
        val languageCode = when (currentQuickSearchType) {
            DictQuickSearchType.KOEN -> "en"
            DictQuickSearchType.KOKO -> "ko"
            else -> preferenceHelper.getCurrentLanguageCode()
        }

        searchHelper = SearchHelper(
            null, application, WordJSONObject::
            class.java
        )
        searchHelper!!.setWordCallback(onPostExecuteQS)
        searchHelper!!.executeOnExecutor(
            AsyncTask.THREAD_POOL_EXECUTOR,
            "word",
            textSelection,
            "5",
            languageCode
        )
    }

    private
    val onPostExecuteQS: WordCallback = object : WordCallback {
        override fun execute(wordObject: WordJSONObject?) {
            bindingSV.progressBar.visibility = View.GONE
            if (isFinishing || wordObject == null) {
                return
            }
            val data: List<WordJSONObject.Datum>? = wordObject.data
            if (data == null || data.isEmpty()) {
                return
            }
            bindingSV.btnSpeak.setOnClickListener {
                model.getWordLinkMp3(textSelection, preferenceHelper.getSpeakerID())
            }

            val stringBuilder = StringBuilder()
            var phonetic = ""
            for (datum in data) {
                if (datum.isMatches) {
                    if (stringBuilder.isNotEmpty()) stringBuilder.append("<br><br>")
                    stringBuilder.append(convertMeansWord(datum.means))
                    if (phonetic.isEmpty() && datum.word.trim() == tvWord.text.toString()
                            .trim { it <= ' ' }
                        && datum.phonetic != null && datum.phonetic.isNotEmpty()
                    ) phonetic = "「" + datum.phonetic.toString() + "」"
                }
            }

            val isNight = preferenceHelper.isNightMode()
            val wordResult = getString(
                R.string.word_qs_style,
                if (!isNight) "#FF9800" else "#F44336", tvWord.text.toString(),
                if (!isNight) "#F5F5F5" else "#737373", phonetic
            )
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                tvWord.text = Html.fromHtml(wordResult, Html.FROM_HTML_MODE_COMPACT)
                tvMean.text = Html.fromHtml(
                    stringBuilder.toString(),
                    Html.FROM_HTML_MODE_COMPACT
                )
            } else {
                tvWord.text = Html.fromHtml(wordResult)
                tvMean.text = Html.fromHtml(stringBuilder.toString())
            }
            tvMean.visibility = View.VISIBLE
        }
    }

    private fun convertMeansWord(means: ArrayList<WordJSONObject.Mean>?): String {
        if (means == null || means.isEmpty()) return ""
        val converted = StringBuilder()
        val isNight = preferenceHelper.isNightMode()
        var lastKind = ""
        val colorKind = if (isNight) "#A32B2E" else "#DCE775"
        val colorMean = if (isNight) "#2196F3" else "#4DD0E1"
        for (mean in means) {
            if (mean.kind != null && mean.kind.isNotEmpty() && lastKind != mean.kind) {
                val arrTypeWord: List<String> = mean.kind.split(",")
                val kind = StringBuilder()
                for (s in arrTypeWord) {
                    kind.append(
                        if (kind.isEmpty()) KindMapHelper.getKind(
                            s.trim { it <= ' ' },
                            this
                        ) else ", " + KindMapHelper.getKind(
                            s.trim { it <= ' ' },
                            this
                        )
                    )
                }
                val convertedKind =
                    if (converted.isEmpty()) "☆ $kind" else "<br><br>☆ $kind"
                converted.append(
                    String.format(
                        "<font color = '%s'>%s</font>",
                        colorKind,
                        convertedKind
                    )
                )
                lastKind = mean.kind
            }
            converted.append(
                java.lang.String.format(
                    "<font color = '%s'>%s</font>", colorMean,
                    if (converted.isEmpty()) " ◆ " + mean.mean else "<br> ◆ " + mean.mean
                )
            )
        }
        return converted.toString()
    }

    private fun copyToClipboard() {
        if (textSelection.isEmpty()) return
        val clipboard = getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("Copied text", textSelection)
        clipboard.setPrimaryClip(clip)
        Toast.makeText(this, getString(R.string.copied), Toast.LENGTH_SHORT).show()
    }

//endregion

    /* region LIFE_CYCLE */
    override fun onDestroy() {
        super.onDestroy()
        mpNews.release()
        mpWord.release()
        pasteData = ""
        if (searchHelper != null && !searchHelper!!.isCancelled) searchHelper!!.cancel(
            true
        )
        mainHandler.removeCallbacks(updateTextTask)
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    override fun onPause() {
        super.onPause()
        if (mpNews.isPlaying) {
            mpNews.pause()
        }
        bindingAV.playPauseBtn.setImageDrawable(resources.getDrawable(R.drawable.ic_play_filled))
        mpWord.release()
        isFirstCreate = false
        mainHandler.removeCallbacks(updateTextTask)
    }

    @SuppressLint("NewApi")
    override fun onResume() {
        super.onResume()
        if (!isFirstCreate) {
            if (linkMp3News == "" && !isPrepared) {
                bindingAV.playPauseBtn.visibility = View.GONE
                bindingAV.progressBarPlay.visibility = View.VISIBLE
                initAudioData()
            }
        }
        mainHandler.post(updateTextTask)
        reloadTranslateItemTextview()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        webView.stopLoading()
        webView.removeJavascriptInterface("JSInterface")
        webView.removeAllViews()
        webView.onPause()
        webView.destroy()
    }
/* endregion */
}

package mobi.eup.easykorean.view.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import mobi.eup.easykorean.data.model.ItemFeed
import mobi.eup.easykorean.database.repo.MyDatabaseRepo
import mobi.eup.easykorean.databinding.ItemRccNewsBinding
import mobi.eup.easykorean.utils.app.GlobalHelper
import mobi.eup.easykorean.view.activity.DetailActivity


class RccNewsAdapter(
    private var data: MutableList<ItemFeed>,
    context: Context,
    val onItemClick: (ItemFeed) -> Unit
) :
    RecyclerView.Adapter<RccNewsAdapter.ViewHoler>() {
    private var repoDatabase: MyDatabaseRepo
    private var mContext: Context = context

    init {
        repoDatabase = MyDatabaseRepo(mContext)
    }

    class ViewHoler(val binding: ItemRccNewsBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHoler {
        val binding = ItemRccNewsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHoler(binding)
    }

    override fun onBindViewHolder(holder: ViewHoler, position: Int) {
        val item = data[position]
        val isFavorite = repoDatabase.asyncTaskGetFaforiteStatus(data[position].id)
        item.isFavorite = isFavorite
        holder.binding.itemData = item

        if (isFavorite == 1) {
            holder.binding.tvFavoriteItemRcc.visibility = View.VISIBLE
        } else holder.binding.tvFavoriteItemRcc.visibility = View.GONE

        if (repoDatabase.asyncTaskGetSeenStatus(data[position].id.trim()) == 1) {
            holder.binding.tvSeenItemRcc.visibility = View.VISIBLE
        } else holder.binding.tvSeenItemRcc.visibility = View.GONE

        holder.binding.root.setOnClickListener {
            onItemClick(item)
            holder.binding.tvSeenItemRcc.visibility = View.VISIBLE
            val intent = Intent(mContext, DetailActivity::class.java)
            intent.putExtra(GlobalHelper.ITEM_ID_EXTRA, item.id)
            intent.putExtra(GlobalHelper.ITEM_TYPE_EXTRA, item.type)
            mContext.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun setAdapterData(data: MutableList<ItemFeed>) {
        data.also { it.also { it.also { this.data = it } } }
        notifyDataSetChanged()
    }

    fun insertAdapterData(data: MutableList<ItemFeed>, pos: Int) {
        this.data = data
        notifyItemInserted(pos)
    }
}

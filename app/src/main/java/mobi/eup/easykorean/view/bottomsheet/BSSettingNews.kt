package mobi.eup.easykorean.view.bottomsheet

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.multidex.BuildConfig
import androidx.recyclerview.widget.LinearLayoutManager
import mobi.eup.easykorean.R
import mobi.eup.easykorean.databinding.BottomSheetSettingsNewsBinding
import mobi.eup.easykorean.event.IntegerCallback
import mobi.eup.easykorean.data.model.ItemMore
import mobi.eup.easykorean.utils.app.GlobalHelper
import mobi.eup.easykorean.view.adapter.RccMoreAdapter
import mobi.eup.easykorean.view.base.BaseFragment

@Suppress("DEPRECATION")
class BSSettingNews : BaseFragment() {
    private lateinit var binding: BottomSheetSettingsNewsBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = BottomSheetSettingsNewsBinding.inflate(inflater, container, false)
        setupRecyclerView()
        return binding.root
    }

    private fun setupRecyclerView() {
        binding.rvApps.apply {
            adapter = RccMoreAdapter(createAppsList(), 1, appClickCallback())
            layoutManager = LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false)
            hasFixedSize()
            isNestedScrollingEnabled = false
        }

        binding.rv.apply {
            adapter = RccMoreAdapter(createSettingsList(), 0, settingClickCallback())
            layoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false)
            hasFixedSize()
            isNestedScrollingEnabled = false
        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun createSettingsList(): MutableList<ItemMore> {
        return mutableListOf(
            ItemMore(
                getString(R.string.share),
                getString(R.string.share_desc),
                resources.getDrawable(R.drawable.img_share)
            ),
            ItemMore(
                getString(R.string.feedback),
                getString(R.string.feedback_desc),
                resources.getDrawable(R.drawable.img_feedback)
            ),
            ItemMore(
                getString(R.string.rate_app),
                getString(R.string.rate_app_desc),
                resources.getDrawable(
                    R.drawable.img_star
                )
            )
        )
    }

    private fun appClickCallback() = object : IntegerCallback {
        override fun execute(position: Int) {
            when (position) {
                0 -> actionDownload(GlobalHelper.VOIKY_PLAYSTORE_URL)
                1 -> actionDownload(GlobalHelper.MYTEST_PLAYSTORE_URL)
                2 -> actionDownload(GlobalHelper.JEDICT_PLAYSTORE_URL)
            }

        }

    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun createAppsList(): MutableList<ItemMore> {
        return mutableListOf(
            ItemMore(
                getString(R.string.download_jv),
                getString(R.string.download_jv_desc),
                resources.getDrawable(R.drawable.ic_voiky)
            ),
            ItemMore(
                getString(R.string.download_mt),
                getString(R.string.download_mt_desc),
                resources.getDrawable(R.drawable.ic_mytest)
            ),
            ItemMore(
                getString(R.string.download_dictionary),
                getString(R.string.download_dictionary_desc),
                resources.getDrawable(
                    R.drawable.ic_mazii
                )
            )
        )
    }

    private fun settingClickCallback() = object : IntegerCallback {
        override fun execute(position: Int) {
            when (position) {
                0 -> actionShare()
                1 -> actionFeedBack()
                2 -> actionRate()
            }
        }
    }

    private fun actionFeedBack() {
        try {
            val emailIntent = Intent(
                Intent.ACTION_SENDTO,
                Uri.parse("mailto:" + "eup.mobi@gmail.com")
            )
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.feed_back_mail))
            emailIntent.putExtra(
                Intent.EXTRA_TEXT, "Android version: " + Build.VERSION.RELEASE +
                        "\nApp version: " + BuildConfig.VERSION_CODE +
                        "\n"
            )

            startActivity(Intent.createChooser(emailIntent, getString(R.string.email_us)))

        } catch (e: ActivityNotFoundException) {
            Toast.makeText(
                activity,
                resources.getString(R.string.error),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun actionRate() {
        if (activity == null)
            return
        val packageName = requireActivity().packageName
        val uri = Uri.parse("market://details?id=$packageName")
        val goToMarket = Intent(Intent.ACTION_VIEW, uri)
        try {
            startActivity(goToMarket)
        } catch (e: ActivityNotFoundException) {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=$packageName")
                )
            )
        }
    }

    private fun actionShare() {
        val sharingIntent = Intent(Intent.ACTION_SEND)
        sharingIntent.type = "text/plain"
        val shareBody = GlobalHelper.EASY_PLAYSTORE_URL
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share))
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody)
        startActivity(Intent.createChooser(sharingIntent, getString(R.string.share)))
    }

    private fun actionDownload(packageName: String) {
        val uri = Uri.parse(packageName)
        val goToMarket = Intent(Intent.ACTION_VIEW, uri)
        try {
            startActivity(goToMarket)
        } catch (e: ActivityNotFoundException) {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(GlobalHelper.JEDICT_PLAYSTORE_URL)))
        }
    }

}
package mobi.eup.easykorean.view.base

import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.ads.MobileAds
import mobi.eup.easykorean.admob.AdsBanner
import mobi.eup.easykorean.admob.AdsHelper
import mobi.eup.easykorean.admob.AdsInterstitial
import mobi.eup.easykorean.admob.AdsNative
import mobi.eup.easykorean.database.repo.MyDatabaseRepo
import mobi.eup.easykorean.utils.app.GlobalHelper
import mobi.eup.easykorean.utils.app.PreferenceHelper
import mobi.eup.easykorean.utils.eventbus.EventSettingsHelper
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

open class BaseActivity : AppCompatActivity() {
    protected lateinit var preferenceHelper: PreferenceHelper
    protected lateinit var repoDatabase: MyDatabaseRepo
    protected lateinit var mAdsInterstitial: AdsInterstitial
    protected lateinit var mAdsBanner: AdsBanner
    protected lateinit var adsNative: AdsNative

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        preferenceHelper = PreferenceHelper(this, GlobalHelper.PREFERENCE_NAME_APP)
        repoDatabase = MyDatabaseRepo(this)
        EventBus.getDefault().register(this)
        MobileAds.initialize(this@BaseActivity)
        mAdsInterstitial = AdsInterstitial(this@BaseActivity)
    }

    protected fun notifyUser(context: Context, mess: String) {
        Toast.makeText(context, mess, Toast.LENGTH_SHORT).show()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    open fun onSettingsEvent(event: EventSettingsHelper?) {
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    open fun onAdsmobEvent(event: AdsHelper?) {
    }

    override fun onDestroy() {
        EventBus.getDefault().unregister(this)
        super.onDestroy()
    }
}
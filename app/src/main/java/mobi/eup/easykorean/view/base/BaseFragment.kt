package mobi.eup.easykorean.view.base

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import mobi.eup.easykorean.database.repo.MyDatabaseRepo
import mobi.eup.easykorean.utils.app.GlobalHelper
import mobi.eup.easykorean.utils.app.PreferenceHelper


@Suppress("DEPRECATION")
open class BaseFragment : Fragment() {
    protected lateinit var mContext: FragmentActivity
    protected lateinit var preferenceHelper: PreferenceHelper
    protected lateinit var repoDatabase: MyDatabaseRepo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.preferenceHelper = PreferenceHelper(mContext, GlobalHelper.PREFERENCE_NAME_APP)
        this.repoDatabase = MyDatabaseRepo(mContext)
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        mContext = activity as FragmentActivity
    }
}
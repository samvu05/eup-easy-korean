package mobi.eup.easykorean.view.activity

import android.os.Bundle
import android.widget.RadioGroup
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.ViewPager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.gson.Gson
import mobi.eup.easykorean.R
import mobi.eup.easykorean.databinding.ActivityWordsBinding
import mobi.eup.easykorean.view.adapter.SlidePagerAdapterWord
import mobi.eup.easykorean.view.base.BaseActivity
import mobi.eup.easykorean.view.bottomsheet.BSSettingContainer
import mobi.eup.easykorean.view.fragment.WordsCardFragment
import mobi.eup.easykorean.view.fragment.WordsWordFragment
import info.hoang8f.android.segmented.SegmentedGroup

class WordsActivity : BaseActivity(), ViewPager.OnPageChangeListener,
    RadioGroup.OnCheckedChangeListener {
    private lateinit var binding: ActivityWordsBinding
    private lateinit var wordsWordFragment: WordsWordFragment
    private lateinit var wordsCardFragment: WordsCardFragment
    private lateinit var mPager: ViewPager
    private lateinit var mBottomNavigation: BottomNavigationView
    private lateinit var mSegmentControl: SegmentedGroup
    private lateinit var adapter: SlidePagerAdapterWord
    private lateinit var mToolbar: Toolbar
    private var isFavorite = false
    private lateinit var listWordID: List<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_words)
        initViews()
    }

    private fun initViews() {
        mPager = binding.homeViewpaper
        mBottomNavigation = binding.navBottom
        mSegmentControl = binding.segmentControlWord
        mToolbar = binding.toolBar
        getDataFromIntent()

        binding.btnSetting.setOnClickListener {
            val bottomSheetSettingAll = BSSettingContainer(false)
            bottomSheetSettingAll.show(supportFragmentManager, bottomSheetSettingAll.tag)
        }

        wordsWordFragment = WordsWordFragment(listWordID)
        wordsCardFragment = WordsCardFragment(listWordID)

        adapter = SlidePagerAdapterWord(supportFragmentManager)
        adapter.addFragment(wordsWordFragment, "Words")
        adapter.addFragment(wordsCardFragment, "Card")

        mPager.adapter = adapter
        mPager.offscreenPageLimit = adapter.count
        mPager.addOnPageChangeListener(this)

        // default selected segment
        mSegmentControl.check(R.id.tab_current_word)
        mSegmentControl.setOnCheckedChangeListener(this)

        // setup toolbar
        mToolbar = binding.toolBar
        setSupportActionBar(mToolbar)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayShowTitleEnabled(false)
            supportActionBar!!.setDisplayHomeAsUpEnabled(false)
        }
        binding.btnBack.setOnClickListener {
            onBackPressed()
            finish()
        }
        mBottomNavigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.navbot_item_words -> mPager.currentItem = 0
                R.id.navbot_item_flashcard -> mPager.currentItem = 1
            }
            true

        }
    }

    private fun getDataFromIntent() {
        val bundle = intent.extras ?: return
        val jsonData = bundle.getString(DetailActivity.SEND_ID_TO_WORD_ACTIVITY, "")
        listWordID = Gson().fromJson(jsonData, Array<String>::class.java).toList()
    }


    override fun onBackPressed() {
        if (mPager.currentItem == 0) {
            super.onBackPressed()
        } else {
            mPager.currentItem = mPager.currentItem - 1
        }
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
        mBottomNavigation.menu.getItem(position).isChecked = true
    }

    override fun onPageScrollStateChanged(state: Int) {
    }

    override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
        when (checkedId) {
            R.id.tab_current_word -> {
                isFavorite = false
                wordsWordFragment.onActionFavorite(false)
                wordsCardFragment.onActionFavorite(false)
            }
            R.id.tab_favorite_word -> {
                isFavorite = true
                wordsWordFragment.onActionFavorite(true)
                wordsCardFragment.onActionFavorite(true)
            }
        }
    }

    override fun onDestroy() {
        mAdsInterstitial.showAds()
        super.onDestroy()
    }
}


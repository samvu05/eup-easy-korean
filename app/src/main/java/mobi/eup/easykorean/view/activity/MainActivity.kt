package mobi.eup.easykorean.view.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.RadioGroup
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.ViewPager
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.anjlab.android.iab.v3.BillingProcessor
import com.anjlab.android.iab.v3.BillingProcessor.IBillingHandler
import com.anjlab.android.iab.v3.TransactionDetails
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.bottomnavigation.BottomNavigationView
import info.hoang8f.android.segmented.SegmentedGroup
import mobi.eup.easykorean.R
import mobi.eup.easykorean.admob.AdsHelper
import mobi.eup.easykorean.admob.AdsNative
import mobi.eup.easykorean.admob.BannerEvent
import mobi.eup.easykorean.databinding.ActivityMainBinding
import mobi.eup.easykorean.event.IAPCallback
import mobi.eup.easykorean.event.MainPagerListener
import mobi.eup.easykorean.event.VoidCallback
import mobi.eup.easykorean.utils.app.GlobalHelper
import mobi.eup.easykorean.utils.eventbus.EventBusState
import mobi.eup.easykorean.utils.eventbus.EventSettingsHelper
import mobi.eup.easykorean.utils.view.AlertHelper
import mobi.eup.easykorean.view.adapter.SlidePagerAdapterMain
import mobi.eup.easykorean.view.base.BaseActivity
import mobi.eup.easykorean.view.bottomsheet.BSAudioManager
import mobi.eup.easykorean.view.bottomsheet.BSHistoryContainer
import mobi.eup.easykorean.view.bottomsheet.BSPremium
import mobi.eup.easykorean.view.bottomsheet.BSSettingContainer
import org.greenrobot.eventbus.EventBus

class MainActivity : BaseActivity(), OnPageChangeListener, RadioGroup.OnCheckedChangeListener,
    MainPagerListener, BannerEvent, IAPCallback {
    private lateinit var binding: ActivityMainBinding

    private lateinit var mPager: ViewPager
    private lateinit var mBottomNavigation: BottomNavigationView
    private lateinit var mSegmentControl: SegmentedGroup
    private lateinit var mAppbar: AppBarLayout

    private lateinit var bp: BillingProcessor
    private var readyToPurchase = false
    private var bsPremium: BSPremium? = null
    private var bsSettingAll: BSSettingContainer? = null

    private lateinit var adapter: SlidePagerAdapterMain
    private var isEasyFavorite = false
    private var isDifficultFavorite = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        initInAppPurchase()
        initView()
        initAds()
    }

    private fun initAds() {
        if (preferenceHelper.isPremiumUser()) {
            return
        }
//        mAdsBanner = AdsBanner(this@MainActivity)
//        mAdsBanner.showBannerAdsView(binding.includedViewAdsBanner.adView, false)

        adsNative = AdsNative(this@MainActivity)
        adsNative.showNativeAdsView(binding.includedViewAdsBanner.adView, false)
    }

    private fun initView() {
        mPager = binding.homeViewpaper
        mBottomNavigation = findViewById(R.id.nav_bottom_home)
        mSegmentControl = binding.segmentControl
        mAppbar = binding.appBar
        adapter = SlidePagerAdapterMain(this@MainActivity.supportFragmentManager)
        mPager.adapter = adapter
        mPager.isSaveEnabled = false
        mPager.offscreenPageLimit = adapter.count
        mPager.addOnPageChangeListener(this)
        mSegmentControl.check(R.id.btn_current)
        mSegmentControl.setOnCheckedChangeListener(this)

        mBottomNavigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.navbot_item_easy -> mPager.currentItem = 0
                R.id.navbot_item_hard -> mPager.currentItem = 1
                R.id.navbot_item_dictionary -> mPager.currentItem = 2
            }
            true
        }
        binding.btnSearch.setOnClickListener {
            if (mPager.currentItem < 2) {
                startActivity(
                    Intent(this, SearchActivity::class.java).putExtra(
                        "isDifficult",
                        mPager.currentItem == 1
                    )
                )
            } else mPager.currentItem -= 1
        }

        binding.btnSetting.setOnClickListener {
            bsSettingAll = BSSettingContainer(true)
            bsSettingAll!!.show(supportFragmentManager, bsSettingAll!!.tag)
        }
        binding.btnHistory.setOnClickListener {
            val bsHistory = BSHistoryContainer()
            bsHistory.show(supportFragmentManager, bsHistory.tag)
        }
        binding.btnAudioManager.setOnClickListener {
            if (preferenceHelper.isPremiumUser()) {
                val bsAudio = BSAudioManager()
                bsAudio.show(supportFragmentManager, bsAudio.tag)
            } else {
                AlertHelper.showPremiumOnlyDialog(this@MainActivity, null, object : VoidCallback {
                    override fun execute() {
                        bsPremium = BSPremium(bp, false)
                        bsPremium!!.show(supportFragmentManager, bsPremium!!.tag)
                    }

                })
            }
        }
    }

    override fun onBackPressed() {
        if (mPager.currentItem == 0) {
            finish()
            finishAffinity()
        } else {
            mPager.currentItem = mPager.currentItem - 1
        }
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
        mBottomNavigation.menu.getItem(position).isChecked = true
        setCurrentStateSegmentControl(position, null)
    }

    override fun onPageScrollStateChanged(state: Int) {
    }

    override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
        if (!isFromUserChangeSegmentControl) return

        when (mPager.currentItem) {
            0 -> {
                isEasyFavorite = checkedId == R.id.btn_favorite
                adapter.toggleEasyFragment(isEasyFavorite)
            }
            1 -> {
                isDifficultFavorite = checkedId == R.id.btn_favorite
                adapter.toggleHardFragment(isDifficultFavorite)
            }
        }
        mBottomNavigation.menu.getItem(mPager.currentItem).isChecked = true
    }

    private var isFromUserChangeSegmentControl = true // tu dong chuyen thi k can replace fragment

    override fun setCurrentStateSegmentControl(pos: Int, isFavorite: Boolean?) {
        if (pos < 2) {
            if (isFavorite != null) {
                if (pos == 0) {
                    isEasyFavorite = isFavorite
                } else if (pos == 1) {
                    isDifficultFavorite = isFavorite
                }
            }
            val currentFavorite: Boolean = if (pos == 0) {
                isEasyFavorite
            } else
                isDifficultFavorite

            val id = if (!currentFavorite) R.id.btn_current else R.id.btn_favorite
            isFromUserChangeSegmentControl = false
            mSegmentControl.check(id)
            isFromUserChangeSegmentControl = true
            mSegmentControl.visibility = View.VISIBLE
            binding.btnHistory.visibility = View.VISIBLE
            binding.btnSearch.setImageResource(R.drawable.ic_search)
            binding.tvTitleDictionary.visibility = View.GONE

        } else {
            mSegmentControl.visibility = View.GONE
            binding.btnHistory.visibility = View.GONE
            binding.btnSearch.setImageResource(R.drawable.ic_back_arrow_night)
            binding.tvTitleDictionary.visibility = View.VISIBLE
        }
    }

    // MARK: banner event
    override fun updateContentViewWithBanner(height: Int) {
        val params = binding.navBottomHome.layoutParams as CoordinatorLayout.LayoutParams
        params.setMargins(0, 0, 0, height)
        binding.navBottomHome.layoutParams = params
        binding.navBottomHome.requestLayout()

        binding.homeViewpaper.setPadding(0, 0, 0, height)
        binding.homeViewpaper.requestLayout()
    }

    // MARK: hide banner
    override fun onAdsmobEvent(event: AdsHelper?) {
        super.onAdsmobEvent(event)
        if (event?.getState() === AdsHelper.State.REMOVE_ADS) {
            binding.includedViewAdsBanner.adView.visibility = View.GONE
            updateContentViewWithBanner(0)
        }
    }

    /**
     * MARK: in app purchase setup
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (readyToPurchase) {
            bp.handleActivityResult(requestCode, resultCode, data)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun initInAppPurchase() {
        val base64Key: String = resources.getString(R.string.base64key)
        bp = BillingProcessor(this@MainActivity, base64Key, object : IBillingHandler {
            override fun onProductPurchased(productId: String, details: TransactionDetails?) {
                Log.d(GlobalHelper.LOGCAT_TAG, "On Product Purchased, PoductID : $productId")
                when {
                    productId.contains(GlobalHelper.SKU_PREMIUM) -> {
                        preferenceHelper.setPremium(true)
                        removeAdsAndShowAlert(productId)
                    }
                    productId.contains(GlobalHelper.SKU_SUB3) -> {
                        val expired3 = System.currentTimeMillis() + 7889231490L
                        preferenceHelper.setPremiumDay(expired3)
                        removeAdsAndShowAlert(productId)
                    }
                    productId.contains(GlobalHelper.SKU_SUB12) -> {
                        val expired12 = System.currentTimeMillis() + 31556926000L
                        preferenceHelper.setPremiumDay(expired12)
                        removeAdsAndShowAlert(productId)
                    }
                }
            }

            private fun removeAdsAndShowAlert(SKU: String) {
                println(SKU)
                if (!isFinishing) {
                    if (bsPremium != null)
                        bsPremium!!.dismiss()
                }

                EventBus.getDefault().post(AdsHelper(AdsHelper.State.REMOVE_ADS))

                runOnUiThread {
                    AlertHelper.showTipAlert(
                        this@MainActivity,
                        R.drawable.img_premium,
                        resources.getString(R.string.premium_upgrade_successful_title),
                        resources.getString(R.string.premium_restore_successful_desc),
                        null
                    )
                }
            }

            override fun onPurchaseHistoryRestored() {
                Log.d(GlobalHelper.LOGCAT_TAG, "onPurchaseHistoryRestored")
                for (sku in bp.listOwnedProducts()) {
                    Log.d(GlobalHelper.LOGCAT_TAG, "owned product : $sku")
                    when {
                        sku.contains(GlobalHelper.SKU_PREMIUM) -> {
                            preferenceHelper.setPremium(true)
                            EventBus.getDefault().post(AdsHelper(AdsHelper.State.REMOVE_ADS))
                            return
                        }
                        sku.contains(GlobalHelper.SKU_SUB3) -> {
                            val expired3 = System.currentTimeMillis() + 7889231490L
                            preferenceHelper.setPremiumDay(expired3)
                            EventBus.getDefault().post(AdsHelper(AdsHelper.State.REMOVE_ADS))
                            return
                        }
                        sku.contains(GlobalHelper.SKU_SUB12) -> {
                            val expired12 = System.currentTimeMillis() + 31556926000L
                            preferenceHelper.setPremiumDay(expired12)
                            EventBus.getDefault().post(AdsHelper(AdsHelper.State.REMOVE_ADS))
                            return
                        }
                    }
                }
                if (bp.isSubscribed(GlobalHelper.SKU_SUB3) || bp.isSubscribed(
                        GlobalHelper.SKU_SUB3 + "_30"
                    ) || bp.isSubscribed(GlobalHelper.SKU_SUB3 + "_40") || bp.isSubscribed(
                        GlobalHelper.SKU_SUB3 + "_50"
                    )
                ) {
                    val expired3 = System.currentTimeMillis() + 7889231490L
                    preferenceHelper.setPremiumDay(expired3)
                    EventBus.getDefault().post(AdsHelper(AdsHelper.State.REMOVE_ADS))
                } else if (bp.isSubscribed(GlobalHelper.SKU_SUB12) || bp.isSubscribed(
                        GlobalHelper.SKU_SUB12 + "_30"
                    ) || bp.isSubscribed(GlobalHelper.SKU_SUB12 + "_40") || bp.isSubscribed(
                        GlobalHelper.SKU_SUB12 + "_50"
                    )
                ) {
                    val expired12 = System.currentTimeMillis() + 31556926000L
                    preferenceHelper.setPremiumDay(expired12)
                    EventBus.getDefault().post(AdsHelper(AdsHelper.State.REMOVE_ADS))
                }
            }

            override fun onBillingError(errorCode: Int, error: Throwable?) {}
            override fun onBillingInitialized() {
                Log.d(GlobalHelper.LOGCAT_TAG, "onBillingInitialized")
                readyToPurchase = true
                bp.loadOwnedPurchasesFromGoogle()

                Log.d(GlobalHelper.LOGCAT_TAG, "premiumUser : ${preferenceHelper.isPremiumUser()}")
                Log.d(GlobalHelper.LOGCAT_TAG, "premium : ${preferenceHelper.isPremium()}")
                Log.d(GlobalHelper.LOGCAT_TAG, "getPemiumDay : ${preferenceHelper.getPremiumDay()}")
                Log.d(GlobalHelper.LOGCAT_TAG, "isPremiumDay : : ${preferenceHelper.isPremiumDay()}")

                if (!preferenceHelper.isPremiumUser()) {
                    bsPremium = BSPremium(bp, false)
                    bsPremium!!.show(supportFragmentManager, bsPremium!!.tag)

                }
            }
        })
        bp.initialize()
    }

    override fun onUpgradePremiumClicked(skuID: String) {
        if (preferenceHelper.isPremiumUser()) {
            AlertHelper.showTipAlert(
                this,
                R.drawable.img_premium,
                resources.getString(R.string.premium_already_title),
                resources.getString(R.string.premium_already_desc),
                null
            )
            return
        }
        if (skuID.contains(GlobalHelper.SKU_PREMIUM)) {
            bp.purchase(this, skuID)
        } else {
            bp.subscribe(this, skuID)
        }
    }

    private fun onRestorePremiumClicked() {
        if (preferenceHelper.isPremiumUser()) {
            AlertHelper.showTipAlert(
                this,
                R.drawable.img_premium,
                resources.getString(R.string.premium_already_title),
                resources.getString(R.string.premium_already_desc),
                null
            )
            return
        }
        var removeAds = false
        if (bp.isPurchased(GlobalHelper.SKU_PREMIUM) || bp.isPurchased(
                GlobalHelper.SKU_PREMIUM + "_30"
            ) || bp.isPurchased(GlobalHelper.SKU_PREMIUM + "_40") || bp.isPurchased(
                GlobalHelper.SKU_PREMIUM + "_50"
            )
        ) {
            preferenceHelper.setPremium(true)
            removeAds = true
        } else if (bp.isSubscribed(GlobalHelper.SKU_SUB12) || bp.isSubscribed(
                GlobalHelper.SKU_SUB12 + "_30"
            ) || bp.isSubscribed(GlobalHelper.SKU_SUB12 + "_40") || bp.isSubscribed(
                GlobalHelper.SKU_SUB12 + "_50"
            )
        ) {
            removeAds = true
        } else if (bp.isSubscribed(GlobalHelper.SKU_SUB3) || bp.isSubscribed(
                GlobalHelper.SKU_SUB3 + "_30"
            ) || bp.isSubscribed(GlobalHelper.SKU_SUB3 + "_40") || bp.isSubscribed(
                GlobalHelper.SKU_SUB3 + "_50"
            )
        ) {
            removeAds = true
        }
        if (removeAds) {
            // loai bo quang cao hien tai event bus
            EventBus.getDefault().post(AdsHelper(AdsHelper.State.REMOVE_ADS))
            // thong bao cho nguoi dung restore thanh cong
            AlertHelper.showTipAlert(
                this@MainActivity,
                R.drawable.img_premium,
                resources.getString(R.string.premium_restore_successful_title),
                resources.getString(R.string.premium_restore_successful_desc),
                null
            )
        } else {
            AlertHelper.showTipAlert(
                this@MainActivity,
                R.drawable.img_premium,
                resources.getString(R.string.premium_restore_failed_title),
                resources.getString(R.string.premium_restore_failed_desc),
                null
            )
        }
    }

    override fun onSettingsEvent(event: EventSettingsHelper?) {
        super.onSettingsEvent(event)
        when (event?.getStateChange()) {
            EventBusState.UPGRADE_PREMIUM -> if (preferenceHelper.isPremiumUser()) {
                onRestorePremiumClicked()
            } else {
                if (!isFinishing && !isDestroyed) {
                    bsSettingAll?.dismiss()
                }
                if (bsPremium != null) {
                    bsPremium!!.dismiss()
                }
                bsPremium = BSPremium(bp, false)
                bsPremium!!.show(supportFragmentManager, bsPremium!!.tag)
            }
            else -> return
        }
    }
}
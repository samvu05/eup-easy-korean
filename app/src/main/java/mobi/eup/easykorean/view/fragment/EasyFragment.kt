package mobi.eup.easykorean.view.fragment

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.size
import androidx.fragment.app.FragmentManager
import androidx.viewpager.widget.ViewPager
import mobi.eup.easykorean.databinding.FragmentEasyBinding
import mobi.eup.easykorean.event.MainPagerListener
import mobi.eup.easykorean.view.adapter.SlidePagerAdapterCommon
import mobi.eup.easykorean.view.animation.DepthPageTransformer
import mobi.eup.easykorean.view.base.BaseFragment


@Suppress("DEPRECATION")
class EasyFragment : BaseFragment(), ViewPager.OnPageChangeListener {
    private lateinit var binding: FragmentEasyBinding
    private lateinit var easyCurrentFragment: EasyCurrentFragment
    private lateinit var easyFavoriteFragment: EasyFavoriteFragment
    private lateinit var mPaper: ViewPager
    private var listener: MainPagerListener? = null

    companion object {
        fun newInstance(): EasyFragment {
            val args = Bundle()
            val fragment =
                EasyFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentEasyBinding.inflate(inflater, container, false)
        mPaper = binding.viewpaperEasyFrag
        mPaper.setPageTransformer(true, DepthPageTransformer())
        mPaper.addOnPageChangeListener(this)
        easyCurrentFragment = EasyCurrentFragment()
        easyFavoriteFragment = EasyFavoriteFragment()

        val fragManager: FragmentManager = mContext.supportFragmentManager
        val pagerAdapter = SlidePagerAdapterCommon(fragManager)
        pagerAdapter.addFragment(easyCurrentFragment, "Current")
        pagerAdapter.addFragment(this.easyFavoriteFragment, "Faforite")
        mPaper.adapter = pagerAdapter
        mPaper.offscreenPageLimit = pagerAdapter.count
        return binding.root
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        if (context is MainPagerListener) listener = context as MainPagerListener
    }

    fun setCurrentItem(pos: Int) {
        println(pos)
        println(mPaper.size)
        mPaper.setCurrentItem(pos, true)
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
        if (listener != null) {
            listener!!.setCurrentStateSegmentControl(0, position == 1)
        }
    }

    override fun onPageScrollStateChanged(state: Int) {
    }
}